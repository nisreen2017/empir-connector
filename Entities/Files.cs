﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class Files : ReadOnlyListBase<Files, Sharepoint_Doc.Entities.File>
    {
        public object Sort(string property, bool ascending)
        {
            List<File> list = null;

            if (ascending)
            {
                switch (property.ToLower())
                {
                    case "namn":
                        list = (from j in this orderby j.Name select j).ToList();
                        break;
                    case "ändrad av":
                        list = (from j in this orderby j.Author select j).ToList();
                        break;
                    case "ändrad":
                        list = (from j in this orderby j.Modified select j).ToList();
                        break;
                }
            }
            else
            {
                switch (property.ToLower())
                {
                    case "namn":
                        list = (from j in this orderby j.Name descending select j).ToList();
                        break;
                    case "ändrad av":
                        list = (from j in this orderby j.Author descending select j).ToList();
                        break;
                    case "ändrad":
                        list = (from j in this orderby j.Modified descending select j).ToList();
                        break;
                }
            }

            RaiseListChangedEvents = false;
            IsReadOnly = false;

            this.Clear();

            foreach (File file in list)
                Add(file);

            IsReadOnly = true;
            RaiseListChangedEvents = true;

            return this;
        }

        public static Files GetFiles(string serverRelativeUrl, string parentWebUrl, string listId, string webApplication)
        {
            return DataPortal.FetchChild<Files>(new FilesCriteria(serverRelativeUrl, parentWebUrl, listId, webApplication));
        }

        #region Data Access

        private void Child_Fetch(FilesCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;

            DAL.FileDAL dal = new DAL.FileDAL();
           List< Microsoft.SharePoint.Client.File> files = dal.LoadFiles2(criteria.ServerRelativeUrl, criteria.ParentWebUrl, criteria.ListId);

            foreach (Microsoft.SharePoint.Client.File fi in files.OrderBy(f => f.Name))
            {
                string folderUrl = criteria.WebApplication + criteria.ServerRelativeUrl;

                if (criteria.ParentWebUrl.ToLower().Contains(AddInGlobals.ConnectorSettings.OneDriveWebUrl.ToLower()))
                {
                    var uri = new Uri(AddInGlobals.ConnectorSettings.OneDriveUrl);
                    folderUrl = uri.Scheme + "://" + uri.Host + criteria.ServerRelativeUrl;
                }
                Add(File.GetFile(fi, folderUrl));
            }

            IsReadOnly = true;
            RaiseListChangedEvents = true;

        }

        private bool IncludeByExtension(ListItem listItem)
        {
            string filename = listItem["FileLeafRef"].ToString();

            if (!filename.Contains("."))
                return false;

            string extension = System.IO.Path.GetExtension(filename).Substring(1);

            bool res  = false;

            if (AddInGlobals.ExtensionAndFileTypes.Extensions[0] == "*")
                res = true;
            else
                res = AddInGlobals.ExtensionAndFileTypes.Extensions.Where(s => s == extension).FirstOrDefault() != null;

            return res;
        }

        #endregion

    }

    public class FilesCriteria : CriteriaBase<FilesCriteria>
    {
        public string ListId
        {
            set;
            get;
        }

        public string ParentWebUrl
        {
            set;
            get;
        }

        public string ServerRelativeUrl
        {
            set;
            get;
        }

        public string WebApplication
        {
            set;
            get;
        }

        public FilesCriteria(string serverRelativeUrl, string parentWebUrl, string listId, string webApplication)
        {
            ServerRelativeUrl = serverRelativeUrl;
            ListId = listId;
            ParentWebUrl = parentWebUrl;
            WebApplication = webApplication;
        }

    }
}
