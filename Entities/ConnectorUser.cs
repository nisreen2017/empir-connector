﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmpirConnector.Entities
{
    public class ConnectorUser
    {
        public static string Username
        {
            get
            {
                //return "thn__asf_ifouser";
                return System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\')[1];
            }
        }
    }
}
