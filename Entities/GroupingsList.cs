﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;

namespace Sharepoint_Doc.Entities
{
    public class GroupingsList: ReadOnlyListBase<GroupingsList, Grouping>
    {

         public static GroupingsList GetList()
        {
          return DataPortal.FetchChild<GroupingsList>();
        }

        private GroupingsList()
        { /* require use of factory methods */ }




        private void Child_Fetch()
        {
          RaiseListChangedEvents = false;
          IsReadOnly = false;


          DAL.GroupingDAL dal = new DAL.GroupingDAL();
          ListItemCollection list = dal.GetList();

          foreach(ListItem listItem in list)
          {
              Add(Grouping.GetGrouping(listItem));
          }
          IsReadOnly = true;
          RaiseListChangedEvents = true;
        }
    }
}
