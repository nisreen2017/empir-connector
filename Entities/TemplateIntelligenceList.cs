﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;

namespace EmpirConnector.Entities
{
    public class TemplateIntelligenceList: ReadOnlyListBase<TemplateIntelligenceList, TemplateIntelligence>
    {
        private List<string> _preparedGovernances;

        public bool IsGovernancePrepared(string g)
        {
            return _preparedGovernances.Where(s => s == g).FirstOrDefault() != null;
        }

         public static TemplateIntelligenceList GetList()
        {
          return DataPortal.FetchChild<TemplateIntelligenceList>();
        }

         private TemplateIntelligenceList()
        { /* require use of factory methods */ }




        private void Child_Fetch()
        {
          RaiseListChangedEvents = false;
          IsReadOnly = false;


          DAL.TemplateIntelligenceDAL dal = new DAL.TemplateIntelligenceDAL();

          FolderCollection attachmentFolders;

         
          ListItemCollection list = dal.GetList(out attachmentFolders);

          foreach (ListItem listItem in list)
          {
              Add(TemplateIntelligence.GetTemplateIntelligence(listItem));
          }

          _preparedGovernances = new List<string>();
          foreach (Folder folder in attachmentFolders)
          {
              if (folder.Files.Count == 2)
                  _preparedGovernances.Add(folder.Name);
          }

          IsReadOnly = true;
          RaiseListChangedEvents = true;
        }
    }
}