﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Sharepoint_Doc.Entities;

namespace EmpirConnector.Entities
{
    [Serializable]
    public class GovernancesList:  ReadOnlyListBase<GovernancesList, Governance>
    {
        public static GovernancesList GetList()
        {
            return DataPortal.FetchChild<GovernancesList>();
        }

        private void Child_Fetch()
        {
            Sharepoint_Doc.DAL.ListDAL dal = new Sharepoint_Doc.DAL.ListDAL();

           // Sharepoint_Doc.DAL.ListDAL.SharePointFolder rootFolder = dal.LoadFolders("c61c9c43-b21d-4ac0-90b6-7b04e16aa284", "http://stadsportalen.trollhattan.se", true);
            Sharepoint_Doc.DAL.ListDAL.SharePointFolder rootFolder = dal.LoadFolders("c61c9c43-b21d-4ac0-90b6-7b04e16aa284", AddInGlobals.ConnectorSettings.RootUrl, true);

            RaiseListChangedEvents = false;
            IsReadOnly = false;


            foreach (Sharepoint_Doc.DAL.ListDAL.SharePointFolder folder in rootFolder.Folders)
            {
                Add(Governance.GetGovernance(folder));
            }
        

            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }
    }
}
