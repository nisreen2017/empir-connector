﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sharepoint_Doc.Entities;

namespace EmpirConnector.Entities
{
    [Serializable]
    public class PreloadedWebList : Csla.BusinessListBase<PreloadedWebList, Web>
    {
    }
}
