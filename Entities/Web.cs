﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;
using EmpirConnector;
using EmpirConnector.Entities;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class Web : BusinessBase<Web>
    {
        public static readonly PropertyInfo<string> TitleProperty = RegisterProperty<string>(p => p.Title);
        public string Title
        {
            get { return GetProperty(TitleProperty); }
            set { LoadProperty(TitleProperty, value); }
        }

        public static readonly PropertyInfo<string> RootSiteProperty = RegisterProperty<string>(p => p.RootSite);
        public string RootSite
        {
            get { return GetProperty(RootSiteProperty); }
            set { LoadProperty(RootSiteProperty, value); }
        }

        public static readonly PropertyInfo<string> WebIdProperty = RegisterProperty<string>(p => p.WebId);
        public string WebId
        {
            get { return GetProperty(WebIdProperty); }
            set { LoadProperty(WebIdProperty, value); }
        }

        public static readonly PropertyInfo<string> ServerRelativeUrlProperty = RegisterProperty<string>(p => p.ServerRelativeUrl);
        public string ServerRelativeUrl
        {
            get { return GetProperty(ServerRelativeUrlProperty); }
            set { LoadProperty(ServerRelativeUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> WebTemplateProperty = RegisterProperty<string>(p => p.WebTemplate);
        public string WebTemplate
        {
            get { return GetProperty(WebTemplateProperty); }
            set { LoadProperty(WebTemplateProperty, value); }
        }

        public static readonly PropertyInfo<string> FullUrlProperty = RegisterProperty<string>(p => p.FullUrl);
        public string FullUrl
        {
            get { return GetProperty(FullUrlProperty); }
            set { LoadProperty(FullUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> UrlProperty = RegisterProperty<string>(p => p.Url);
        public string Url
        {
            get { return GetProperty(UrlProperty); }
            set { LoadProperty(UrlProperty, value); }
        }

        private bool IsOneDrive
        {
            get
            {
                return this.Url.ToLower().Contains("minsida");
            }
        }

        public static readonly PropertyInfo<string> SiteUrlProperty = RegisterProperty<string>(p => p.SiteUrl);
        public string SiteUrl
        {
            get { return GetProperty(SiteUrlProperty); }
            set { LoadProperty(SiteUrlProperty, value); }
        }

        

        public static readonly PropertyInfo<Grouping> GroupingProperty = RegisterProperty<Grouping>(p => p.Grouping);
        public Grouping Grouping
        {
            get { return GetProperty(GroupingProperty); }
            set { LoadProperty(GroupingProperty, value); }
        }

        public bool LoadingRead
        {
            get;
            set;
        }

        bool _includeSubSitesInListing = true;
        public bool IncludeSubSitesInListing
        {
            set
            {
                _includeSubSitesInListing = value;
            }
            get
            {
                return _includeSubSitesInListing;
            }
        }

        public static readonly PropertyInfo<DocumentLibraries> DocumentLibrariesProperty =
            RegisterProperty<DocumentLibraries>(p => p.DocumentLibraries);
        public DocumentLibraries DocumentLibraries
        {
            get {

                if (!FieldManager.FieldExists(DocumentLibrariesProperty))
                {

                    Sharepoint_Doc.DAL.WebDAL dal = new DAL.WebDAL();
                    GetListsAndSubWebsCommand.WebData webDataResult = null;

                    if (AddinModule.CurrentFunction == AddinModule.CurrentFunctionEnum.Loading)
                    {
                        webDataResult = GetListsAndSubWebsCommand.Execute(this.Url, LoadingRead);
                    }
                    else
                    {
                        webDataResult = GetListsAndSubWebsCommand.Execute(this.Url, AddinModule.CurrentFunction == AddinModule.CurrentFunctionEnum.Open);
                    }

                    DocumentLibraries = DocumentLibraries.GetDocumentLibraries(webDataResult.DocumentLibraries, Grouping, this.IsOneDrive);
                    SubWebs = Webs.GetWebs(new Webs.WebsCriteria(webDataResult.SubWebs, Grouping));
                }

                return GetProperty(DocumentLibrariesProperty); 
            
            }
            set { LoadProperty(DocumentLibrariesProperty, value); }
        }

        private string GetSiteUrl()
        {
            Web parent = this;

            while (parent.Parent != null)
                parent = (Web)parent.Parent;

            return parent.Grouping.WebApplikation + parent.ServerRelativeUrl;
        }

        public static readonly PropertyInfo<Webs> SubWebsProperty =
            RegisterProperty<Webs>(p => p.SubWebs);
        public Webs SubWebs
        {
            get {
                if (!FieldManager.FieldExists(SubWebsProperty))
                {
                    try
                    {
                        GetListsAndSubWebsCommand.WebData webDataResult = null;
                        Sharepoint_Doc.DAL.WebDAL dal = new DAL.WebDAL();
                        if (AddinModule.CurrentFunction == AddinModule.CurrentFunctionEnum.Loading)
                        {
                            webDataResult = GetListsAndSubWebsCommand.Execute(this.Url, LoadingRead);
                        }
                        else
                        {
                            webDataResult = GetListsAndSubWebsCommand.Execute(this.Url, AddinModule.CurrentFunction == AddinModule.CurrentFunctionEnum.Open);
                        }
                            //Grouping.WebApplikation + this.ServerRelativeUrl
                        DocumentLibraries = DocumentLibraries.GetDocumentLibraries(webDataResult.DocumentLibraries, Grouping, this.IsOneDrive);
                        SubWebs = Webs.GetWebs(new Webs.WebsCriteria(webDataResult.SubWebs, Grouping));                      
                    }
                    catch(Exception ex)
                    {
                        string s = ex.Message;
                        SubWebs = Webs.NewWebs();
                    }
                }
                return GetProperty(SubWebsProperty); 
            
            }
            set 
            {
                if (this.WebTemplate.ToLower().StartsWith("gov"))
                {
                    foreach (Web web in value)
                    {
                        if (web.WebTemplate.ToLower().Contains("room"))
                        {
                            Console.Write("");
                        }
                    }
                }
                LoadProperty(SubWebsProperty, value); 
            }
        }

        public void LoadDirectoriesAndSubWebs()
        {
            DataPortal_Fetch(new WebCriteria(this.Url, this.Grouping, false));
        }

       #region Factory Methods

        internal static Web NewWeb()
        {
            return DataPortal.Create<Web>();
        }

        internal static Web GetWeb(WebCriteria criteria)
        {
            return DataPortal.Fetch<Web>(criteria);
        }

        internal static Web GetWeb(string  url)
        {
            return DataPortal.Fetch<Web>(url);
        }

        private Web()
        { /* require use of factory methods */ }

        #endregion

        #region Child Factory Methods

        internal static Web GetWeb(GetListsAndSubWebsCommand.SubWeb childData, Grouping grouping)
        {
            return DataPortal.FetchChild<Web>(new WebCriteriaChild(grouping, childData));
        }


        #endregion

        #region Root Data Access

        internal void LoadProperties(Microsoft.SharePoint.Client.Web web, bool isSubWeb, Grouping grouping)
        {
            
            if (web == null)
                return;

            LoadProperty(TitleProperty, web.Title);
            LoadProperty(WebIdProperty, web.Id.ToString());
            if (!isSubWeb)
                LoadProperty(DocumentLibrariesProperty, DocumentLibraries.GetDocumentLibraries(web, grouping));


            LoadProperty(ServerRelativeUrlProperty, web.ServerRelativeUrl);
            Grouping = grouping;

            if (web.AllProperties.FieldValues.ContainsKey("WebTemplate"))
            {
                string webTemplate = web.AllProperties.FieldValues["WebTemplate"].ToString();
                LoadProperty(WebTemplateProperty, webTemplate.Split('.')[1]);
            }

          
        }

        internal void LoadProperties(GetListsAndSubWebsCommand.SubWeb web, bool isSubWeb, Grouping grouping)
        {

            if (web == null)
                return;

            LoadProperty(TitleProperty, web.Title);
            LoadProperty(WebIdProperty, web.Id);
            //LoadProperty(SiteUrlProperty, web.SiteUrl);
            LoadProperty(ServerRelativeUrlProperty, web.ServerRelativeUrl);
            Grouping = grouping;
  

        }

        private void DataPortal_Fetch(WebCriteria criteria)
        {
            DAL.WebDAL dal = new DAL.WebDAL();
            var data = dal.GetWeb(criteria.Url);
            LoadProperties(data, criteria.IsSubWeb, criteria.Grouping); //false
            Url = criteria.Url;
        }


        private void DataPortal_Fetch(string url)
        {
            string[] parts = url.Split("/".ToCharArray());

            LoadProperty(TitleProperty, parts[parts.Length - 1]);
            Url = url;
        }

        private void Child_Fetch(WebCriteriaChild criteria)
        {
            LoadProperties(criteria.Web, true, criteria.Grouping);

            Url = AddInGlobals.ConnectorSettings.RootUrl + criteria.Web.ServerRelativeUrl;
        }

        #endregion

        internal class WebCriteria : CriteriaBase<WebCriteria>
        {
            public string Url
            {
                get;
                set;
            }

            public Grouping Grouping
            {
                set;
                get;
            }

            public bool IsSubWeb
            {
                get;
                set;
            }

            public WebCriteria(string url, Grouping grouping, bool isSubWeb)
            {
                Url = url;
                Grouping = grouping;
                IsSubWeb = isSubWeb;
            }
        }


        internal class WebCriteriaChild : CriteriaBase<WebCriteriaChild>
        {


            public Grouping Grouping
            {
                set;
                get;
            }

            public GetListsAndSubWebsCommand.SubWeb Web
            {
                set;
                get;
            }

            public WebCriteriaChild(Grouping grouping, GetListsAndSubWebsCommand.SubWeb childData)
            {
                Grouping = grouping;
                Web = childData;
            }
        }
    }
}
