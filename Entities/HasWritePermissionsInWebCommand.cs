﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Entities;

namespace EmpirConnector.Entities
{
    public class HasWritePermissionsInWebCommand
    {
        private static bool ExcludedLibrary(List list)
        {
            bool exclude = AddInGlobals.ExcludeListNameList.Where(s => s.Title.ToLower() == list.Title.ToLower()).FirstOrDefault() != null;

            if (list.Title == "Pages")
                exclude = true;

            if (!exclude)
                exclude = list.Title.ToLower().Contains("bilder");

            return exclude;
        }

        public static bool Execute(Microsoft.SharePoint.Client.Web web)
        {
            foreach (Microsoft.SharePoint.Client.List list in web.Lists)
            {
                if (list.BaseType == BaseType.DocumentLibrary && !list.Hidden && !ExcludedLibrary(list))
                {
                    if (list.EffectiveBasePermissions.Has(Microsoft.SharePoint.Client.PermissionKind.AddListItems))
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
