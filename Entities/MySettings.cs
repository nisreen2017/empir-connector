﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using System.Windows.Forms;
using System.Security.AccessControl;
using System.Security.Principal;

namespace EmpirConnector.Entities
{
    public class MySettings
    {

        private static MySettings _mySettings;
        private ApplicationSettings _settings = new ApplicationSettings();

        public static MySettings Instance
        {
            get
            {
                if (_mySettings == null)
                {
                    _mySettings = new MySettings();
                    _mySettings.LoadSettings();
                }
  

                return _mySettings;
            }
                    
        }

        public bool ShowDIP
        {
            get
            {
                return _settings.ShowDIP;
            }
            set
            {
                _settings.ShowDIP = value;
            }

        }

        public void LoadSettings()
        {
            _settings.LoadAppSettings();
        }

        public void Save()
        {
            _settings.SaveAppSettings();
        }



    }

    public class ApplicationSettings
    {
        private bool appSettingsChanged;


        private string m_defaultDirectory;
        private bool m_ShowDip;


        private string ConfigPath
        {
            get
            {
                string userName = Environment.UserName;

                CommonApplicationData common = new CommonApplicationData("Empir", "Connector", true);


                return common.ApplicationFolderPath + @"\" + userName + "_EmpirConnector.config";
            }
        }

        // Properties used to access the application settings variables.
        public string DefaultDirectory
        {
            get { return m_defaultDirectory; }
            set
            {
                if (value != m_defaultDirectory)
                {
                    m_defaultDirectory = value;
                    appSettingsChanged = true;
                }
            }
        }

        public bool ShowDIP
        {
            get { return m_ShowDip; }
            set
            {
                m_ShowDip = value;
                appSettingsChanged = true;    
            }
        }

        public ApplicationSettings()
        {
            
        }

        public bool SaveAppSettings()
        {
            if (this.appSettingsChanged)
            {
                StreamWriter myWriter = null;
                XmlSerializer mySerializer = null;
                try
                {
                    // Create an XmlSerializer for the 
                    // ApplicationSettings type.
                    mySerializer = new XmlSerializer(
                      typeof(ApplicationSettings));
                    myWriter =
                      new StreamWriter(ConfigPath, false);
                    // Serialize this instance of the ApplicationSettings 
                    // class to the config file.
                    mySerializer.Serialize(myWriter, this);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                finally
                {
                    // If the FileStream is open, close it.
                    if (myWriter != null)
                    {
                        myWriter.Close();
                    }
                }
            }
            return appSettingsChanged;
        }


        public bool LoadAppSettings()
        {
            XmlSerializer mySerializer = null;
            FileStream myFileStream = null;
            bool fileExists = false;

            try
            {
                // Create an XmlSerializer for the ApplicationSettings type.
                mySerializer = new XmlSerializer(typeof(ApplicationSettings));
                FileInfo fi = new FileInfo(ConfigPath);
                // If the config file exists, open it.
                if (fi.Exists)
                {
                    myFileStream = fi.OpenRead();
                    // Create a new instance of the ApplicationSettings by
                    // deserializing the config file.
                    ApplicationSettings myAppSettings =
                      (ApplicationSettings)mySerializer.Deserialize(
                       myFileStream);
                    // Assign the property values to this instance of 
                    // the ApplicationSettings class.
                    this.m_ShowDip = myAppSettings.ShowDIP;
                    this.m_defaultDirectory = myAppSettings.DefaultDirectory;
                    fileExists = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                throw;
            }
            finally
            {
                // If the FileStream is open, close it.
                if (myFileStream != null)
                {
                    myFileStream.Close();
                }
            }

            if (m_defaultDirectory == null)
            {
                // If myDirectory is not set, default
                // to the user's "My Documents" directory.
                m_defaultDirectory = Environment.GetFolderPath(
                   System.Environment.SpecialFolder.Personal);
                this.appSettingsChanged = true;
            }
            return fileExists;
        }
    }


    public class CommonApplicationData
    {
        private string applicationFolder;
        private string companyFolder;
        private static readonly string directory =
            Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

        /// <summary>
        /// Creates a new instance of this class creating the specified company and application folders
        /// if they don't already exist and optionally allows write/modify to all users.
        /// </summary>
        /// <param name="companyFolder">The name of the company's folder (normally the company name).</param>
        /// <param name="applicationFolder">The name of the application's folder (normally the application name).</param>
        /// <remarks>If the application folder already exists then permissions if requested are NOT altered.</remarks>
        public CommonApplicationData(string companyFolder, string applicationFolder)
            : this(companyFolder, applicationFolder, false)
        { }
        /// <summary>
        /// Creates a new instance of this class creating the specified company and application folders
        /// if they don't already exist and optionally allows write/modify to all users.
        /// </summary>
        /// <param name="companyFolder">The name of the company's folder (normally the company name).</param>
        /// <param name="applicationFolder">The name of the application's folder (normally the application name).</param>
        /// <param name="allUsers">true to allow write/modify to all users; otherwise, false.</param>
        /// <remarks>If the application folder already exists then permissions if requested are NOT altered.</remarks>
        public CommonApplicationData(string companyFolder, string applicationFolder, bool allUsers)
        {
            this.applicationFolder = applicationFolder;
            this.companyFolder = companyFolder;
            CreateFolders(allUsers);
        }

        /// <summary>
        /// Gets the path of the application's data folder.
        /// </summary>
        public string ApplicationFolderPath
        {
            get { return Path.Combine(CompanyFolderPath, applicationFolder); }
        }
        /// <summary>
        /// Gets the path of the company's data folder.
        /// </summary>
        public string CompanyFolderPath
        {
            get { return Path.Combine(directory, companyFolder); }
        }

        private void CreateFolders(bool allUsers)
        {
            DirectoryInfo directoryInfo;
            DirectorySecurity directorySecurity;
            AccessRule rule;
            SecurityIdentifier securityIdentifier = new SecurityIdentifier
                (WellKnownSidType.BuiltinUsersSid, null);
            if (!Directory.Exists(CompanyFolderPath))
            {
                directoryInfo = Directory.CreateDirectory(CompanyFolderPath);
                bool modified;
                directorySecurity = directoryInfo.GetAccessControl();
                rule = new FileSystemAccessRule(
                        securityIdentifier,
                        FileSystemRights.Write |
                        FileSystemRights.ReadAndExecute |
                        FileSystemRights.Modify,
                        AccessControlType.Allow);
                directorySecurity.ModifyAccessRule(AccessControlModification.Add, rule, out modified);
                directoryInfo.SetAccessControl(directorySecurity);
            }
            if (!Directory.Exists(ApplicationFolderPath))
            {
                directoryInfo = Directory.CreateDirectory(ApplicationFolderPath);
                if (allUsers)
                {
                    bool modified;
                    directorySecurity = directoryInfo.GetAccessControl();
                    rule = new FileSystemAccessRule(
                        securityIdentifier,
                        FileSystemRights.Write |
                        FileSystemRights.ReadAndExecute |
                        FileSystemRights.Modify,
                        InheritanceFlags.ContainerInherit |
                        InheritanceFlags.ObjectInherit,
                        PropagationFlags.InheritOnly,
                        AccessControlType.Allow);
                    directorySecurity.ModifyAccessRule(AccessControlModification.Add, rule, out modified);
                    directoryInfo.SetAccessControl(directorySecurity);
                }
            }
        }
        /// <summary>
        /// Returns the path of the application's data folder.
        /// </summary>
        /// <returns>The path of the application's data folder.</returns>
        public override string ToString()
        {
            return ApplicationFolderPath;
        }
    }
}
