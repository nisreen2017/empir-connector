﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;
using EmpirConnector.Entities;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class DocumentLibraries : ReadOnlyListBase<DocumentLibraries, DocumentLibrary>
    {
         #region Authorization Rules

        private static void AddObjectAuthorizationRules()
        {
            // TODO: add authorization rules
            //AuthorizationRules.AllowGet(typeof(ReadOnlyList), "Role");
        }

        #endregion

        public List<string> _excludedLibraries;

        #region Factory Methods

 

        public static DocumentLibraries GetDocumentLibraries(Microsoft.SharePoint.Client.Web web, Grouping grouping)
        {
            return DataPortal.FetchChild<DocumentLibraries>(new DocumentLibrariesCriteria(web, grouping));
        }

        public static DocumentLibraries GetDocumentLibraries(List<GetListsAndSubWebsCommand.DocumentLibrary> libaries, Grouping grouping, bool isOneDrive)
        {
            return DataPortal.FetchChild<DocumentLibraries>(new DocumentLibrariesFromServiceCriteria(libaries, DocumentLibrariesFromServiceCriteria.RequiredPermissionEnum.Read, grouping, isOneDrive));
        }

        private DocumentLibraries()
        {
            //_excludedLibraries = new List<string>();
            //_excludedLibraries.Add("c");
            //_excludedLibraries.Add("bilder i webbplatssamling");
            //_excludedLibraries.Add("dokument");
            //_excludedLibraries.Add("dokument i webbplatssamling");
            //_excludedLibraries.Add("formatbibliotek");
            //_excludedLibraries.Add("sidor");
            //_excludedLibraries.Add("webbplatssidor");
            //_excludedLibraries.Add("webbplatstillgångar");
            //_excludedLibraries.Add("connector - mallar");
        }


        #endregion

        #region Data Access

        public static bool ExcludedLibrary(List list)
        {
            bool exclude = AddInGlobals.ExcludeListNameList.Where(s => s.Title.ToLower() == list.Title.ToLower()).FirstOrDefault() != null;

            if(list.Title == "Pages")
                exclude = true;

            if(!exclude)
                exclude = list.Title.ToLower().Contains("bilder");

            return exclude;
        }

        public static bool ExcludedLibrary(GetListsAndSubWebsCommand.DocumentLibrary list)
        {
            bool exclude = AddInGlobals.ExcludeListNameList.Where(s => s.Title.ToLower() == list.Title.ToLower()).FirstOrDefault() != null;

            if(list.Title == "Pages")
                exclude = true;

            if(!exclude)
                exclude = list.Title.ToLower().Contains("bilder");

            return exclude;
        }

        private void Child_Fetch(DocumentLibrariesCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;

            foreach (List list in criteria.Web.Lists)
            {
                if (list.BaseType == BaseType.DocumentLibrary && !list.Hidden && !ExcludedLibrary(list))
                    Add(DocumentLibrary.GetLibrary(list, criteria.Grouping));
            }

            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }
        

        private void Child_Fetch(DocumentLibrariesFromServiceCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;
            foreach (GetListsAndSubWebsCommand.DocumentLibrary library in criteria.Libraries)
            {
                if (!ExcludedLibrary(library))
                {
                    Add(DocumentLibrary.GetLibrary(library, criteria.Grouping, criteria.IsOneDrive));
                }
            }

            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }

        #endregion

        public class DocumentLibrariesCriteria : CriteriaBase<DocumentLibrariesCriteria>
        {
            public Microsoft.SharePoint.Client.Web Web
            {
                set;
                get;
            }

            public Grouping Grouping
            {
                set;
                get;
            }

            public DocumentLibrariesCriteria(Microsoft.SharePoint.Client.Web web, Grouping grouping)
            {
                Web = web;
                Grouping = grouping;
            }

        }

        public class DocumentLibrariesFromServiceCriteria : CriteriaBase<DocumentLibrariesFromServiceCriteria>
        {
            public enum RequiredPermissionEnum
            {
                Read,
                Write
            }

            public Grouping Grouping
            {
                set;
                get;
            }

            public List<GetListsAndSubWebsCommand.DocumentLibrary> Libraries
            {
                set;
                get;
            }

            public RequiredPermissionEnum RequiredPermission
            {
                set;
                get;
            }

            public bool IsOneDrive
            {
                set;
                get;
            }

            public DocumentLibrariesFromServiceCriteria(List<GetListsAndSubWebsCommand.DocumentLibrary> libraries, RequiredPermissionEnum requiredPermission, Grouping grouping, bool isOneDrive)
            {
                Libraries = libraries;
                RequiredPermission = requiredPermission;
                Grouping = grouping;
                IsOneDrive = isOneDrive;
            }

        }
    }
}
