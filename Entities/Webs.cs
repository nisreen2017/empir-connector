﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;
using EmpirConnector.Entities;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class Webs: ReadOnlyListBase<Webs, Web>
    {
         #region Authorization Rules

        private static void AddObjectAuthorizationRules()
        {
            // TODO: add authorization rules
            //AuthorizationRules.AllowGet(typeof(ReadOnlyList), "Role");
        }

        #endregion

        #region Factory Methods

        public static Webs GetWebs(WebsCriteria criteria)
        {
            return DataPortal.FetchChild<Webs>(criteria);
        }

        public static Webs NewWebs()
        {
            return DataPortal.FetchChild<Webs>();
        }

        private Webs()
        { 
        }


        #endregion


        #region Data Access

        private void Child_Fetch()
        {
        }

        private void Child_Fetch(WebsCriteria criteria)
        {
            RaiseListChangedEvents = false;
            IsReadOnly = false;

            foreach (GetListsAndSubWebsCommand.SubWeb web in criteria.WebCollection)
            {
                Add(Web.GetWeb(web, criteria.Grouping));
            }

            IsReadOnly = true;
            RaiseListChangedEvents = true;
        }

        private bool IsRoomInGovernance(Grouping grouping, string webTemplate)
        {
            return grouping.TemplateID == "Förvaltning" &&
                    webTemplate.ToLower().Contains("room");
        }

        private bool IncludeByTemplate(Microsoft.SharePoint.Client.Web web, Grouping grouping)
        {

   
            PropertyValues propValues = web.AllProperties;

            if (propValues.FieldValues.ContainsKey("WebTemplate"))
            {
                string webTemplate = propValues.FieldValues["WebTemplate"].ToString().ToLower();

                bool res = grouping.Groupings.ToLower().Split(';').
                    Where(s => s == webTemplate).FirstOrDefault() != null;

                if (web.Title == "Administration")
                    return true;
                else
                    return res;


            }
            else
                return false;
        }

        #endregion

        public class WebsCriteria : CriteriaBase<WebsCriteria>
        {

            public List<GetListsAndSubWebsCommand.SubWeb> WebCollection
            {
                get;
                set;
            }

            public Grouping Grouping
            {
                get;
                set;
            }

            public WebsCriteria(List<GetListsAndSubWebsCommand.SubWeb> webCollection, Grouping grouping)
            {
                WebCollection = webCollection;
                Grouping = grouping;
            }
        }
    }
}
