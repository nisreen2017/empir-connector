﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;
using System.IO;
using EmpirConnector.Forms;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class File : ReadOnlyBase<File>
    {
        public static readonly PropertyInfo<string> NameProperty = RegisterProperty<string>(p => p.Name);
        public string Name
        {
            get { return GetProperty(NameProperty); }
            set { LoadProperty(NameProperty, value); }
        }

        public static readonly PropertyInfo<string> AuthorProperty = RegisterProperty<string>(p => p.Author);
        public string Author
        {
            get { return GetProperty(AuthorProperty); }
            set { LoadProperty(AuthorProperty, value); }
        }

        public static readonly PropertyInfo<DateTime> ModifiedProperty = RegisterProperty<DateTime>(p => p.Modified);
        public DateTime Modified
        {
            get { return GetProperty(ModifiedProperty); }
            set { LoadProperty(ModifiedProperty, value); }

        }

        public static readonly PropertyInfo<string> UrlProperty = RegisterProperty<string>(p => p.Url);
        public string Url
        {
            get { return GetProperty(UrlProperty); }
            set { LoadProperty(UrlProperty, value); }
        }

        public static readonly PropertyInfo<bool> IntelligentProperty = RegisterProperty<bool>(p => p.Intelligent);
        public bool Intelligent
        {
            get { return GetProperty(IntelligentProperty); }
            set { LoadProperty(IntelligentProperty, value); }
        }

        public static string GetTemplateFilenameInEmpirDir(string filename)
        {
            if (!Directory.Exists(Constants.EMPIR_TEMPLATE_DIR))
                Directory.CreateDirectory(Constants.EMPIR_TEMPLATE_DIR);

            foreach (DirectoryInfo di in new DirectoryInfo(Constants.EMPIR_TEMPLATE_DIR).GetDirectories())
            {
                try
                {
                    di.Delete(true);
                }
                catch (Exception ex)
                { }
            }

            string tempDir = Path.Combine(Constants.EMPIR_TEMPLATE_DIR, System.Guid.NewGuid().ToString());
            Directory.CreateDirectory(tempDir);

            string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(filename);
            string extension = Path.GetExtension(filename);

            string template = Path.Combine(tempDir, fileNameWithoutExtension + extension);

            return template;
        }

        //internal static File GetFile(EmpirConnector.WebReference1.FileInfo childData, string folderUrl)
        //{
        //    return DataPortal.FetchChild<File>(new FileCriteria(childData, folderUrl));
        //}

        internal static File GetFile(Microsoft.SharePoint.Client.File childData, string folderUrl)
        {
            return DataPortal.FetchChild<File>(new FileCriteria(childData, folderUrl));
        }

        private void Child_Fetch(FileCriteria criteria)
        {

            LoadProperty(NameProperty, criteria.ListItem.Name);
            LoadProperty(AuthorProperty, criteria.ListItem.ModifiedBy.Title);

            DateTime modified = (DateTime.SpecifyKind(DateTime.Parse(criteria.ListItem.TimeLastModified.ToString()), DateTimeKind.Utc)).ToLocalTime();
            LoadProperty(ModifiedProperty, modified);
            LoadProperty(UrlProperty, criteria.FolderUrl + "/" + criteria.ListItem.Name);
            if(EmpirConnector.AddinModule.CurrentFunction != EmpirConnector.AddinModule.CurrentFunctionEnum.Templates)
                LoadProperty(IntelligentProperty, false); //criteria.ListItem.Intelligent
            else
                LoadProperty(IntelligentProperty, bool.Parse(criteria.ListItem.ListItemAllFields["Intelligent"].ToString()));
        }

        private class FileCriteria : CriteriaBase<FileCriteria>
        {
            public Microsoft.SharePoint.Client.File ListItem
            {
                set;
                get;
            }

            public string FolderUrl
            {
                set;
                get;
            }

            public FileCriteria(Microsoft.SharePoint.Client.File listItem, string folderUrl)
            {
                FolderUrl = folderUrl;
                ListItem = listItem;
            }

        }
    }
}
