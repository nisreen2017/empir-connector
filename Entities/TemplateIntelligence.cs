﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Sharepoint_Doc.Entities;

namespace EmpirConnector.Entities
{
    [Serializable]
    public class TemplateIntelligence : ReadOnlyBase<TemplateIntelligence>
    {
        public static readonly PropertyInfo<int> IDProperty = RegisterProperty<int>(p => p.ID);
        public int ID
        {
            get { return GetProperty(IDProperty); }
            private set { LoadProperty(IDProperty, value); }
        }

        public static readonly PropertyInfo<string> TitleProperty = RegisterProperty<string>(p => p.Title);
        public string Title
        {
            get { return GetProperty(TitleProperty); }
            private set { LoadProperty(TitleProperty, value); }
        }

        public bool IsCorrectlyPrepared
        {
            get
            {
                string webUrl = AddInGlobals.ConnectorSettings.GroupingListWebUrl;
                DAL.TemplateIntelligenceDAL dal = new DAL.TemplateIntelligenceDAL();
                return dal.IsGovernanceCorrectlyPrepared(webUrl, this.ID);
            }
        }

        public static readonly PropertyInfo<byte[]> FooterProperty = RegisterProperty<byte[]>(p => p.Footer);
        public byte[] Footer
        {
            get
            {
                if (!FieldManager.FieldExists(FooterProperty))
                {
                    string webUrl = AddInGlobals.ConnectorSettings.GroupingListWebUrl;
                    DAL.TemplateIntelligenceDAL dal = new DAL.TemplateIntelligenceDAL();
                    Footer = dal.GetAttachments(webUrl, this.Title, DAL.TemplateIntelligenceDAL.PartType.Footer);
                }

                return GetProperty(FooterProperty);
            }
            private set
            {
                LoadProperty(FooterProperty, value);
            }
        }

        public static readonly PropertyInfo<byte[]> LogoProperty = RegisterProperty<byte[]>(p => p.Logo);
        public byte[] Logo
        {
            get
            {
                if (!FieldManager.FieldExists(LogoProperty))
                {
                    string webUrl=  AddInGlobals.ConnectorSettings.GroupingListWebUrl;
                    DAL.TemplateIntelligenceDAL dal = new DAL.TemplateIntelligenceDAL();
                    Logo = dal.GetAttachments(webUrl, this.Title, DAL.TemplateIntelligenceDAL.PartType.Logo);
                }

                return GetProperty(LogoProperty);
            }
            private set
            {
                LoadProperty(LogoProperty, value);
            }
        }

        internal static TemplateIntelligence GetTemplateIntelligence(Microsoft.SharePoint.Client.ListItem childData)
        {
            return DataPortal.FetchChild<TemplateIntelligence>(childData);
        }

        private void Child_Fetch(Microsoft.SharePoint.Client.ListItem listItem)
        {
            LoadProperty(IDProperty,listItem.Id);
            LoadProperty(TitleProperty, listItem["Title"].ToString());
            //LoadProperty(FooterProperty, listItem["Sidfot"].ToString());
        }
    }
}
