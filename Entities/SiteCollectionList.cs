﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Entities;
using EmpirConnector.Entities;


namespace Empir.OfficeConnector.Entities
{
    [Serializable]
    public class SiteCollectionList: ReadOnlyListBase<SiteCollectionList, SiteCollection>
    {
        public static SiteCollectionList GetList(Grouping grouping)
        {
            return DataPortal.FetchChild<SiteCollectionList>(grouping);
        }

        public static SiteCollectionList GetList(string groupingsToServiceList, string webAppsToServiceList, bool writePermission)
        {
            SiteCollectionCriteria criteria = new SiteCollectionCriteria(groupingsToServiceList, webAppsToServiceList, writePermission);
            return DataPortal.FetchChild<SiteCollectionList>(criteria);
        }

          private SiteCollectionList()
        { /* require use of factory methods */ }



          private void Child_Fetch(SiteCollectionCriteria criteria)
        {
           
          RaiseListChangedEvents = false;
          IsReadOnly = false;


          using (EmpirConnector.WebReference1.EmpirConnectorServiceWS client = new EmpirConnector.WebReference1.EmpirConnectorServiceWS())
          {
              client.Url = AddInGlobals.ConnectorSettings.ServiceUrl;
              client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
              EmpirConnector.WebReference1.SiteCollection[] list = client.GetSiteCollections(criteria.WebAppsToService, criteria.GroupingsToService, criteria.WritePermission);

              foreach (EmpirConnector.WebReference1.SiteCollection siteCollection in list)
              {
                  Add(SiteCollection.GetSiteCollection(siteCollection));
              }
          }

          IsReadOnly = true;
          RaiseListChangedEvents = true;
        }


        private void Child_Fetch(Grouping grouping)
        {
           
          RaiseListChangedEvents = false;
          IsReadOnly = false;


          using (EmpirConnector.WebReference1.EmpirConnectorServiceWS client = new EmpirConnector.WebReference1.EmpirConnectorServiceWS())
          {
              client.Url = AddInGlobals.ConnectorSettings.ServiceUrl;
              client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
              EmpirConnector.WebReference1.SiteCollection[] list = client.GetSiteCollections(grouping.WebApplikation, grouping.Groupings, false);

              foreach (EmpirConnector.WebReference1.SiteCollection siteCollection in list)
              {
                  Add(SiteCollection.GetSiteCollection(siteCollection));
              }
          }

          IsReadOnly = true;
          RaiseListChangedEvents = true;
        }

        private class SiteCollectionCriteria : CriteriaBase<SiteCollectionCriteria>
        {
            public string GroupingsToService
            {
                set;
                get;
            }

            public string WebAppsToService
            {
                set;
                get;
            }

            public bool WritePermission
            {
                set;
                get;
            }

            public SiteCollectionCriteria(string groupingsToServiceList, string webAppsToServiceList, bool writePermission)
            {
                GroupingsToService = groupingsToServiceList;
                WebAppsToService = webAppsToServiceList;
                WritePermission = writePermission;
            }
        }
    }

}
