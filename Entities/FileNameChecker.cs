﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EmpirConnector.Entities
{
    public class FileNameChecker
    {
        public static bool CheckName(string line)
        {
            Char[] invalidChars = Path.GetInvalidFileNameChars();

            bool ok = true;

            foreach (char a in line)
            {
                foreach (char c in invalidChars)
                {
                    if (a.CompareTo(c) == 0)
                    {
                        ok = false;
                    }
                }
            }

            return ok;

        }
    }
}
