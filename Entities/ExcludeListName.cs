﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Sharepoint_Doc.Entities;

namespace EmpirConnector.Entities
{
    [Serializable]
    public class ExcludeListName : ReadOnlyBase<ExcludeListName>
    {
        public static readonly PropertyInfo<int> IDProperty = RegisterProperty<int>(p => p.ID);
        public int ID
        {
            get { return GetProperty(IDProperty); }
            private set { LoadProperty(IDProperty, value); }
        }

        public static readonly PropertyInfo<string> TitleProperty = RegisterProperty<string>(p => p.Title);
        public string Title
        {
            get { return GetProperty(TitleProperty); }
            private set { LoadProperty(TitleProperty, value); }
        }

    
      

        internal static ExcludeListName GetExcludeListName(Microsoft.SharePoint.Client.ListItem childData)
        {
            return DataPortal.FetchChild<ExcludeListName>(childData);
        }

        private void Child_Fetch(Microsoft.SharePoint.Client.ListItem listItem)
        {
            LoadProperty(IDProperty, listItem.Id);
            LoadProperty(TitleProperty, listItem["Title"].ToString());
            //LoadProperty(FooterProperty, listItem["Sidfot"].ToString());
        }
    }
}
