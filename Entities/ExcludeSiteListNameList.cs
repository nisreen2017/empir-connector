﻿using Csla;
using Microsoft.SharePoint.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmpirConnector.Entities
{
    [Serializable]
    public class ExcludeSiteListNameList : ReadOnlyListBase<ExcludeSiteListNameList, ExcludeSiteListName>
    {
        public static ExcludeSiteListNameList GetList()
        {


            return DataPortal.Fetch<ExcludeSiteListNameList>();
        }

        private ExcludeSiteListNameList()
        { /* require use of factory methods */ }




        private void DataPortal_Fetch()
        {
            try
            {
                RaiseListChangedEvents = false;
                IsReadOnly = false;

                DAL.ExcludeSiteListNameDAL dal = new DAL.ExcludeSiteListNameDAL();

                ListItemCollection list = dal.GetList();


                foreach (ListItem listItem in list)
                {
                    Add(ExcludeSiteListName.Get(listItem));
                }
                IsReadOnly = true;
                RaiseListChangedEvents = true;
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(@"c:\temp\ConnectorLog.txt", ex.Message + Environment.NewLine + ex.StackTrace);
                throw;
            }
        }
    }
}