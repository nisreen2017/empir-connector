﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Outlook;
using EmpirConnector.Entities;

namespace Sharepoint_Doc.Entities
{
    

    public class AddInGlobals
    {
        private static string _lastSavedDocument;
        private static EmpirConnector.Forms.FormOpenDocument _frmOpen;

        public static ConnectorSettings ConnectorSettings
        {
            get;
            set;
        }

        public static ExcludeListNameList ExcludeListNameList
        {
            set;
            get;
        }

        public static ExcludeSiteListNameList ExcludeSiteListNameList
        {
            set;
            get;
        }

        public static EmpirConnector.Forms.FormOpenDocument FormOpenDocument
        {
            get
            {
                if (_frmOpen == null)
                    _frmOpen = new EmpirConnector.Forms.FormOpenDocument();

                return _frmOpen;
            }
        }


        public static Sharepoint_Doc.Controls.FileTypeComboBox.FillerBase.ExtensionAndFileTypes ExtensionAndFileTypes
        {
            set;
            get;
        }

        public enum ApplicationEnum
        {
            Word,
            Excel,
        }

        public static bool OpenAsTemplate
        {
            set;
            get;
        }

        public static ApplicationEnum HostApplicationName;

        public static Microsoft.Office.Interop.Word.Application WordApplication;

        public static Inspector MailInspector;

        public static string LastSavedDocument
        {
            get
            {
                return _lastSavedDocument.Replace("%20", " ");
            }
            set
            {
                _lastSavedDocument = value;
            }
        }

    }
}
