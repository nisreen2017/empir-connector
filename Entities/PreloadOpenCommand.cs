﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sharepoint_Doc.Entities;
using Empir.OfficeConnector.Entities;
using System.Threading.Tasks;
using System.Threading;
using Sharepoint_Doc.DAL;
using System.Xml.Serialization;
using System.IO;
using Csla.Serialization.Mobile;
using Csla;

namespace EmpirConnector.Entities
{
    public class PreloadCommand
    {
        const bool LOG_TO_FILE = false;

        object lockObject = new object();

        public bool BakgroundJob
        {
            get;
            set;
        }

        public int NumberOfThreads
        {
            set;
            get;
        }

        public int NumberOfLoadedThreads
        {
            set;
            get;
        }

        public bool WritePermission
        {
            set;
            get;
        }

        public bool Loaded
        {
            get;
            set;
        }

        private bool _updated = false;
        public bool Updated
        {
            get
            {
                return _updated;
            }
            set
            {
                _updated = value;
                if (UpdatedEvent != null)
                    UpdatedEvent();
            }
        }

        public long ByteSize
        {
            get;
            set;

        }
        public bool NetworkError
        {
            get;
            set;
        }

        public bool Error
        {
            get;
            set;
        }

        public string ErrorMessage
        {
            set;
            get;
        }

        public bool FromCache
        {
            set;
            get;
        }

        public delegate void UpdatedHandler();
        public event UpdatedHandler UpdatedEvent;


        public PreloadedWebsList _list = new PreloadedWebsList();
        private SiteCollectionList _sites;
        ManualResetEvent resetEvent = null;
        int taskCount = 0;

        public class LoadDirsClass
        {
            public int Index;
            public Web Web;
        }

        [Serializable]
        public class PreloadedWebs : Csla.BusinessBase<PreloadedWebs>
        {
            public static readonly PropertyInfo<string> TemplateIDProperty = RegisterProperty<string>(p => p.TemplateID);
            public string TemplateID
            {
                get { return GetProperty(TemplateIDProperty); }
                set { LoadProperty(TemplateIDProperty, value); }
            }

            public static readonly PropertyInfo<Grouping> GroupingProperty = RegisterProperty<Grouping>(p => p.Grouping);
            public Grouping Grouping
            {
                get { return GetProperty(GroupingProperty); }
                set { LoadProperty(GroupingProperty, value); }
            }

            public static readonly PropertyInfo<PreloadedWebList> WebsProperty = RegisterProperty<PreloadedWebList>(p => p.Webs);
            public PreloadedWebList Webs
            {
                get { return GetProperty(WebsProperty); }
                set { LoadProperty(WebsProperty, value); }
            }

            public static readonly PropertyInfo<string> UrlProperty = RegisterProperty<string>(p => p.Url);
            public string Url
            {
                get { return GetProperty(UrlProperty); }
                set { LoadProperty(UrlProperty, value); }
            }
            #region Child Data Access

            protected override void Child_Create()
            {

            }

            private void Child_Fetch()
            {


            }

            private void Child_Insert()
            {


            }

            private void Child_Update()
            {

            }


            #endregion


        }

        private delegate void SubSiteHandler(Grouping grouping, PreloadedWebs pre);

        public bool CheckNetworkConnection()
        {
            try
            {
                string hostName = AddInGlobals.ConnectorSettings.RootUrl.Substring(8);
                System.Net.NetworkInformation.Ping ping = new System.Net.NetworkInformation.Ping();
                System.Net.NetworkInformation.PingReply reply = ping.Send(hostName);
                return true;
            }
            catch (Exception ex)
            {

                NetworkError = true;
            }

            return false;
        }

        public void Execute()
        {
            try
            {

               
                if (CheckNetworkConnection())
                {
                    bool FromCache = GetFromCache();
                    if (!FromCache || BakgroundJob)
                        GetAllSubSites();
                }
            }
            catch (Exception ex)
            {
                Error = true;
                ErrorMessage = ex.Message;
            }
        }


        private bool GetFromCache()
        {

            try
            {
                using (EmpirConnector.WebReference1.EmpirConnectorServiceWS client = new EmpirConnector.WebReference1.EmpirConnectorServiceWS())
                {
                    client.Url = AddInGlobals.ConnectorSettings.ServiceUrl;

                    lock (lockObject)
                    {
                        client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                        byte[] data = null;
                        if (WritePermission)
                            data = client.GetCahedData(WebReference1.CacheType.Save);
                        else
                            data = client.GetCahedData(WebReference1.CacheType.Open);


                        if (data != null)
                        {
                            this.ByteSize = data.Length;
                            try
                            {
                                this._list = (PreloadedWebsList)MobileFormatter.Deserialize(data);
                                Loaded = true;
                                FromCache = true;
                            }
                            catch (Exception ex)
                            {
                                return false;
                            }
                        }


                        return data != null;
                    }
                }
            }
            catch(Exception ex)
            {
                throw;
            }
 
        }

        private void UpdateCache()
        {


            lock (lockObject)
            {
                using (EmpirConnector.WebReference1.EmpirConnectorServiceWS client = new EmpirConnector.WebReference1.EmpirConnectorServiceWS())
                {
                    client.Url = AddInGlobals.ConnectorSettings.ServiceUrl;

                    byte[] data = MobileFormatter.Serialize(_list);

                    PreloadedWebsList ll = (PreloadedWebsList)MobileFormatter.Deserialize(data);
                    int xx = ll.Count();

                    client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;

                    try
                    {
                        if (WritePermission)
                            client.Cache(data, WebReference1.CacheType.Save);
                        else
                            client.Cache(data, WebReference1.CacheType.Open);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                 

                    if (BakgroundJob)
                    {
                        if (WritePermission)
                            AddinModule.PreloadSaveCommand._list = ll;
                        else
                            AddinModule.PreloadOpenCommand._list = ll;
                    }

                    Loaded = true;

                    if (this.WritePermission)
                    {
                        //AddinModule.PreloadSaveCommand.ByteSize > -1 && 
                        //if (data.Length != AddinModule.PreloadSaveCommand.ByteSize)
                        AddinModule.PreloadSaveCommand.Updated = true;

                        AddinModule.PreloadSaveCommand.ByteSize = data.Length;

                    }
                    else
                    {
                        //AddinModule.PreloadOpenCommand.ByteSize > 0 && 
                        //if (data.Length != AddinModule.PreloadOpenCommand.ByteSize)
                        AddinModule.PreloadOpenCommand.Updated = true;

                        AddinModule.PreloadOpenCommand.ByteSize = data.Length;
                    }

                }
            }
        }

        private void GetAllSubSites()
        {

            try
            {
                _list = new PreloadedWebsList();

                Templates templates = null;
                SiteCollectionList sites = null;


                string logFile = "";

                var dl = new WebDAL();
                var rw = dl.GetWeb(AddInGlobals.ConnectorSettings.RootUrl);

                var mainWebs = dl.GetSubWebs(rw, rw.Url);    //13 + min sida = 14

                //if (true || !this.WritePermission) //|| HasWritePermissionsInWebCommand.Execute(rw)  // added by me, Min Sida...
                //{
                    foreach (var mainWebSP in mainWebs)
                    {

                        var mainWeb = _list.AddNew();
                        mainWeb.TemplateID = mainWebSP.Title;
                        mainWeb.Url = mainWebSP.Url;
                        mainWeb.Webs = new PreloadedWebList();

                        Microsoft.SharePoint.Client.WebCollection subWebs;

                        try
                        {
                            subWebs = dl.GetSubWebs(mainWebSP, mainWebSP.Url);
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.Contains("Begäran använder"))// "ServerErrorCode = -2146232832")
                            continue;
                            else
                                throw ex;
                        }

                        foreach (var subWeb in subWebs)
                        {
                            if (!this.WritePermission || HasWritePermissionsInWebCommand.Execute(subWeb))
                            {
                                Web newSubWeb = mainWeb.Webs.AddNew();
                                newSubWeb.Url = AddInGlobals.ConnectorSettings.RootUrl + subWeb.ServerRelativeUrl;
                                newSubWeb.Title = subWeb.Title;

                                newSubWeb.ServerRelativeUrl = subWeb.ServerRelativeUrl;
                            }
                        }
                    //}
                }

                UpdateCache();

                Loaded = true;
            }
            catch(Exception ex)
            {
                throw ex;
            }
           
        }

        private void GetGroupingsFromService(out Templates templates, out SiteCollectionList sites)
        {


            GroupingsList groupingList = GroupingsList.GetList();
            templates = new Templates();

            string groupingsToServiceList = string.Empty;
            var groupings = groupingList.GroupBy(s => s.Groupings).Select(g => g.First()).ToList();
            foreach (Grouping grouping in groupings)
            {
                if (grouping.Groupings != string.Empty)
                {
                    if (groupingsToServiceList != "")
                        groupingsToServiceList += ";";

                    groupingsToServiceList += grouping.Groupings;
                }
            }

            foreach (Grouping grouping in groupingList)
            {
                templates.AddTemplate(grouping.TemplateID, grouping.Groupings);

            }


            string webAppsToServiceList = string.Empty;
            var webApps = groupingList.GroupBy(s => s.WebApplikation).Select(g => g.First()).ToList();
            foreach (Grouping webApp in webApps)
            {
                if (webAppsToServiceList != "")
                    webAppsToServiceList += ";";

                webAppsToServiceList += webApp.WebApplikation;
            }

            sites = SiteCollectionList.GetList(groupingsToServiceList, webAppsToServiceList, WritePermission);

            foreach (var siteCollection in sites)
            {
                string userName = ConnectorUser.Username; // System.Security.Principal.WindowsIdentity.GetCurrent().Name.Split('\\')[1];

                if (siteCollection.Url.ToLower().Contains("minsida") &&
                    !siteCollection.Url.ToLower().Contains(userName))
                    continue;

                foreach (Grouping grouping in groupings)
                {

                    if ((grouping.Groupings == siteCollection.Template &&
                        grouping.WebApplikation == siteCollection.WebApplication))
                    {


                        PreloadedWebs pre = _list.Where(s => s.TemplateID == grouping.TemplateID).FirstOrDefault();
                        if (pre == null)
                        {
                            pre = _list.AddNew();
                            pre.Webs = new PreloadedWebList();
                            pre.TemplateID = grouping.TemplateID;
                            pre.Grouping = grouping;
                            // _list.Add(pre);
                        }
                    }
                }
            }
        }

        private void AddToLog(string logFile, string text)
        {
            if (!LOG_TO_FILE)
                return;

            System.IO.File.AppendAllText(logFile, text + System.Environment.NewLine);

        }

        private void LoadDirectories(object o)
        {
            LoadDirsClass loadDirs = ((LoadDirsClass)o);

            loadDirs.Web.LoadingRead = !this.WritePermission;
            int dummy = loadDirs.Web.DocumentLibraries.Count;

            //foreach (Web web in loadDirs)
            //{
            //    int dummy = web.DocumentLibraries.Count;
            //}

            //if (Interlocked.Decrement(ref taskCount) == 0)
            //    resetEvent.Set();
        }

        private void GetGroupingSite(Grouping grouping, PreloadedWebs pre)
        {

            if (_sites == null)
                _sites = SiteCollectionList.GetList(grouping);

            foreach (SiteCollection siteCollection in _sites)
            {


                lock (lockObject)
                {

                    Web web = Web.GetWeb(siteCollection.Url);
                    web.Grouping = grouping;
                    web.Url = siteCollection.Url;


                    pre.Webs.Add(web);
                }

                ////Task task = new Task(delegate
                ////{

                //        Web web = Web.GetWeb(siteCollection.Url);
                //        //lock (lockObject)
                //        //{
                //        web.Grouping = grouping;
                //        web.Url = siteCollection.Url;
                //        lock (lockObject)
                //        {
                //            pre.Webs.Add(web);
                ////        }
                ////    }

                ////});

                ////task.Start();

            }

            NumberOfLoadedThreads++;

        }

        private class Templates
        {
            public enum GroupingEnum
            {
                Stadsportalen = 0,
                Forvaltning = 1,
                Rum = 2,
                MinSida = 3
            }

            private List<string>[] _groupings = new List<string>[4];

            public Templates()
            {
                for (int i = 0; i < 4; i++)
                    _groupings[i] = new List<string>();
            }

            public void AddTemplate(string group, string template)
            {
                template = template.ToLower();

                switch (group.ToLower())
                {
                    case "stadsportalen":
                        _groupings[(int)GroupingEnum.Stadsportalen].Add(template);
                        break;
                    case "förvaltning":
                        _groupings[(int)GroupingEnum.Forvaltning].Add(template);
                        break;
                    case "mina rum":
                        _groupings[(int)GroupingEnum.Rum].Add(template);
                        break;
                }
            }

            public bool IsGroupTemplate(GroupingEnum group, string template)
            {
                if (template.Contains("."))
                    template = template.Split(".".ToCharArray())[1];

                return _groupings[(int)group].Where(g => g == template.ToLower()).FirstOrDefault() != null;
            }
        }
    }
}
