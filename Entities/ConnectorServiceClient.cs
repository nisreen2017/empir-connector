﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmpirConnector.Entities
{
    public class EmpirConnectorServiceClient
    {
        public static EmpirConnector.ServiceReference1.ConnectorServiceClient GetClient()
        {
            System.ServiceModel.WSHttpBinding binding = new System.ServiceModel.WSHttpBinding(System.ServiceModel.SecurityMode.Message);
            System.ServiceModel.EndpointAddress adress = new System.ServiceModel.EndpointAddress("http://172.29.253.124/EmpirConnectorService/ConnectorService.svc");

            return new ServiceReference1.ConnectorServiceClient(binding, adress);
        }
    }
}
