﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class Folders : BusinessListBase<Folders, Folder>
    {
        private string[] _builtInFolders = {"Forms"};

        public static Folders GetFolders(string listId, string parentWebUrl, string webApplication, Sharepoint_Doc.DAL.ListDAL.SharePointFolder folder)
        {
            return DataPortal.FetchChild<Folders>(new SubFoldersCriteria(listId, parentWebUrl, folder, webApplication));
        }

        public static Folders GetFolders(string listId, string parentWebUrl, string webApplication)
        {
            return DataPortal.FetchChild<Folders>(new FoldersCriteria(listId, parentWebUrl, webApplication, false));
        }

        public static Folders GetTemplateFolders(string listId, string parentWebUrl, string webApplication)
        {
            return DataPortal.FetchChild<Folders>(new FoldersCriteria(listId, parentWebUrl, webApplication, true));
        }

        #region Data Access

        private void Child_Fetch(FoldersCriteria criteria)
        {
            RaiseListChangedEvents = false;
            //IsReadOnly = false;

            DAL.ListDAL dal = new DAL.ListDAL();
            Sharepoint_Doc.DAL.ListDAL.SharePointFolder sharePointFolder  = dal.LoadFolders(criteria.ListId, criteria.ParentWebUrl, criteria.DefaultView);

            Folder folderToAdd = Folder.GetFolder(sharePointFolder, criteria.ListId, criteria.ParentWebUrl, criteria.WebApplication);
            Add(folderToAdd);


            //foreach (Microsoft.SharePoint.Client.Folder folder in list.RootFolder.Folders)
            //{
            //    if (NotBuiltIn(folder.Name))
            //    {
            //        Folder folderToAdd = Folder.GetFolder(folder, criteria.ListId, criteria.ParentWebUrl);
            //        Add(folderToAdd);
            //    }
            //}

            //IsReadOnly = true;
            RaiseListChangedEvents = true;
        }

        private void Child_Fetch(SubFoldersCriteria criteria)
        {
            RaiseListChangedEvents = false;
            //IsReadOnly = false;


            try
            {

                foreach (Sharepoint_Doc.DAL.ListDAL.SharePointFolder folder in criteria.Folder.Folders)
                {
                    if (NotBuiltIn(folder.Name))
                        Add(Folder.GetFolder(folder, criteria.ListId, criteria.ParentWebUrl, criteria.WebApplication));
                }
            }
            catch
            {}


            //IsReadOnly = true;
            RaiseListChangedEvents = true;
        }


        private bool NotBuiltIn(string name)
        {
            return true;
            //!name.StartsWith("_") &&
            // _builtInFolders.Where(s => s == name).FirstOrDefault() ==null;

            //bool exclude = AddInGlobals.ExcludeListNameList.Where(s => s.Title.ToLower() == name.ToLower()).FirstOrDefault() != null;
            //return exclude;
        }
        #endregion


        public class FoldersCriteria : CriteriaBase<FoldersCriteria>
        {
            public bool DefaultView
            {
                get;
                set;
            }

            public string ListId
            {
                set;
                get;
            }

            public string ParentWebUrl
            {
                set;
                get;
            }

            public string WebApplication
            {
                set;
                get;
            }

            public FoldersCriteria(string listId, string parentWebUrl, string webApplication, bool defaultView)
            {
                ListId = listId;
                ParentWebUrl = parentWebUrl;
                WebApplication = webApplication;
                DefaultView = defaultView;
            }

        }


        public class SubFoldersCriteria : CriteriaBase<SubFoldersCriteria>
        {
            public string ListId
            {
                set;
                get;
            }

            public string ParentWebUrl
            {
                set;
                get;
            }

            public string WebApplication
            {
                set;
                get;
            }

            public Sharepoint_Doc.DAL.ListDAL.SharePointFolder Folder
            {
                set;
                get;
            }

            public SubFoldersCriteria(string listId, string parentWebUrl, Sharepoint_Doc.DAL.ListDAL.SharePointFolder folder, string webApplication)
            {
                Folder = folder;
                ListId = listId;
                ParentWebUrl = parentWebUrl;
                WebApplication = webApplication;
            }

        }
    }

    
}
