﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Word;
using Sharepoint_Doc.DAL;
using System.Reflection;
using Sharepoint_Doc.Entities;

namespace EmpirConnector.Entities.Office
{
    public class WordDocument: OfficeDocument
    {
        private string _tempFilename = "";

        public override string TempFilename
        {
            get
            {
                return _tempFilename;
            }
        }

        public override void Open(string fullname, OpenModeEnum openMode)
        {
            object missing = Type.Missing;

            switch (System.IO.Path.GetExtension(fullname).ToLower())
            {
                case ".pdf":
                case ".xml":
                case ".mht":
                case ".mhtml":
                case ".xps":
                    System.Diagnostics.Process.Start(fullname);
                    break;
                //case ".xps":
                //    MessageBi
                default:

    
                 

                    Document doc = null;
                    if (openMode == OpenModeEnum.ReadOnly)
                    {
                        doc = AddinModule.CurrentInstance.WordApp.Documents.Open(fullname, missing, true);
                        doc.Protect(WdProtectionType.wdAllowOnlyReading);
                    }
                    else
                        doc = AddinModule.CurrentInstance.WordApp.Documents.Open(fullname);

                    

                            break;
            }
        }

        public override void Save(string fullname, string internalFileType)
        {
            object wdFormat = WdSaveFormat.wdFormatDocument;
            object missing = Type.Missing;

            fullname = fullname.Replace(" ", "%20");

            string extension =  System.IO.Path.GetExtension(fullname);
            switch (extension.ToLower())
            {
                case ".docx":
                    wdFormat = 12;
                    break;
                case ".docm":
                    wdFormat = WdSaveFormat.wdFormatXMLDocumentMacroEnabled;
                    break;
                case ".dotx":
                    wdFormat = WdSaveFormat.wdFormatXMLTemplate;
                    break;
                case ".dotm":
                    wdFormat = WdSaveFormat.wdFormatXMLTemplateMacroEnabled;
                    break;
                case ".doc":
                    wdFormat = WdSaveFormat.wdFormatDocument97;
                    break;
                case ".dot":
                    wdFormat = WdSaveFormat.wdFormatTemplate97;
                    break;
                case ".rtf":
                    wdFormat = WdSaveFormat.wdFormatRTF;
                    break;
                case ".pdf":
                    wdFormat = WdSaveFormat.wdFormatPDF;
                    break;
                case ".mhtml":
                case ".mht":
                    wdFormat = WdSaveFormat.wdFormatWebArchive;
                    break;
                case ".xps":
                    wdFormat = WdSaveFormat.wdFormatXPS;
                    break;
                case ".htm":
                    switch (internalFileType)
                    {
                        case "htm":
                            wdFormat = WdSaveFormat.wdFormatHTML;
                            break;
                        case "htm2":
                            wdFormat = WdSaveFormat.wdFormatFilteredHTML;
                            break;
                    }
                    break;
                case ".txt":
                    wdFormat = WdSaveFormat.wdFormatText;
                    break;
                case ".xml":
                    switch (internalFileType)
                    {
                        case "xml":
                            wdFormat = WdSaveFormat.wdFormatFlatXML;
                            break;
                        case "xml2":
                            wdFormat = WdSaveFormat.wdFormatXML;
                            break;
                    }
                    break;
                case ".odt":
                    wdFormat = WdSaveFormat.wdFormatOpenDocumentText;
                    break;
                case ".wps":
                    wdFormat = 100;
                    break;
        }


            _tempFilename = Guid.NewGuid().ToString() +  System.IO.Path.GetExtension(fullname);

            string ff = System.IO.Path.GetTempPath() + _tempFilename;

            AddinModule.CurrentInstance.WordApp.ActiveDocument.SaveAs(System.IO.Path.GetTempPath() + _tempFilename, wdFormat, ref missing, ref missing, false);


        }

        public override void Close()
        {

            object wdSaveOptions = WdSaveOptions.wdDoNotSaveChanges;
            //object wdOriginalFormat = Word.WdOriginalFormat.wdWordDocument; //works only in SP1
            object wdOriginalFormat = WdOriginalFormat.wdOriginalDocumentFormat;
            object missing = Type.Missing;


            AddinModule.CurrentInstance.WordApp.ActiveDocument.Close(ref wdSaveOptions, ref wdOriginalFormat, ref missing);
        }

        public override void Add(string fullname)
        {
            Document doc =  AddinModule.CurrentInstance.WordApp.Documents.Add(fullname);
            doc.set_AttachedTemplate("");

            doc.Activate();
        }
    }
}
