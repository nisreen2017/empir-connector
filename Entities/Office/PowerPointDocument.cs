﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Sharepoint_Doc.Entities;
using Microsoft.Office.Interop.PowerPoint;
using Microsoft.Office.Core;

namespace EmpirConnector.Entities.Office
{
    public class PowerPointDocument: OfficeDocument
    {
        private string _tempFilename = "";

        public override string TempFilename
        {
            get
            {
                return _tempFilename;
            }

        }
        public override void Open(string fullname, OpenModeEnum openMode)
        {

            switch(Path.GetExtension(fullname).ToLower())
            {
                case ".pdf":
                case ".xml":
                case ".htm":
                case ".gif":
                case ".jpg":
                case ".tif":
                case ".bmp":
                case ".png":
                case ".wmf":
                case ".emf":
                    System.Diagnostics.Process.Start(fullname);
                    break;
                default:
                    if (openMode == OpenModeEnum.ReadOnly)
                        AddInGlobals.FormOpenDocument.OpenDocument(fullname);
                    else
                        AddinModule.CurrentInstance.PowerPointApp.Presentations.Open2007(fullname, Microsoft.Office.Core.MsoTriState.msoFalse);
                    break;
            }
        }

        public override void Save(string fullname, string internalFileType)
        {
            Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType ppFileType = 
                Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsDefault;


            string extension =  System.IO.Path.GetExtension(fullname);
            switch (extension.ToLower())
            {
                case ".pptx":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsDefault;
                    break;
                case ".pptm":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLPresentationMacroEnabled;
                    break;
                case ".ppt":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsPresentation;
                    break;
                case ".potx":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLTemplate;
                    break;
                case ".potm":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLTemplateMacroEnabled;
                    break;
                case ".pot":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsTemplate;
                    break;
                case ".pdf":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsPDF;
                    break;
                case ".xps":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsXPS;
                    break;
                case ".thmx":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLTheme;
                    break;
                case ".ppsm":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLShowMacroEnabled;
                    break;
                case ".pps":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLShow;
                    break;
                case ".ppam":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLAddin;
                    break;
                case ".ppa":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsAddIn;
                    break;
                case ".xml":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenXMLPresentation;
                    break;
                case ".mht":
                case "*.mhtml":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsWebArchive;
                    break;
                case ".htm":
                case ".html":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsHTML;
                    break;
                case ".gif":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsGIF;
                    break;
                case ".jpg":
                case ".jpeg":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsJPG;
                    break;
                case ".png":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsPNG;
                    break;
                case ".tif":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsTIF;
                    break;
                case ".bmp":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsBMP;
                    break;
                case ".wmf":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsMetaFile;
                    break;
                case ".emf":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsEMF;
                    break;
                case ".rtf":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsRTF;
                    break;
                case ".odp":
                    ppFileType = Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsOpenDocumentPresentation;
                    break;
            }

            _tempFilename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(fullname);

            switch (ppFileType)
            {
                case Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsGIF:
                case Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsJPG:
                case Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsTIF:
                case Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsBMP:
                case Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsPNG:
                case Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsMetaFile:
                case Microsoft.Office.Interop.PowerPoint.PpSaveAsFileType.ppSaveAsEMF:
                    AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.Slides[1].Export(System.IO.Path.GetTempPath() + _tempFilename, Path.GetExtension(fullname).Substring(1));
                    break;
                default:
                    AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.SaveAs(System.IO.Path.GetTempPath() + _tempFilename, ppFileType);
                    break;
            }
        }

        public override void Close()
        {
            AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.Close();
        }

        public override void Add(string fullname)
        {

            string templatePath = System.IO.Path.GetTempPath() + "template.pot";

            //var activePresentation = AddinModule.CurrentInstance.PowerPointApp.Presentations.Add();

            var source = AddinModule.CurrentInstance.PowerPointApp.Presentations.Open(fullname);
            source.SaveAs(templatePath, PpSaveAsFileType.ppSaveAsOpenXMLTemplate);
            source.Close();

            AddinModule.CurrentInstance.PowerPointApp.Presentations.Open(templatePath, MsoTriState.msoFalse, MsoTriState.msoCTrue, MsoTriState.msoCTrue);

            return;

            //int RGB = source.SlideMaster.Background.Fill.ForeColor.RGB;
            //source.Close();


            //AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.Slides.AddSlide(1, null);

            //AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.SlideMaster.Background.Fill.ForeColor.RGB = RGB;
            //AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.SlideMaster.Background.Fill.BackColor.RGB = RGB;
            //AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.ApplyTemplate(fullname);

            return;

       
            for(int index = 1; index<=source.Slides.Count; index++)
            {
                source.Slides[index].Copy();
                AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.Slides.Paste();
                AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.SlideMaster.BackgroundStyle = source.SlideMaster.BackgroundStyle;
               // object slide = AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.Slides.AddSlide(index, source.Slides[index].CustomLayout);
                //slide.BackgroundStyle = source.Slides[index].BackgroundStyle;
            }
         
            source.Close();
            source = null;

           // AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.Slides.InsertFromFile(fullname, 0);
            
           
        }
    }
}
