﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sharepoint_Doc.Entities;
using System.IO;
using Sharepoint_Doc.DAL;
using EmpirConnector.Forms;

namespace EmpirConnector.Entities.Office
{
    public class OutlookDocument : OfficeDocument
    {
        private string _tempFilename = "";

        public override string TempFilename
        {
            get
            {
                return SaveAttachmentForm.SelectedFilename;
            }

        }
        public static string ParentWebUrl
        {
            set;
            get;
        }

        public static string ListName
        {
            set;
            get;
        }

        public override void Open(string fullname, OpenModeEnum openMode)
        {

        }

        public override void Save(string fullname, string internalFileType)
        {


            //FileDAL dal = new FileDAL();
            //dal.SaveFileInSharePoint(ParentWebUrl, fullname, ListName); 
   
        }

        public override void Add(string fullname)
        {
            throw new NotImplementedException();
        }

        public override void Close()
        {

        }
    }
}
