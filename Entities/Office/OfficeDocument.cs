﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmpirConnector.Entities.Office
{
    public abstract class OfficeDocument
    {
        public enum OpenModeEnum
        {
            None,
            ReadOnly,
            Edit
        }

        public string Extension
        {
            set;
            get;
        }

        public abstract void Open(string fullname, OpenModeEnum openMode);
        public abstract void Save(string fullname, string internalFileType);
        public abstract void Add(string fullname);
        public abstract void Close();
        public abstract string TempFilename
        {
            get;
        }

        public static OfficeDocument GetDocument()
        {
            if (AddinModule.CurrentInstance.ExcelApp != null)
                return new ExcelDocument();
            else if (AddinModule.CurrentInstance.WordApp != null)
                return new WordDocument();
            else if (AddinModule.CurrentInstance.PowerPointApp != null)
                return new PowerPointDocument();
            else
                return new OutlookDocument();

        }

        public static bool  IsWordApp()
        {
            return AddinModule.CurrentInstance.WordApp != null;
        }
    }
}
