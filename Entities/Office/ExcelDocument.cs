﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sharepoint_Doc.Entities;
using Microsoft.Office.Interop.Excel;

namespace EmpirConnector.Entities.Office
{
    public class ExcelDocument: OfficeDocument
    {
        private string _tempFilename = "";

        public override string TempFilename
        {
            get
            {
                return _tempFilename;
            }
        }

        public override void Open(string fullname, OpenModeEnum openMode)
        {
            switch (System.IO.Path.GetExtension(fullname).ToLower())
            {
                case ".pdf":
                case ".xps":
                case ".mht":
                case ".mhtml":
                    System.Diagnostics.Process.Start(fullname);
                    break;
                default:
                    if (openMode == OpenModeEnum.ReadOnly)
                    {
                        Workbook wb = AddinModule.CurrentInstance.ExcelApp.Workbooks.Open(fullname, Type.Missing, true);
                    }
                    else
                        AddinModule.CurrentInstance.ExcelApp.Workbooks.Open(fullname);
                    break;
            }
        }

        public override void Save(string fullname, string internalFileType)
        {

            Microsoft.Office.Interop.Excel.XlFileFormat xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook;

            string extension = System.IO.Path.GetExtension(fullname);
            switch (extension.ToLower())
            {
                case ".xlsx":
                    xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbook;
                    break;
                case ".xlsm":
                    xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLWorkbookMacroEnabled;
                    break;
                case ".xlsb":
                    xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlExcel12;
                    break;
                case ".xls":
                    switch (internalFileType.ToLower())
                    {
                        case "xls":
                            xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlExcel8;
                            break;
                        case "xls2":
                            xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlExcel7;
                            break;
                    }
                    break;
                case ".xltx":
                    xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLTemplate;
                    break;
                case ".xltm":
                    xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenXMLTemplateMacroEnabled;
                    break;
                case ".xlt":
                    xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlTemplate8;
                    break;
                case ".txt":
                    switch (internalFileType.ToLower())
                    {
                        case "txt":
                            xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlTextPrinter;
                            break;
                        case "txt2":
                            xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlTextWindows;
                            break;
                        case "txt3":
                            xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlTextMac;
                            break;
                        case "txt4":
                            xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlTextMSDOS;
                            break;
                    }
                    break;

                case ".xml":

                    xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlXMLSpreadsheet;
                    break;
                case ".csv":
                    xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlCSV;
                    break;
                case ".prn":
                    xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlTextPrinter;
                    break;

                case ".ods":
                    xlFormat = xlFormat = Microsoft.Office.Interop.Excel.XlFileFormat.xlOpenDocumentSpreadsheet;
                    break;

            }

            _tempFilename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(fullname);

            switch (System.IO.Path.GetExtension(fullname).ToLower())
            {
                case ".pdf":
                    AddinModule.CurrentInstance.ExcelApp.ActiveWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypePDF, System.IO.Path.GetTempPath() + _tempFilename);
                    break;
                case ".xps":
                    AddinModule.CurrentInstance.ExcelApp.ActiveWorkbook.ExportAsFixedFormat(Microsoft.Office.Interop.Excel.XlFixedFormatType.xlTypeXPS, System.IO.Path.GetTempPath() + _tempFilename);
                    break;
                default:
                    AddinModule.CurrentInstance.ExcelApp.ActiveWorkbook.SaveAs(System.IO.Path.GetTempPath() + _tempFilename, xlFormat);
                    break;
            }


        }

        public override void Close()
        {
            AddinModule.CurrentInstance.ExcelApp.ActiveWorkbook.Close(false);
        }

        public override void Add(string fullname)
        {

            AddinModule.CurrentInstance.ExcelApp.Workbooks.Add(fullname);
          
        }

    }
}
