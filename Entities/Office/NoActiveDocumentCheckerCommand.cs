﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmpirConnector.Entities.Office
{
    public class NoActiveDocumentCheckerCommand
    {
        public static bool Execute()
        {

            if (AddinModule.CurrentInstance.WordApp != null)
                return AddinModule.CurrentInstance.WordApp.Documents.Count == 0;
            else if (AddinModule.CurrentInstance.ExcelApp != null)
                return AddinModule.CurrentInstance.ExcelApp.Workbooks.Count == 0;
            else if (AddinModule.CurrentInstance.PowerPointApp != null)
                return AddinModule.CurrentInstance.PowerPointApp.Presentations.Count == 0;
            else
                throw new InvalidOperationException();
        }
    }
}
