﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class Grouping : ReadOnlyBase<Grouping>
    {
        public static readonly PropertyInfo<string> TemplateIDProperty = RegisterProperty<string>(p => p.TemplateID);
        public string TemplateID
        {
            get { return GetProperty(TemplateIDProperty); }
            set { LoadProperty(TemplateIDProperty, value); }
        }

        public static readonly PropertyInfo<string> GroupingsProperty = RegisterProperty<string>(p => p.Groupings);
        public string Groupings
        {
            get { return GetProperty(GroupingsProperty); }
            set { LoadProperty(GroupingsProperty, value); }
        }

        public static readonly PropertyInfo<string> WebApplikationProperty = RegisterProperty<string>(p => p.WebApplikation);
        public string WebApplikation
        {
            get { return GetProperty(WebApplikationProperty); }
            set { LoadProperty(WebApplikationProperty, value); }
        }

        public static readonly PropertyInfo<bool> CollectionBasedProperty = RegisterProperty<bool>(p => p.CollectionBased);
        public bool CollectionBased
        {
            get { return GetProperty(CollectionBasedProperty); }
            set { LoadProperty(CollectionBasedProperty, value); }
        }

        internal static Grouping GetGrouping(Microsoft.SharePoint.Client.ListItem childData)
        {
            return DataPortal.FetchChild<Grouping>(childData);
        }

        private void Child_Fetch(Microsoft.SharePoint.Client.ListItem listItem)
        {
            LoadProperty(TemplateIDProperty, listItem["Gruppering"].ToString());
            string title = listItem["Title"].ToString();
            string[] titleSplit = listItem["Title"].ToString().Split('#');
            if(titleSplit.Length>1)
                LoadProperty(GroupingsProperty, titleSplit[1]);
            LoadProperty(WebApplikationProperty, listItem["Webbapplikation"].ToString());
            string collectionBased = listItem["CollectionBased"].ToString();
            LoadProperty(CollectionBasedProperty, bool.Parse(collectionBased));
        }
    }
}
