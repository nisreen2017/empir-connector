﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sharepoint_Doc.DAL;
using Microsoft.SharePoint.Client;

namespace EmpirConnector.Entities
{
    public class GetListsAndSubWebsCommand
    {

        public class SubWeb
        {
            public string Id;
            public string Title;
            public string ServerRelativeUrl;
            public string Template;
        }

        public class DocumentLibrary
        {
            public string Id;
            public string Title;
            public string ParentWebUrl;
            public string DefaultEditFormUrl;
            public bool ReadPermission;
            public bool WritePermission;
            public bool EnableFolderCreation;
        }

        public class WebData
        {
            public List<SubWeb> SubWebs;
            public List<DocumentLibrary> DocumentLibraries;

            public WebData()
            {
                this.SubWebs = new List<SubWeb>();
                this.DocumentLibraries = new List<DocumentLibrary>();
            }
        }

            //      ClientContext clientContext = new ClientContext(url);
            //Microsoft.SharePoint.Client.Web oWebsite = clientContext.Web;
            //clientContext.Load(oWebsite, w => w.Webs.Include(ww => ww.Id, ww => ww.Title, ww => ww.ServerRelativeUrl, ww => ww.Lists.Include(tt => tt.EffectiveBasePermissions, list => list.Id, list => list.Title, list => list.ParentWebUrl, list => list.EnableFolderCreation, ll => ll.DefaultEditFormUrl)),
            //    w=> w.Lists.Include(tt=>tt.EffectiveBasePermissions, list=>list.Id, list=>list.Title, list=>list.ParentWebUrl, list=>list.EnableFolderCreation, ll=>ll.DefaultEditFormUrl));
            //clientContext.ExecuteQuery();

        public static WebData Execute(string url, bool readPermission)
        {
            WebData webData = new WebData();
  
            WebDAL dal = new WebDAL();
            Sharepoint_Doc.DAL.WebDAL.SubWebsAndListsResult web = dal.GetSubs(url, readPermission, false);

            foreach (Microsoft.SharePoint.Client.Web w in web.SubWebs)
            {
  
                foreach (List l in w.Lists)
                {
                    if (l.EffectiveBasePermissions.Has(PermissionKind.AddListItems) || readPermission)
                    {
                        webData.SubWebs.Add(new SubWeb()
                            { Id=w.Id.ToString(), Title=w.Title, ServerRelativeUrl=w.ServerRelativeUrl});

                        //Todo
                        //webData.SubWebs.Add(new SubWeb()
                        //{ Id = w.Id.ToString(), Title = w.Title, ServerRelativeUrl = w.ServerRelativeUrl, Template = w.AllProperties["WebTemplate"].ToString() });

                        break;
                    }
                }
            }

            foreach(List list in web.Lists)
            {
                if(list.EffectiveBasePermissions.Has(PermissionKind.AddListItems) || readPermission)
                {
                    if (list.BaseType == BaseType.DocumentLibrary && !list.Hidden)
                    {
                        webData.DocumentLibraries.Add(new DocumentLibrary()
                        {
                            DefaultEditFormUrl = list.DefaultEditFormUrl,
                            EnableFolderCreation = list.EnableFolderCreation,
                            Id = list.Id.ToString(),
                            ParentWebUrl = list.ParentWebUrl,
                            Title = list.Title,
                            ReadPermission = list.EffectiveBasePermissions.Has(PermissionKind.OpenItems),
                            WritePermission = list.EffectiveBasePermissions.Has(PermissionKind.AddListItems)
                        });
                    }
                }
        
            }

            return webData;

        }
    }
}
