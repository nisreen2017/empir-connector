﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;

namespace EmpirConnector.Entities
{

    public class ExcludeListNameList : ReadOnlyListBase<ExcludeListNameList, ExcludeListName>
    {
        public static ExcludeListNameList GetList()
        {
  

          return DataPortal.Fetch<ExcludeListNameList>();
        }

        private ExcludeListNameList()
        { /* require use of factory methods */ }




        private void DataPortal_Fetch()
        {
            try
            {
                RaiseListChangedEvents = false;
                IsReadOnly = false;

                DAL.ExcludeListNameDAL dal = new DAL.ExcludeListNameDAL();

                ListItemCollection list = dal.GetList();

   
                foreach (ListItem listItem in list)
                {
                    Add(ExcludeListName.GetExcludeListName(listItem));
                }
                IsReadOnly = true;
                RaiseListChangedEvents = true;
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(@"c:\temp\ConnectorLog.txt", ex.Message + Environment.NewLine + ex.StackTrace);
                throw;
            }
        }
    }
}
