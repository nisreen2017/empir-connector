﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;

namespace EmpirConnector.Entities
{
    [Serializable]
    public class Governance : ReadOnlyBase<Governance>
    {
        public static readonly PropertyInfo<string> NameProperty = RegisterProperty<string>(p => p.Name);

        public string Name
        {
            get { return GetProperty(NameProperty); }
            private set { LoadProperty(NameProperty, value); }
        }

        internal static Governance GetGovernance(Sharepoint_Doc.DAL.ListDAL.SharePointFolder childData)
        {
            return DataPortal.FetchChild<Governance>(childData);
        }

        private void Child_Fetch(Sharepoint_Doc.DAL.ListDAL.SharePointFolder childData)
        {
            LoadProperty(NameProperty, childData.Name);
        }
    }
}
