﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using System.Threading;
using EmpirConnector.Entities;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class Folder : BusinessBase<Folder>
    {
        public static readonly PropertyInfo<string> NameProperty = RegisterProperty<string>(p => p.Name);
        public string Name
        {
            get { return GetProperty(NameProperty); }
            set { LoadProperty(NameProperty, value); }
        }

        public static readonly PropertyInfo<string> ServerRelativeUrlProperty = RegisterProperty<string>(p => p.ServerRelativeUrl);
        public string ServerRelativeUrl
        {
            get {
                return GetProperty(ServerRelativeUrlProperty);
                //if (IsOneDrive ||  this.ParentWebUrl.ToLower().Contains("minsida"))
                //{
                //    var fullUrl = GetProperty(ServerRelativeUrlProperty);
                //    string userName = ConnectorUser.Username;
                //    var result = fullUrl.ToLower().Substring(fullUrl.IndexOf(userName.ToLower()) + ConnectorUser.Username.Length);
                //    return result;
                //}
                //else

                //{ return GetProperty(ServerRelativeUrlProperty); }


            }
            set { LoadProperty(ServerRelativeUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> ParentWebUrlProperty = RegisterProperty<string>(p => p.ParentWebUrl);
        public string ParentWebUrl
        {
            get { return GetProperty(ParentWebUrlProperty); }
            set { LoadProperty(ParentWebUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> ListIdProperty = RegisterProperty<string>(p => p.ListId);
        public string ListId
        {
            get { return GetProperty(ListIdProperty); }
            set { LoadProperty(ListIdProperty, value); }

        }

        public static readonly PropertyInfo<string> WebApplicationProperty = RegisterProperty<string>(p => p.WebApplication);
        public string WebApplication
        {
            get
            {
                if (ParentWebUrl.ToLower().Contains("minsida"))
                    return AddInGlobals.ConnectorSettings.OneDriveUrl; // "http://minsidadev.trollhattan.se"; //AddInGlobals.ConnectorSettings.OneDriveUrl
                else
                    return GetProperty(WebApplicationProperty);
            } 
            set { LoadProperty(WebApplicationProperty, value); }

        }

        public static readonly PropertyInfo<Files> FilesProperty = RegisterProperty<Files>(p => p.Files);
        public Files Files
        {
            get
            {
                if (!FieldManager.FieldExists(FilesProperty))
                {
                    Files = Files.GetFiles(this.ServerRelativeUrl, this.ParentWebUrl, this.ListId, this.WebApplication);
                }

                return GetProperty(FilesProperty);
            }
            set
            {
                LoadProperty(FilesProperty, value);
            }

            
        }

        public void ForceReloadOfFiles()
        {
            FieldManager.GetRegisteredProperties().Remove(FilesProperty);
        }

        public void Sort(string property, bool ascending)
        {
            Files = (Files)this.Files.Sort(property, ascending);
        }
        

        public static readonly PropertyInfo<Folders> SubFoldersProperty = RegisterProperty<Folders>(p => p.SubFolders);
        public Folders SubFolders
        {
            get
            {
                return GetProperty(SubFoldersProperty);
            }
            set
            {
                LoadProperty(SubFoldersProperty, value);
            }
        }

        public bool IsOneDrive
        {
            get
            {
                return this.ParentWebUrl.ToLower().Contains("minsida");
            }
        }

        #region Child Factory Methods

        internal static Folder GetFolder(Sharepoint_Doc.DAL.ListDAL.SharePointFolder sharePointFolder, string listId, string parentWebUrl, string webApplication)
        {
            return DataPortal.FetchChild<Folder>(new FolderCriteria(sharePointFolder, listId, parentWebUrl, webApplication));
        }

        #endregion

        private void Child_Fetch(FolderCriteria criteria)
        {
            string[] nameparts = criteria.Folder.FileRef.Split('/');

            LoadProperty(NameProperty, nameparts[nameparts.Count() - 1]);
            LoadProperty(ListIdProperty, criteria.ListId);
            LoadProperty(ParentWebUrlProperty, criteria.ParentWebUrl);
            LoadProperty(ServerRelativeUrlProperty, criteria.Folder.FileRef);
            LoadProperty(WebApplicationProperty, criteria.WebApplication);
            LoadProperty(SubFoldersProperty, Folders.GetFolders(criteria.ListId, criteria.ParentWebUrl, criteria.WebApplication, criteria.Folder));


        }
    }

    public class FolderCriteria : CriteriaBase<FolderCriteria>
    {
        public Sharepoint_Doc.DAL.ListDAL.SharePointFolder Folder
        {
            set;
            get;
        }

        public string ListId
        {
            set;
            get;
        }

        public string ParentWebUrl
        {
            set;
            get;
        }

        public string ServerRelativeUrl
        {
            set;
            get;
        }

        public string WebApplication
        {
            set;
            get;
        }

        public FolderCriteria(Sharepoint_Doc.DAL.ListDAL.SharePointFolder folder, string listId, string parentWebUrl, string webApplication)
        {
            Folder = folder;
            ListId = listId;
            ParentWebUrl = parentWebUrl;
            WebApplication = webApplication;
        }

    }
}
