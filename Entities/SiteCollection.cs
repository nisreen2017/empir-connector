﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;

namespace Empir.OfficeConnector.Entities
{
    [Serializable]
    public class SiteCollection : ReadOnlyBase<SiteCollection>
    {
        public static readonly PropertyInfo<string> UrlProperty = RegisterProperty<string>(p => p.Url);
        public static readonly PropertyInfo<string> NameProperty = RegisterProperty<string>(p => p.Name);
        public static readonly PropertyInfo<string> TemplateProperty = RegisterProperty<string>(p => p.Template);
        public static readonly PropertyInfo<string> WebApplicationProperty = RegisterProperty<string>(p => p.WebApplication);

        public string Url
        {
            get { return GetProperty(UrlProperty); }
            private set { LoadProperty(UrlProperty, value); }
        }

        public string Name
        {
            get { return GetProperty(NameProperty); }
            private set { LoadProperty(NameProperty, value); }
        }

        public string Template
        {
            get { return GetProperty(TemplateProperty); }
            private set { LoadProperty(TemplateProperty, value); }
        }

        public string WebApplication
        {
            get { return GetProperty(WebApplicationProperty); }
            private set { LoadProperty(WebApplicationProperty, value); }
        }

        internal static SiteCollection GetSiteCollection(EmpirConnector.WebReference1.SiteCollection childData)
        {
            return DataPortal.FetchChild<SiteCollection>(childData);
        }

        private void Child_Fetch(EmpirConnector.WebReference1.SiteCollection childData)
        {
        
            LoadProperty(UrlProperty, childData.Url);
            Sharepoint_Doc.DAL.WebDAL webDAL = new Sharepoint_Doc.DAL.WebDAL();
            LoadProperty(TemplateProperty, webDAL.GetTemplate(childData.Url));
            int pos = childData.Url.ToLower().IndexOf(".se");
            string webApplication = childData.Url.Substring(0, pos + 3);
            LoadProperty(WebApplicationProperty, webApplication);
            LoadProperty(NameProperty, childData.Name);
        }
    }
}
