﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.DAL;
using System.Threading.Tasks;
using EmpirConnector.Entities;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class DocumentLibrary: ReadOnlyBase<DocumentLibrary>
    {
        public static readonly PropertyInfo<string> TitleProperty = RegisterProperty<string>(p => p.Title);
        public string Title
        {
            get { return GetProperty(TitleProperty); }
            set { LoadProperty(TitleProperty, value); }
        }

        public static readonly PropertyInfo<string> SubSiteProperty = RegisterProperty<string>(p => p.SubSite);
        public string SubSite
        {
            get { return GetProperty(SubSiteProperty); }
            set { LoadProperty(SubSiteProperty, value); }
        }

        public static readonly PropertyInfo<string> ListIdProperty = RegisterProperty<string>(p => p.ListId);
        public string ListId
        {
            get { return GetProperty(ListIdProperty); }
            set { LoadProperty(ListIdProperty, value); }
        }

        public static readonly PropertyInfo<string> UrlProperty = RegisterProperty<string>(p => p.Url);
        public string Url
        {
            get { return GetProperty(UrlProperty); }
            set { 
                LoadProperty(UrlProperty, value); 
            }
        }

        public static readonly PropertyInfo<string> DefaultEditFormUrlProperty = RegisterProperty<string>(p => p.DefaultEditFormUrl);
        public string DefaultEditFormUrl
        {
            get { return GetProperty(DefaultEditFormUrlProperty); }
            set { LoadProperty(DefaultEditFormUrlProperty, value); }
        }

        public static readonly PropertyInfo<bool> ReadPermissionProperty = RegisterProperty<bool>(p => p.ReadPermission);
        public bool ReadPermission
        {
            get { return GetProperty(ReadPermissionProperty); }
            set { LoadProperty(ReadPermissionProperty, value); }
        }


        public static readonly PropertyInfo<bool> WritePermissionProperty = RegisterProperty<bool>(p => p.WritePermission);
        public bool WritePermission
        {
            get { return GetProperty(WritePermissionProperty); }
            set { LoadProperty(WritePermissionProperty, value); }
        }

        public static readonly PropertyInfo<bool> EnableFolderCreationProperty = RegisterProperty<bool>(p => p.EnableFolderCreation);
        public bool EnableFolderCreation
        {
            get { return GetProperty(EnableFolderCreationProperty); }
            set { LoadProperty(EnableFolderCreationProperty, value); }
        }

        public static readonly PropertyInfo<string> WebApplicationProperty = RegisterProperty<string>(p => p.WebApplication);
        public string WebApplication
        {
            get { return GetProperty(WebApplicationProperty); }
            set { LoadProperty(WebApplicationProperty, value); }
        }



        public static readonly PropertyInfo<Folders> TemplateFoldersProperty = RegisterProperty<Folders>(p => p.TemplateFolders);
        public Folders TemplateFolders
        {
            get
            {
                if (!FieldManager.FieldExists(TemplateFoldersProperty))
                {
                    TemplateFolders = Folders.GetTemplateFolders(this.ListId, this.Url, this.WebApplication);
                }

                return GetProperty(TemplateFoldersProperty);
            }
            set
            {
                LoadProperty(TemplateFoldersProperty, value);
            }
        }

        public static readonly PropertyInfo<Folders> FoldersProperty = RegisterProperty<Folders>(p => p.Folders);
        public Folders Folders
        {
            get
            {
                if (!FieldManager.FieldExists(FoldersProperty))
                {
                    Folders = Folders.GetFolders(this.ListId, this.Url, this.WebApplication);
                }

                return GetProperty(FoldersProperty);
            }
            set
            {
                LoadProperty(FoldersProperty, value);
            }
        }


        public void ReloadFolders()
        {
         
            Folders = Folders.GetFolders(this.ListId, this.Url, this.WebApplication);

         

        }

        public static bool UserHasWritePermissions(string parentWebUrl, string listId)
        {
            ListDAL dalA = new ListDAL();

            return dalA.CheckPermissions(parentWebUrl, listId, true);
        }

        #region Child Factory Methods

        internal static DocumentLibrary GetTemplateLibrary()
        {
            return DataPortal.Fetch<DocumentLibrary>(new TemplateLibraryCriteria());
        }

        internal static DocumentLibrary GetLibrary(List childData, Grouping grouping)
        {
            return DataPortal.FetchChild<DocumentLibrary>(new DocumentLibraryCriteria(childData, grouping));
        }

        internal static DocumentLibrary GetLibrary(EmpirConnector.WebReference1.DocumentLibrary library)
        {
            return DataPortal.FetchChild<DocumentLibrary>(library);
        }

        internal static DocumentLibrary GetLibrary(GetListsAndSubWebsCommand.DocumentLibrary childData, Grouping grouping,  bool isOneDrive)
        {
            return DataPortal.FetchChild<DocumentLibrary>(new DocumentLibraryFromServiceCriteria(childData, grouping, isOneDrive));
        }

        #endregion

        private void DataPortal_Fetch(TemplateLibraryCriteria criteria)
        {
           DAL.ListDAL dal = new ListDAL();

           List list = dal.GetTemplateLibrary(AddInGlobals.ConnectorSettings.TemplatesListName, AddInGlobals.ConnectorSettings.TemplateListWebsUrl);

           Grouping grouping = new Grouping();
            grouping.WebApplikation = AddInGlobals.ConnectorSettings.RootUrl;
           LoadProperties(new DocumentLibraryCriteria(list, grouping));
            
        }

        #region Child Data Access
        
        private void LoadProperties(DocumentLibraryCriteria criteria)
        {

            Microsoft.SharePoint.Client.List data = criteria.ChildData;

            if (data == null)
                return;

            LoadProperty(TitleProperty, data.Title);
            //LoadProperty(SubSiteProperty, data.ParentWeb.ServerRelativeUrl);
            LoadProperty(ListIdProperty, data.Id.ToString());

            if (data.ParentWebUrl == "/")
                LoadProperty(UrlProperty, criteria.Grouping.WebApplikation);
            else
                LoadProperty(UrlProperty, criteria.Grouping.WebApplikation + data.ParentWebUrl);

            LoadProperty(WebApplicationProperty, criteria.Grouping.WebApplikation);
 
         //   LoadProperty(DefaultEditFormUrlProperty, criteria.Grouping.WebApplikation + data.DefaultEditFormUrl);
            LoadProperty(DefaultEditFormUrlProperty, AddInGlobals.ConnectorSettings.RootUrl + data.DefaultEditFormUrl);

            LoadProperty(WritePermissionProperty, data.EffectiveBasePermissions.Has(PermissionKind.AddListItems));
            LoadProperty(ReadPermissionProperty, data.EffectiveBasePermissions.Has(PermissionKind.ViewListItems));
            LoadProperty(EnableFolderCreationProperty, data.EnableFolderCreation); 

        }

        private void LoadProperties(DocumentLibraryFromServiceCriteria criteria)
        {


            LoadProperty(TitleProperty, criteria.ChildData.Title);
            //LoadProperty(SubSiteProperty, data.ParentWeb.ServerRelativeUrl);
            LoadProperty(ListIdProperty, criteria.ChildData.Id);

            if (criteria.ChildData.ParentWebUrl == "/")
                LoadProperty(UrlProperty, AddInGlobals.ConnectorSettings.RootUrl);
            else
            {
                if(criteria.IsOneDrive)
                    LoadProperty(UrlProperty, AddInGlobals.ConnectorSettings.OneDriveWebUrl);

                else
                    LoadProperty(UrlProperty, AddInGlobals.ConnectorSettings.RootUrl + criteria.ChildData.ParentWebUrl);

            }

            LoadProperty(WebApplicationProperty, AddInGlobals.ConnectorSettings.RootUrl);

            LoadProperty(DefaultEditFormUrlProperty, AddInGlobals.ConnectorSettings.RootUrl + criteria.ChildData.DefaultEditFormUrl);

            LoadProperty(WritePermissionProperty, criteria.ChildData.WritePermission);
            LoadProperty(ReadPermissionProperty, criteria.ChildData.ReadPermission);
            LoadProperty(EnableFolderCreationProperty, criteria.ChildData.EnableFolderCreation);

        }

        private void Child_Fetch(DocumentLibraryCriteria criteria)
        {

            LoadProperties(criteria);
        }


        private void Child_Fetch(DocumentLibraryFromServiceCriteria criteria)
        {

            LoadProperties(criteria);
        }


        #endregion

        private class DocumentLibraryCriteria : CriteriaBase<DocumentLibraryCriteria>
        {
            public List ChildData
            {
                set;
                get;
            }

            public Grouping Grouping
            {
                set;
                get;
            }

            public DocumentLibraryCriteria(List childData, Grouping grouping)
            {
                ChildData = childData;
                Grouping = grouping;
            }

        }

        private class DocumentLibraryFromServiceCriteria : CriteriaBase<DocumentLibraryFromServiceCriteria>
        {
            public GetListsAndSubWebsCommand.DocumentLibrary ChildData
            {
                set;
                get;
            }

            public Grouping Grouping
            {
                set;
                get;
            }

            public bool IsOneDrive
            {
                set;
                get;
            }

            public DocumentLibraryFromServiceCriteria(GetListsAndSubWebsCommand.DocumentLibrary childData, Grouping grouping, bool isOneDrive)
            {
                ChildData = childData;
                Grouping = grouping;
                IsOneDrive = isOneDrive;
            }

        }
        //

        private class TemplateLibraryCriteria : CriteriaBase<TemplateLibraryCriteria>
        {
        }

    }
}

