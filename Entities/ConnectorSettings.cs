﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Csla;
using Microsoft.Win32;
using EmpirConnector.Forms;
using System.Windows.Forms;
using EmpirConnector.Entities;

namespace Sharepoint_Doc.Entities
{
    [Serializable]
    public class ConnectorSettings : ReadOnlyBase<ConnectorSettings>
    {
        public static readonly PropertyInfo<string> RootUrlProperty = RegisterProperty<string>(p => p.RootUrl);
        public string RootUrl
        {
            get { return GetProperty(RootUrlProperty); }
            private set { LoadProperty(RootUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> OneDriveUrlProperty = RegisterProperty<string>(p => p.OneDriveUrl);
        public string OneDriveUrl
        {
            get { return GetProperty(OneDriveUrlProperty); }
            private set { LoadProperty(OneDriveUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> OneDriveWebUrlProperty = RegisterProperty<string>(p => p.OneDriveWebUrl);
        public string OneDriveWebUrl
        {
            get { return GetProperty(OneDriveWebUrlProperty); }
            private set { LoadProperty(OneDriveWebUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> GroupingListWebProperty = RegisterProperty<string>(p => p.GroupingListWebUrl);
        public string GroupingListWebUrl
        {
            get { return GetProperty(GroupingListWebProperty); }
            private set { LoadProperty(GroupingListWebProperty, value); }
        }

        public static readonly PropertyInfo<string> GroupingListNameProperty = RegisterProperty<string>(p => p.GroupingListName);
        public string GroupingListName
        {
            get { return GetProperty(GroupingListNameProperty); }
            private set { LoadProperty(GroupingListNameProperty, value); }
        }

        public static readonly PropertyInfo<string> ExcludeListWebUrlProperty = RegisterProperty<string>(p => p.ExcludeListWebUrl);
        public string ExcludeListWebUrl
        {
            get { return GetProperty(ExcludeListWebUrlProperty); }
            private set { LoadProperty(ExcludeListWebUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> ExcludeListNameProperty = RegisterProperty<string>(p => p.ExcludeListName);
        public string ExcludeListName
        {
            get { return GetProperty(ExcludeListNameProperty); }
            private set { LoadProperty(ExcludeListNameProperty, value); }
        }

        public static readonly PropertyInfo<string> ExcludeSiteListWebUrlProperty = RegisterProperty<string>(p => p.ExcludeSiteListWebUrl);
        public string ExcludeSiteListWebUrl
        {
            get { return GetProperty(ExcludeSiteListWebUrlProperty); }
            private set { LoadProperty(ExcludeSiteListWebUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> ExcludeSiteListNameProperty = RegisterProperty<string>(p => p.ExcludeSiteListName);
        public string ExcludeSiteListName
        {
            get { return GetProperty(ExcludeSiteListNameProperty); }
            private set { LoadProperty(ExcludeSiteListNameProperty, value); }
        }

        public static readonly PropertyInfo<string> TemplatesListWebUrlProperty = RegisterProperty<string>(p => p.TemplateListWebsUrl);
        public string TemplateListWebsUrl
        {
            get { return GetProperty(TemplatesListWebUrlProperty); }
            private set { LoadProperty(TemplatesListWebUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> TemplatesListNameProperty = RegisterProperty<string>(p => p.TemplatesListName);
        public string TemplatesListName
        {
            get { return GetProperty(TemplatesListNameProperty); }
            private set { LoadProperty(TemplatesListNameProperty, value); }
        }


        public static readonly PropertyInfo<string> IntelligenceListWebUrlProperty = RegisterProperty<string>(p => p.IntelligenceListWebUrl);
        public string IntelligenceListWebUrl
        {
            get { return GetProperty(IntelligenceListWebUrlProperty); }
            private set { LoadProperty(IntelligenceListWebUrlProperty, value); }
        }

        public static readonly PropertyInfo<string> IntelligenceListNameProperty = RegisterProperty<string>(p => p.IntelligenceListName);
        public string IntelligenceListName
        {
            get { return GetProperty(IntelligenceListNameProperty); }
            private set { LoadProperty(IntelligenceListNameProperty, value); }
        }

        public static readonly PropertyInfo<string> ServiceUrlProperty = RegisterProperty<string>(p => p.ServiceUrl);
        public string ServiceUrl
        {
            get { return GetProperty(ServiceUrlProperty); }
            private set { LoadProperty(ServiceUrlProperty, value); }
        }



        public static readonly PropertyInfo<int> PageCountProperty = RegisterProperty<int>(p => p.PageCount);
        public int PageCount
        {
            get { return GetProperty(PageCountProperty); }
            private set { LoadProperty(PageCountProperty, value); }
        }


        public static readonly PropertyInfo<int> UpdateRateProperty = RegisterProperty<int>(p => p.UpdateRate);
        public int UpdateRate
        {
            get { return GetProperty(UpdateRateProperty); }
            private set { LoadProperty(UpdateRateProperty, value); }
        }

        public static readonly PropertyInfo<bool> PreloadWebsProperty = RegisterProperty<bool>(p => p.PreloadWebs);
        public bool PreloadWebs
        {
            get { return GetProperty(PreloadWebsProperty); }
            private set { LoadProperty(PreloadWebsProperty, value); }
        }

        public static ConnectorSettings GetSettings()
        {
            return DataPortal.Fetch<ConnectorSettings>();
        }

        private ConnectorSettings()
        { /* require use of factory methods */ }





        private void DataPortal_Fetch()
        {
            string keyPath;

            object connectionString;
            string path = "";


            if (Environment.Is64BitOperatingSystem)
            {


                try
                {
                    keyPath = @"SOFTWARE\WOW6432Node\OfficeConnector";

                    connectionString = RegistryHelpers.GetRegistryValue(keyPath, "TargetPath");

                    if (connectionString == null)
                    {

                        connectionString = @"C:\Program Files (x86)\Empir AB\EmpirConnector\";
                    }
                }

                catch
                {
                    try
                    {

                        keyPath = @"SOFTWARE\OfficeConnector";

                        connectionString = RegistryHelpers.GetRegistryValue(keyPath, "TargetPath");
                    }
                    catch
                    {
                        connectionString = @"C:\Program Files (x86)\Empir AB\EmpirConnector\";


                    }

                }


            }

            else
            {

                connectionString = @"C:\Program Files\Empir AB\EmpirConnector\";

            }




            if (connectionString == null)
            {

                MessageBox.Show("Registernyckel saknas.", Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            else
            {
                path = connectionString + @"\ConnectorSettings.xml";

            }

            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            if (!System.IO.File.Exists(path))
            {
                MessageBox.Show("Kunde inte hitta ConnectorSettings.xml på angiven plats.", Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            doc.Load(path);

            LoadProperty(RootUrlProperty, doc.SelectSingleNode("//RootUrl").InnerText);
            string userName = ConnectorUser.Username;

            if (userName.StartsWith("_"))
            {
                userName = "thn_" + userName;

            }

            LoadProperty(OneDriveUrlProperty, doc.SelectSingleNode("//OneDriveUrl").InnerText + "/" + userName + "/_layouts/15");
            LoadProperty(OneDriveWebUrlProperty, doc.SelectSingleNode("//OneDriveUrl").InnerText + "/" + userName);
            LoadProperty(TemplatesListWebUrlProperty, doc.SelectSingleNode("//TemplatesListWebUrl").InnerText);
            LoadProperty(TemplatesListNameProperty, doc.SelectSingleNode("//TemplatesListName").InnerText);
            LoadProperty(GroupingListWebProperty, doc.SelectSingleNode("//GroupingListWebUrl").InnerText);
            LoadProperty(GroupingListNameProperty, doc.SelectSingleNode("//GroupingListName").InnerText);
            LoadProperty(ExcludeListWebUrlProperty, doc.SelectSingleNode("//ExcludeListWebUrl").InnerText);
            LoadProperty(ExcludeListNameProperty, doc.SelectSingleNode("//ExcludeListName").InnerText);
            LoadProperty(ExcludeSiteListWebUrlProperty, doc.SelectSingleNode("//ExcludeSiteListWebUrl").InnerText);
            LoadProperty(ExcludeSiteListNameProperty, doc.SelectSingleNode("//ExcludeSiteListName").InnerText);
            LoadProperty(IntelligenceListWebUrlProperty, doc.SelectSingleNode("//IntelligenceListWebUrl").InnerText);
            LoadProperty(IntelligenceListNameProperty, doc.SelectSingleNode("//IntelligenceListName").InnerText);
            LoadProperty(PageCountProperty, int.Parse(doc.SelectSingleNode("//PageCount").InnerText));
            LoadProperty(UpdateRateProperty, int.Parse(doc.SelectSingleNode("//UpdateRate").InnerText));
            LoadProperty(ServiceUrlProperty, doc.SelectSingleNode("//ServiceUrl").InnerText);
            LoadProperty(PreloadWebsProperty, bool.Parse(doc.SelectSingleNode("//PreloadWebs").InnerText));
        }

    }
}
