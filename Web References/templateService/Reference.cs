﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.296
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by Microsoft.VSDesigner, Version 4.0.30319.296.
// 
#pragma warning disable 1591

namespace EmpirConnector.templateService {
    using System;
    using System.Web.Services;
    using System.Diagnostics;
    using System.Web.Services.Protocols;
    using System.ComponentModel;
    using System.Xml.Serialization;
    
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Web.Services.WebServiceBindingAttribute(Name="EmpirTemplateServiceWSSoap", Namespace="http://tempuri.org/")]
    public partial class EmpirTemplateServiceWS : System.Web.Services.Protocols.SoapHttpClientProtocol {
        
        private System.Threading.SendOrPostCallback GetUniqueFileIDsOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetMenuXMLOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetTemplateInstanceOperationCompleted;
        
        private System.Threading.SendOrPostCallback GetGovernancesOperationCompleted;
        
        private bool useDefaultCredentialsSetExplicitly;
        
        /// <remarks/>
        public EmpirTemplateServiceWS() {
            this.Url = global::EmpirConnector.Properties.Settings.Default.Empir_Connector_templateService_EmpirTemplateServiceWS;
            if ((this.IsLocalFileSystemWebService(this.Url) == true)) {
                this.UseDefaultCredentials = true;
                this.useDefaultCredentialsSetExplicitly = false;
            }
            else {
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        public new string Url {
            get {
                return base.Url;
            }
            set {
                if ((((this.IsLocalFileSystemWebService(base.Url) == true) 
                            && (this.useDefaultCredentialsSetExplicitly == false)) 
                            && (this.IsLocalFileSystemWebService(value) == false))) {
                    base.UseDefaultCredentials = false;
                }
                base.Url = value;
            }
        }
        
        public new bool UseDefaultCredentials {
            get {
                return base.UseDefaultCredentials;
            }
            set {
                base.UseDefaultCredentials = value;
                this.useDefaultCredentialsSetExplicitly = true;
            }
        }
        
        /// <remarks/>
        public event GetUniqueFileIDsCompletedEventHandler GetUniqueFileIDsCompleted;
        
        /// <remarks/>
        public event GetMenuXMLCompletedEventHandler GetMenuXMLCompleted;
        
        /// <remarks/>
        public event GetTemplateInstanceCompletedEventHandler GetTemplateInstanceCompleted;
        
        /// <remarks/>
        public event GetGovernancesCompletedEventHandler GetGovernancesCompleted;
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetUniqueFileIDs", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public TemplateInfo[] GetUniqueFileIDs() {
            object[] results = this.Invoke("GetUniqueFileIDs", new object[0]);
            return ((TemplateInfo[])(results[0]));
        }
        
        /// <remarks/>
        public void GetUniqueFileIDsAsync() {
            this.GetUniqueFileIDsAsync(null);
        }
        
        /// <remarks/>
        public void GetUniqueFileIDsAsync(object userState) {
            if ((this.GetUniqueFileIDsOperationCompleted == null)) {
                this.GetUniqueFileIDsOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetUniqueFileIDsOperationCompleted);
            }
            this.InvokeAsync("GetUniqueFileIDs", new object[0], this.GetUniqueFileIDsOperationCompleted, userState);
        }
        
        private void OnGetUniqueFileIDsOperationCompleted(object arg) {
            if ((this.GetUniqueFileIDsCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetUniqueFileIDsCompleted(this, new GetUniqueFileIDsCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetMenuXML", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string GetMenuXML() {
            object[] results = this.Invoke("GetMenuXML", new object[0]);
            return ((string)(results[0]));
        }
        
        /// <remarks/>
        public void GetMenuXMLAsync() {
            this.GetMenuXMLAsync(null);
        }
        
        /// <remarks/>
        public void GetMenuXMLAsync(object userState) {
            if ((this.GetMenuXMLOperationCompleted == null)) {
                this.GetMenuXMLOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetMenuXMLOperationCompleted);
            }
            this.InvokeAsync("GetMenuXML", new object[0], this.GetMenuXMLOperationCompleted, userState);
        }
        
        private void OnGetMenuXMLOperationCompleted(object arg) {
            if ((this.GetMenuXMLCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetMenuXMLCompleted(this, new GetMenuXMLCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetTemplateInstance", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        [return: System.Xml.Serialization.XmlElementAttribute(DataType="base64Binary")]
        public byte[] GetTemplateInstance(string governance, string id, out string filename) {
            object[] results = this.Invoke("GetTemplateInstance", new object[] {
                        governance,
                        id});
            filename = ((string)(results[1]));
            return ((byte[])(results[0]));
        }
        
        /// <remarks/>
        public void GetTemplateInstanceAsync(string governance, string id) {
            this.GetTemplateInstanceAsync(governance, id, null);
        }
        
        /// <remarks/>
        public void GetTemplateInstanceAsync(string governance, string id, object userState) {
            if ((this.GetTemplateInstanceOperationCompleted == null)) {
                this.GetTemplateInstanceOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetTemplateInstanceOperationCompleted);
            }
            this.InvokeAsync("GetTemplateInstance", new object[] {
                        governance,
                        id}, this.GetTemplateInstanceOperationCompleted, userState);
        }
        
        private void OnGetTemplateInstanceOperationCompleted(object arg) {
            if ((this.GetTemplateInstanceCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetTemplateInstanceCompleted(this, new GetTemplateInstanceCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        [System.Web.Services.Protocols.SoapDocumentMethodAttribute("http://tempuri.org/GetGovernances", RequestNamespace="http://tempuri.org/", ResponseNamespace="http://tempuri.org/", Use=System.Web.Services.Description.SoapBindingUse.Literal, ParameterStyle=System.Web.Services.Protocols.SoapParameterStyle.Wrapped)]
        public string[] GetGovernances() {
            object[] results = this.Invoke("GetGovernances", new object[0]);
            return ((string[])(results[0]));
        }
        
        /// <remarks/>
        public void GetGovernancesAsync() {
            this.GetGovernancesAsync(null);
        }
        
        /// <remarks/>
        public void GetGovernancesAsync(object userState) {
            if ((this.GetGovernancesOperationCompleted == null)) {
                this.GetGovernancesOperationCompleted = new System.Threading.SendOrPostCallback(this.OnGetGovernancesOperationCompleted);
            }
            this.InvokeAsync("GetGovernances", new object[0], this.GetGovernancesOperationCompleted, userState);
        }
        
        private void OnGetGovernancesOperationCompleted(object arg) {
            if ((this.GetGovernancesCompleted != null)) {
                System.Web.Services.Protocols.InvokeCompletedEventArgs invokeArgs = ((System.Web.Services.Protocols.InvokeCompletedEventArgs)(arg));
                this.GetGovernancesCompleted(this, new GetGovernancesCompletedEventArgs(invokeArgs.Results, invokeArgs.Error, invokeArgs.Cancelled, invokeArgs.UserState));
            }
        }
        
        /// <remarks/>
        public new void CancelAsync(object userState) {
            base.CancelAsync(userState);
        }
        
        private bool IsLocalFileSystemWebService(string url) {
            if (((url == null) 
                        || (url == string.Empty))) {
                return false;
            }
            System.Uri wsUri = new System.Uri(url);
            if (((wsUri.Port >= 1024) 
                        && (string.Compare(wsUri.Host, "localHost", System.StringComparison.OrdinalIgnoreCase) == 0))) {
                return true;
            }
            return false;
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.233")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="http://tempuri.org/")]
    public partial class TemplateInfo {
        
        private string uniqueIDField;
        
        private bool intelligentField;
        
        /// <remarks/>
        public string UniqueID {
            get {
                return this.uniqueIDField;
            }
            set {
                this.uniqueIDField = value;
            }
        }
        
        /// <remarks/>
        public bool Intelligent {
            get {
                return this.intelligentField;
            }
            set {
                this.intelligentField = value;
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void GetUniqueFileIDsCompletedEventHandler(object sender, GetUniqueFileIDsCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetUniqueFileIDsCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetUniqueFileIDsCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public TemplateInfo[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((TemplateInfo[])(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void GetMenuXMLCompletedEventHandler(object sender, GetMenuXMLCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetMenuXMLCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetMenuXMLCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[0]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void GetTemplateInstanceCompletedEventHandler(object sender, GetTemplateInstanceCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetTemplateInstanceCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetTemplateInstanceCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public byte[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((byte[])(this.results[0]));
            }
        }
        
        /// <remarks/>
        public string filename {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string)(this.results[1]));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    public delegate void GetGovernancesCompletedEventHandler(object sender, GetGovernancesCompletedEventArgs e);
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Web.Services", "4.0.30319.1")]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    public partial class GetGovernancesCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        internal GetGovernancesCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        /// <remarks/>
        public string[] Result {
            get {
                this.RaiseExceptionIfNecessary();
                return ((string[])(this.results[0]));
            }
        }
    }
}

#pragma warning restore 1591