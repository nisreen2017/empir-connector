﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace EmpirConnector
{
    public class ExcelHWND: IWin32Window
    {

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        string m_sWindowName;
        public ExcelHWND(string sWindowName)
        {
            m_sWindowName = sWindowName;
        }


        [DllImport("user32.Dll")]
        static extern IntPtr FindWindow(string className, string windowName);

     
        public IntPtr Handle
        {
            // Note: "OpusApp" is the class name for Word 
            get { return FindWindow("XLMAIN", m_sWindowName); }
        }

    }
}