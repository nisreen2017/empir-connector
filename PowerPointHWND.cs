﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace EmpirConnector
{
    public class PowerPointHWND : IWin32Window
    {
        
        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowThreadProcessId(IntPtr hWnd, out uint ProcessId);

        [DllImport("user32.dll")]
        private static extern IntPtr GetForegroundWindow();

        string m_sWindowName;
        public PowerPointHWND(string sWindowName)
        {
            m_sWindowName = sWindowName + " - Microsoft PowerPoint";
        }


        //[DllImport("user32", EntryPoint = "FindWindowA")]
        //private static extern long FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", CharSet=CharSet.Auto)]
        public static extern IntPtr FindWindow(string strClassName, int nptWindowName);

     
        public IntPtr Handle
        {
            // Note: "OpusApp" is the class name for Word 
            get
            {

                return FindWindow("PP12FrameClass", 0);
            }

        }
    }
}
