﻿using System;
using System.Runtime.InteropServices;
using System.ComponentModel;
using System.Windows.Forms;
using AddinExpress.MSO;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;
using Outlook = Microsoft.Office.Interop.Outlook;
using PowerPoint = Microsoft.Office.Interop.PowerPoint;
using Sharepoint_Doc;
using Sharepoint_Doc.Entities;
using EmpirConnector.Entities;
using Sharepoint_Doc.Forms;
using EmpirConnector.Entities.Office;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using Sharepoint_Doc.Controls.Templates;
using System.IO;
using EmpirConnector.Controls.Templates;
using System.Linq;
using EmpirConnector.Forms;
using System.Drawing;
using Sharepoint_Doc.DAL;


namespace EmpirConnector
{
    /// <summary>
    ///   Add-in Express Add-in Module
    /// </summary>
    [GuidAttribute("EC63AE2C-3F2E-4FE5-8DDF-D1014326DF62"), ProgId("EmpirConnector.AddinModule")]
    public class AddinModule : AddinExpress.MSO.ADXAddinModule
    {
        public enum CurrentFunctionEnum
        {
            Loading,
            Open,
            SaveAs,
            Templates,
        }

        public static CurrentFunctionEnum CurrentFunction
        {
            set;
            get;
        }

        public static PreloadCommand PreloadOpenCommand;
        public static PreloadCommand PreloadSaveCommand;
        System.Windows.Forms.Timer _documentInformationPanelTimer;
        System.Windows.Forms.Timer _updateCacheTimer;
        private ImageList imageList1;
        private bool _updateCacheTimerNotRun = true;
        Forms.HidesInfoPanelForm _frmHidesInfoPanel;
        private ImageList imageList2;
        private ADXRibbonButton btnSettings;

        private System.Windows.Forms.Timer _startupTimer;

    
        public AddinModule()
        {
            Application.EnableVisualStyles();
            InitializeComponent();



            _startupTimer = new System.Windows.Forms.Timer();
            _startupTimer.Interval = 200;
            _startupTimer.Tick += new EventHandler(_startupTimer_Tick);
            _startupTimer.Start();
  

            // Please add any initialization code to the AddinInitialize event handler
            this.AddinInitialize += new ADXEvents_EventHandler(AddinModule_AddinInitialize);
            this.AddinStartupComplete += new ADXEvents_EventHandler(AddinModule_AddinStartupComplete);
        }

        void _startupTimer_Tick(object sender, EventArgs e)
        {
            

            _startupTimer.Stop();

            _documentInformationPanelTimer = new System.Windows.Forms.Timer();
            _documentInformationPanelTimer.Interval = 1000;
            _documentInformationPanelTimer.Tick += new EventHandler(_documentInformationPanelTimer_Tick);
            _documentInformationPanelTimer.Start();

            ConnectorSettings settings = ConnectorSettings.GetSettings();
            _updateCacheTimer = new System.Windows.Forms.Timer();
            _updateCacheTimer.Interval =  60000 * settings.UpdateRate;
            _updateCacheTimer.Tick += new EventHandler(_updateCacheTimer_Tick);
            _updateCacheTimer.Start();


            btnNewFromSharePoint.OnClick += new ADXRibbonOnAction_EventHandler(btnNewFromSharePoint_OnClick);
            btnOpenFromSharePoint.OnClick += new ADXRibbonOnAction_EventHandler(btnOpenFromSharePoint_OnClick);
            btnSaveInSharePoint.OnClick += new ADXRibbonOnAction_EventHandler(btnSaveInSharePoint_OnClick);

            

            Task.Factory.StartNew(() => GetSettings());

        }

        void AddinModule_AddinStartupComplete(object sender, EventArgs e)
        {
   
        }

        public void UpdateCache()
        {
            PreloadCommand preloadOpenCommand = new PreloadCommand();
            preloadOpenCommand.WritePermission = false;
            preloadOpenCommand.BakgroundJob = true;

            var t = new Thread(() => preloadOpenCommand.Execute());
            t.Start();


            PreloadCommand preloadSaveCommand = new PreloadCommand();
            preloadSaveCommand.WritePermission = true;
            preloadSaveCommand.BakgroundJob = true;
            var t2 = new Thread(() => preloadSaveCommand.Execute());
            t2.Start();
        }

        private void GetSettings()
        {
            try
            {
                AddInGlobals.ConnectorSettings = ConnectorSettings.GetSettings();

                AddInGlobals.ExcludeSiteListNameList = ExcludeSiteListNameList.GetList();
                AddInGlobals.ExcludeListNameList = ExcludeListNameList.GetList();


                PreloadOpenCommand = new PreloadCommand();
                PreloadOpenCommand.WritePermission = false;
                var t = new Thread(() => PreloadOpenCommand.Execute());
                t.IsBackground = true;
                t.Start();



                PreloadSaveCommand = new PreloadCommand();
                PreloadSaveCommand.WritePermission = true;
                var t2 = new Thread(() => PreloadSaveCommand.Execute());
                t2.IsBackground = true;
                t2.Start();
            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(@"c:\temp\ConnectorLog.txt", ex.Message + Environment.NewLine + ex.StackTrace);
                DisableRibbon();
            }
        }

        public void DisableRibbon()
        {
                     
           
            foreach (var control in this.adxRibbonTab1.Controls)
            {
                if (control is ADXRibbonGroup)
                {
                    ADXRibbonGroup group = (ADXRibbonGroup)control;
                    foreach(var button in group.Controls)
                        ((ADXRibbonButton)button).Enabled = false;
                }

            }
            //});
        }

        private delegate void SetFormOpen(SharepointDocuments form);

        void AddinModule_AddinInitialize(object sender, EventArgs e)
        {

            

        }

        /// <summary>
        /// Triggered first time from _documentInformationPanelTimer_Tick
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void _updateCacheTimer_Tick(object sender, EventArgs e)
        {
            if (PreloadOpenCommand == null || PreloadSaveCommand == null)
                return;

            if (PreloadOpenCommand.ByteSize > 0 &&
                PreloadSaveCommand.ByteSize > 0)
            {
                // = false;
                _updateCacheTimer.Enabled = false;
                return;
            }

            _updateCacheTimerNotRun = false;

            UpdateCache();

        }

        void _documentInformationPanelTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                if (PreloadOpenCommand != null && PreloadOpenCommand.Loaded && PreloadOpenCommand.FromCache &&
                    PreloadSaveCommand.Loaded && PreloadSaveCommand.FromCache && _updateCacheTimerNotRun)
                {
                    _updateCacheTimer_Tick(this, EventArgs.Empty);
                }

                if (MySettings.Instance.ShowDIP)
                    return;

                if (ExcelApp != null)
                {
                    ExcelApp.DisplayDocumentInformationPanel = false;
                }

                else if (WordApp != null)
                {
                    WordApp.DisplayDocumentInformationPanel = false;
   
                }
                else if (PowerPointApp != null)
                {
                    PowerPointApp.DisplayDocumentInformationPanel =false;
                }

            }
            catch { }
        }

        void btnSaveInSharePoint_OnClick(object sender, IRibbonControl control, bool pressed)
        {
            try
            {


                if (OutlookApp != null)
                {
                    AddInGlobals.MailInspector = control.Context as Microsoft.Office.Interop.Outlook.Inspector;
                    SaveAttachmentForm frm = new SaveAttachmentForm();
                    frm.ShowDialog();
                }
                else
                {
                    if (NoActiveDocumentCheckerCommand.Execute())
                    {
                        MessageBox.Show("Det finns inget dokument att spara.", Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    if (PreloadSaveCommand == null)
                        Waiting.Instance.Show();

                    while (PreloadSaveCommand == null)
                        Thread.Sleep(1000);

                    if (!PreloadSaveCommand.Loaded)
                        Waiting.Instance.Show();
                    SaveAsDocumentForm frm = new SaveAsDocumentForm();

                    if (AddinModule.CurrentInstance.WordApp != null)
                        frm.ShowDialog(new WordHWND(WordApp.ActiveDocument.Name));
                    else
                        frm.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void btnOpenFromSharePoint_OnClick(object sender, IRibbonControl control, bool pressed)
        {

            try
            {
                SharepointDocuments frmOpen;
                frmOpen = new SharepointDocuments(SharepointDocuments.eActionType.Open);

                if(PreloadOpenCommand == null)
                    Waiting.Instance.Show();

                while (PreloadOpenCommand == null)
                    Thread.Sleep(1000);

                if (AddinModule.CurrentInstance.WordApp != null)
                {
                    if (WordApp.Documents.Count == 0)
                        frmOpen.ShowDialog();
                    else
                    {
                        WordHWND hwnd = new WordHWND(WordApp.ActiveDocument.Name);
                        frmOpen.ShowDialog(hwnd);
                    }
                }
                else if (AddinModule.CurrentInstance.ExcelApp != null)
                {
                    if (ExcelApp.Workbooks.Count == 0)
                        frmOpen.ShowDialog();
                    else
                    {
                            ExcelHWND hwnd = new ExcelHWND(ExcelApp.Application.Caption);
                            frmOpen.ShowDialog(hwnd);
                    }
                }
                else if (AddinModule.CurrentInstance.PowerPointApp != null)
                {
                    if (PowerPointApp.Presentations.Count == 0)
                        frmOpen.ShowDialog();
                    else
                    {
                        PowerPointHWND hwnd = new PowerPointHWND(PowerPointApp.ActivePresentation.Name);
                        frmOpen.ShowDialog(hwnd);
                    }
                }

                EmpirConnector.Entities.Office.OfficeDocument.OpenModeEnum openModeEnum = OfficeDocument.OpenModeEnum.None;
 
                if (frmOpen.DialogResult == DialogResult.OK)
                {
                    OfficeDocument.GetDocument().Open(frmOpen.SelectedFileUrl, OfficeDocument.OpenModeEnum.None);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
          
        }

        void btnNewFromSharePoint_OnClick(object sender, IRibbonControl control, bool pressed)
        {
            int xx = 0;

            try
            {
                TemplatesForm frm = new TemplatesForm();

                if (AddinModule.CurrentInstance.WordApp != null && WordApp.Documents.Count > 0)
                    frm.ShowDialog(new WordHWND(WordApp.ActiveDocument.Name));
                else
                    frm.ShowDialog();

                if (frm.DialogResult != DialogResult.OK)
                    return;

           
                string template = Sharepoint_Doc.Entities.File.GetTemplateFilenameInEmpirDir(frm.Filename); 

                FileDAL dal = new FileDAL();
                WebClient webClient = new WebClient();
                webClient.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                webClient.DownloadFile(frm.SelectedTemplateUrl, template);
                MemoryStream templateStream = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(template))
                {
                    fs.CopyTo(templateStream);
                }
                   
      
                if (frm.Intelligent && OfficeDocument.IsWordApp())
                {
                    if (frm.OpenAsTemplate)
                        OfficeDocument.GetDocument().Open(frm.SelectedTemplateUrl, OfficeDocument.OpenModeEnum.Edit);
                    else
                    {
                        TemplateIntelligenceList list = TemplateIntelligenceList.GetList();
                        TemplateIntelligence templateIntelligence = list.Where(t => t.Title == frm.Governance).FirstOrDefault();

                        if (templateIntelligence == null)
                        {
                            MessageBox.Show(Constants.NO_INTELLIGENCE_FOR_TEMPLATE, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }


      
                        byte[] logo = templateIntelligence.Logo;
                        byte[] footer = templateIntelligence.Footer;

                        if (logo == null || footer == null)
                        {
                            MessageBox.Show(Constants.PAGEFOOTER_OR_LOGO_MISSING, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }

                        AddInGlobals.OpenAsTemplate = frm.OpenAsTemplate;


                        if (frm.Intelligent && templateIntelligence != null)
                        {
                            WordprocessingWorker worker = new WordprocessingWorker();

                            MemoryStream logoStream = null;
                            MemoryStream footerStream = null;
                            if (logo != null)
                                logoStream = new MemoryStream(logo);
                            if (footer != null)
                                footerStream = new MemoryStream(footer);

                            worker._logoStream = logoStream;
                            worker._footerStream = footerStream;
                            worker.Open(templateStream);
                            worker.ApplyHeaderAndFooter();

                            if (logoStream != null)
                                logoStream.Close();
                            if (footerStream != null)
                                footerStream.Close();

                            template = GetTemplateFilename(Path.GetFileNameWithoutExtension(frm.Filename));
                            worker.SaveAs(template);

                            templateStream.Close();

                            worker.ReplaceLogoInHeader(template);
                        }

                        OfficeDocument.GetDocument().Add(template);


                    }
                }
                else
                {
                    if (frm.OpenAsTemplate)
                        OfficeDocument.GetDocument().Open(frm.SelectedTemplateUrl, OfficeDocument.OpenModeEnum.None);
                    else
                        OfficeDocument.GetDocument().Add(template);
                }


            }
            catch (Exception ex)
            {
                System.IO.File.AppendAllText(Path.GetTempPath() + "ConnectorLog.txt", ex.Message + System.Environment.NewLine + ex.StackTrace);
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.InnerException + xx.ToString(), Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

         
            Application.DoEvents();
        }

        private void btnSettings_OnClick(object sender, IRibbonControl control, bool pressed)
        {
            SettingsForm frm = new SettingsForm();
            frm.ShowDialog();
        }

        private string GetTemplateFilename(string fileNameWithoutExtension)
        {
            string tryFilename = "";
            for(int i = 1; i<1000; i++)
            {
                bool ok = true;
                 tryFilename =System.IO.Path.GetTempPath() + fileNameWithoutExtension + i.ToString() + ".docx";
                if(System.IO.File.Exists(tryFilename))
                {
                    try
                    {
                        System.IO.File.Delete(tryFilename);
                    }
                    catch
                    {
                        ok=false;
                    }

                }
                
                if(ok)
                    break;
            }

            return tryFilename;
        }


        private ADXRibbonTab adxRibbonTab1;
        private ADXRibbonGroup adxRibbonGroup1;
        private ADXRibbonButton btnNewFromSharePoint;
        private ADXRibbonButton btnOpenFromSharePoint;
        private ADXRibbonButton btnSaveInSharePoint;
 
        #region Component Designer generated code
        /// <summary>
        /// Required by designer
        /// </summary>
        private System.ComponentModel.IContainer components;
 
        /// <summary>
        /// Required by designer support - do not modify
        /// the following method
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AddinModule));
            this.adxRibbonTab1 = new AddinExpress.MSO.ADXRibbonTab(this.components);
            this.adxRibbonGroup1 = new AddinExpress.MSO.ADXRibbonGroup(this.components);
            this.btnNewFromSharePoint = new AddinExpress.MSO.ADXRibbonButton(this.components);
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.btnOpenFromSharePoint = new AddinExpress.MSO.ADXRibbonButton(this.components);
            this.imageList2 = new System.Windows.Forms.ImageList(this.components);
            this.btnSaveInSharePoint = new AddinExpress.MSO.ADXRibbonButton(this.components);
            this.btnSettings = new AddinExpress.MSO.ADXRibbonButton(this.components);
            // 
            // adxRibbonTab1
            // 
            this.adxRibbonTab1.Caption = "Stadsportalen";
            this.adxRibbonTab1.Controls.Add(this.adxRibbonGroup1);
            this.adxRibbonTab1.Id = "adxRibbonTab_bbd231db6ab54145ba9d6661782e1e0c";
            this.adxRibbonTab1.Ribbons = ((AddinExpress.MSO.ADXRibbons)(((((AddinExpress.MSO.ADXRibbons.msrOutlookMailRead | AddinExpress.MSO.ADXRibbons.msrOutlookMailCompose)
                        | AddinExpress.MSO.ADXRibbons.msrExcelWorkbook)
                        | AddinExpress.MSO.ADXRibbons.msrWordDocument)
                        | AddinExpress.MSO.ADXRibbons.msrPowerPointPresentation)));
            // 
            // adxRibbonGroup1
            // 
            this.adxRibbonGroup1.Caption = "Inställningar";
            this.adxRibbonGroup1.Controls.Add(this.btnNewFromSharePoint);
            this.adxRibbonGroup1.Controls.Add(this.btnOpenFromSharePoint);
            this.adxRibbonGroup1.Controls.Add(this.btnSaveInSharePoint);
            this.adxRibbonGroup1.Controls.Add(this.btnSettings);
            this.adxRibbonGroup1.Id = "adxRibbonGroup_55bd372cc2934fffb4c3826f86351040";
            this.adxRibbonGroup1.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.adxRibbonGroup1.Ribbons = ((AddinExpress.MSO.ADXRibbons)(((((AddinExpress.MSO.ADXRibbons.msrOutlookMailRead | AddinExpress.MSO.ADXRibbons.msrOutlookMailCompose)
                        | AddinExpress.MSO.ADXRibbons.msrExcelWorkbook)
                        | AddinExpress.MSO.ADXRibbons.msrWordDocument)
                        | AddinExpress.MSO.ADXRibbons.msrPowerPointPresentation)));
            // 
            // btnNewFromSharePoint
            // 
            this.btnNewFromSharePoint.Caption = "Nytt från mall";
            this.btnNewFromSharePoint.Id = "adxRibbonButton_5149810122ce45a5856610ef7465519a";
            this.btnNewFromSharePoint.Image = 0;
            this.btnNewFromSharePoint.ImageList = this.imageList1;
            this.btnNewFromSharePoint.ImageTransparentColor = System.Drawing.Color.White;
            this.btnNewFromSharePoint.Ribbons = ((AddinExpress.MSO.ADXRibbons)(((AddinExpress.MSO.ADXRibbons.msrExcelWorkbook | AddinExpress.MSO.ADXRibbons.msrWordDocument)
                        | AddinExpress.MSO.ADXRibbons.msrPowerPointPresentation)));
            this.btnNewFromSharePoint.Size = AddinExpress.MSO.ADXRibbonXControlSize.Large;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "SharePoint_newdoc.png");
            this.imageList1.Images.SetKeyName(1, "SharePoint_save.png");
            this.imageList1.Images.SetKeyName(2, "SharePoint_open.gif");
            this.imageList1.Images.SetKeyName(3, "Settings.png");
            // 
            // btnOpenFromSharePoint
            // 
            this.btnOpenFromSharePoint.Caption = "Öppna från Stadsportalen";
            this.btnOpenFromSharePoint.Id = "adxRibbonButton_aa7edc7270154e229d3da2c77d6b7270";
            this.btnOpenFromSharePoint.Image = 2;
            this.btnOpenFromSharePoint.ImageList = this.imageList2;
            this.btnOpenFromSharePoint.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnOpenFromSharePoint.Ribbons = ((AddinExpress.MSO.ADXRibbons)(((AddinExpress.MSO.ADXRibbons.msrExcelWorkbook | AddinExpress.MSO.ADXRibbons.msrWordDocument)
                        | AddinExpress.MSO.ADXRibbons.msrPowerPointPresentation)));
            this.btnOpenFromSharePoint.Size = AddinExpress.MSO.ADXRibbonXControlSize.Large;
            // 
            // imageList2
            // 
            this.imageList2.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList2.ImageStream")));
            this.imageList2.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList2.Images.SetKeyName(0, "SharePoint_save.png");
            this.imageList2.Images.SetKeyName(1, "SharePoint_newdoc.png");
            this.imageList2.Images.SetKeyName(2, "SharePoint_open.png");
            // 
            // btnSaveInSharePoint
            // 
            this.btnSaveInSharePoint.Caption = "Spara till Stadsportalen";
            this.btnSaveInSharePoint.Id = "adxRibbonButton_c5479f66dffb4837b325e6a08435c7f0";
            this.btnSaveInSharePoint.Image = 1;
            this.btnSaveInSharePoint.ImageList = this.imageList1;
            this.btnSaveInSharePoint.ImageTransparentColor = System.Drawing.Color.White;
            this.btnSaveInSharePoint.Ribbons = ((AddinExpress.MSO.ADXRibbons)((((AddinExpress.MSO.ADXRibbons.msrOutlookMailRead | AddinExpress.MSO.ADXRibbons.msrExcelWorkbook)
                        | AddinExpress.MSO.ADXRibbons.msrWordDocument)
                        | AddinExpress.MSO.ADXRibbons.msrPowerPointPresentation)));
            this.btnSaveInSharePoint.Size = AddinExpress.MSO.ADXRibbonXControlSize.Large;
            // 
            // btnSettings
            // 
            this.btnSettings.Caption = "Inställningar";
            this.btnSettings.Id = "adxRibbonButton_2fbe3a1276b445dbba5f250e21e37ae4";
            this.btnSettings.Image = 3;
            this.btnSettings.ImageList = this.imageList1;
            this.btnSettings.ImageTransparentColor = System.Drawing.Color.Transparent;
            this.btnSettings.Ribbons = ((AddinExpress.MSO.ADXRibbons)(((AddinExpress.MSO.ADXRibbons.msrExcelWorkbook | AddinExpress.MSO.ADXRibbons.msrWordDocument)
                        | AddinExpress.MSO.ADXRibbons.msrPowerPointPresentation)));
            this.btnSettings.Size = AddinExpress.MSO.ADXRibbonXControlSize.Large;
            this.btnSettings.OnClick += new AddinExpress.MSO.ADXRibbonOnAction_EventHandler(this.btnSettings_OnClick);
            // 
            // AddinModule
            // 
            this.AddinName = "EmpirConnector";
            this.Images = this.imageList1;
            this.RegisterForAllUsers = true;
            this.SupportedApps = ((AddinExpress.MSO.ADXOfficeHostApp)((((AddinExpress.MSO.ADXOfficeHostApp.ohaExcel | AddinExpress.MSO.ADXOfficeHostApp.ohaWord)
                        | AddinExpress.MSO.ADXOfficeHostApp.ohaOutlook)
                        | AddinExpress.MSO.ADXOfficeHostApp.ohaPowerPoint)));

        }
        #endregion
 
        #region Add-in Express automatic code
 
        // Required by Add-in Express - do not modify
        // the methods within this region
 
        public override System.ComponentModel.IContainer GetContainer()
        {
            if (components == null)
                components = new System.ComponentModel.Container();
            return components;
        }
 
        [ComRegisterFunctionAttribute]
        public static void AddinRegister(Type t)
        {
            AddinExpress.MSO.ADXAddinModule.ADXRegister(t);
        }
 
        [ComUnregisterFunctionAttribute]
        public static void AddinUnregister(Type t)
        {
            AddinExpress.MSO.ADXAddinModule.ADXUnregister(t);
        }
 
        public override void UninstallControls()
        {
            base.UninstallControls();
        }

        #endregion

        public static new AddinModule CurrentInstance 
        {
            get
            {
                return AddinExpress.MSO.ADXAddinModule.CurrentInstance as AddinModule;
            }
        }

        public Excel._Application ExcelApp
        {
            get
            {
                return (HostApplication as Excel._Application);
                
            }
        }

        public Word._Application WordApp
        {
            get
            {
                return (HostApplication as Word._Application);
            }
        }

        public Outlook._Application OutlookApp
        {
            get
            {
                return (HostApplication as Outlook._Application);
            }
        }

        public PowerPoint._Application PowerPointApp
        {
            get
            {
                return (HostApplication as PowerPoint._Application);
            }
        }



    }
}

