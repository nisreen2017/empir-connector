﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmpirConnector.Forms
{
    public partial class HidesInfoPanelForm : Form
    {

        Timer _timer;
        private int _ticks = 0;

        public HidesInfoPanelForm()
        {
            InitializeComponent();
        }

        private void HidesInfoPanelForm_Load(object sender, EventArgs e)
        {
            //_timer = new Timer();
            //_timer.Interval = 100;
            //_timer.Tick += new EventHandler(_timer_Tick);
            //_timer.Start();
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            _ticks += 1;
            if (_ticks > 20)
                _ticks = 1;

            string points = "";
            for(int i = 1; i<=_ticks; i++)
                points += ".";

            this.label1.Text = "Döljer informationspanel" + points;
            Application.DoEvents();
        }
    }
}
