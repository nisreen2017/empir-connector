﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sharepoint_Doc.Entities;
using EmpirConnector;
using EmpirConnector.Entities;
using System.Threading;
using Sharepoint_Doc.Controls;
using EmpirConnector.Forms;

namespace Sharepoint_Doc.Forms
{
    public partial class SaveAsDocumentForm : Form
    {
        private struct DistanceFromLeftBottom
        {
            public int FromLeft;
            public int FromBottom;
        }

        private Size _formSize;
        private Size _splitterSize;
        private DistanceFromLeftBottom _pnlFilenameDistance;
        private DistanceFromLeftBottom _pnlOpenCloseDistance;

        private System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();

        public SaveAsDocumentForm()
        {
            InitializeComponent();
        }


        private bool CheckPreload()
        {
            while (AddinModule.PreloadSaveCommand == null)
            {
                System.Windows.Forms.Application.DoEvents();
                Thread.Sleep(200);
            }

            if (AddinModule.PreloadSaveCommand.NetworkError)
            {
                MessageBox.Show(Constants.NETWORK_ERROR_MESSAGE, "Stadsportalen", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            else if (AddinModule.PreloadSaveCommand.Error)
            {
                MessageBox.Show(Constants.ERROR_MESSAGE, "Stadsportalen", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }

            if (!AddinModule.PreloadSaveCommand.Loaded && !AddinModule.PreloadSaveCommand.FromCache)
            {
                this.btnUpdateMyRooms.Visible = false;
                this.lblInfoTextFirstTime.Visible = true;
            }

            AddinModule.PreloadSaveCommand.UpdatedEvent += new PreloadCommand.UpdatedHandler(PreloadSaveCommand_UpdatedEvent);

            return true;
        }

        void PreloadSaveCommand_UpdatedEvent()
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.btnUpdateMyRooms.Enabled = true;

                this.folderTreeView1.Load();
                btnInformation.Visible = false;
                this.lblInfoTextFirstTime.Visible = false;

                if (!lblInfoTextFirstTime.Visible)
                    MessageBox.Show("Mina Rum har uppdaterats.", "Stadsportalen", MessageBoxButtons.OK, MessageBoxIcon.Information);

                lblUpdateMyRooms.Visible = false;
            });
        }

        private void SaveAsDocumentFormcs_Load(object sender, EventArgs e)
        {
            try
            {
                if (!CheckPreload())
                {
                    Waiting.Instance.PrepareHide();
                    return;
                }

                AddinModule.CurrentFunction = AddinModule.CurrentFunctionEnum.SaveAs;

                Waiting.Instance.PrepareHide();

                System.Windows.Forms.Application.DoEvents();

                _formSize = this.Size;
                _splitterSize = this.splitContainerEx1.Size;
                _pnlFilenameDistance.FromBottom = this.Height - this.pnlFilename.Location.Y;
                _pnlOpenCloseDistance.FromBottom = this.Height - this.pnlOpenCancel.Location.Y;
                _pnlOpenCloseDistance.FromLeft = this.Width - this.pnlOpenCancel.Location.X;

                this.previousPageButton1.Init(this.fileListView1);
                this.nextPageButton1.Init(this.fileListView1);

                this.Resize += new EventHandler(SaveAsDocumentForm_Resize);
                this.folderTreeView1.PreloadCommand = AddinModule.PreloadSaveCommand;
                this.folderTreeView1.Init(libraryFolderTreeView1, this.contextMenuStrip1);
                this.folderTreeView1.PermissionCheck = FolderTreeView.ePermissionCheck.Write;
                this.fileListView1.Init(this.fileTypeComboBox1);
                this.fileListView1.FileSelected += new FileListView.FileSelectedHandler(fileListView1_FileSelected);
                this.libraryFolderTreeView1.Init(this.fileListView1, this.contextMenuStrip1);

                this.btnSave.Init(this.folderTreeView1, this.txtFilename, this.libraryFolderTreeView1, this.fileTypeComboBox1);
                this.btnSave.Close += new Sharepoint_Doc.Controls.SaveDocumentButton.CloseHandler(btnSave_Close);

                this.fileTypeComboBox1.Init(this.txtFilename);

                this.folderTreeView1.BeforeExpand += new TreeViewCancelEventHandler(folderTreeView1_BeforeExpand);
                this.folderTreeView1.AfterExpand += new TreeViewEventHandler(folderTreeView1_AfterExpand);
                this.libraryFolderTreeView1.BeforeDisplay += new LibraryFolderTreeView.BeforeDisplayHandler(libraryFolderTreeView1_BeforeDisplay);
                this.libraryFolderTreeView1.AfterDisplay += new LibraryFolderTreeView.AfterDisplayHandler(libraryFolderTreeView1_AfterDisplay);

  
                this.txtFilename.Text = DocumentNameCreator.MakeName();
                this.fileTypeComboBox1.SelectFileType(this.txtFilename.Text);

                _timer = new System.Windows.Forms.Timer();
                _timer.Interval = 5;
                _timer.Tick += new EventHandler(_timer_Tick);
                _timer.Start();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message + ex.StackTrace, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void SaveAsDocumentForm_Resize(object sender, EventArgs e)
        {
            this.splitContainerEx1.Width = _splitterSize.Width + (this.Size.Width - _formSize.Width);
            this.splitContainerEx1.Height = _splitterSize.Height + (this.Size.Height - _formSize.Height);

            this.pnlFilename.Top = this.Height - _pnlFilenameDistance.FromBottom;
            this.pnlOpenCancel.Top = this.Height - this._pnlOpenCloseDistance.FromBottom;
            this.pnlOpenCancel.Left = this.Width - this._pnlOpenCloseDistance.FromLeft;
        }

        void fileListView1_FileSelected(string filename)
        {
            //Dont't clear filename in Save
            if(filename != "")
                this.txtFilename.Text = filename;
        }

        void libraryFolderTreeView1_AfterDisplay()
        {
            Cursor = Cursors.Default;
        }

        void libraryFolderTreeView1_BeforeDisplay()
        {
            Cursor = Cursors.WaitCursor;
        }

        void folderTreeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
        }

        void folderTreeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            Cursor = Cursors.Default;
        }

        void _timer_Tick(object sender, EventArgs e)
        {
            _timer.Stop();

            this.folderTreeView1.Load();
        }




        void btnSave_Close()
        {
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private class DocumentNameCreator
        {
            public static bool IsNewDocument(string fullname)
            {
                return System.IO.Path.GetDirectoryName(fullname) == "";
            }

            public static string MakeName()
            {
                if (AddinModule.CurrentInstance.WordApp != null)
                {
                    if (!IsNewDocument(AddinModule.CurrentInstance.WordApp.ActiveDocument.FullName))
                        return AddinModule.CurrentInstance.WordApp.ActiveDocument.Name;


                    string name = AddinModule.CurrentInstance.WordApp.ActiveDocument.Content.Text.
                        Split(Environment.NewLine.ToCharArray())[0];
                    if (name == "")
                        return AddinModule.CurrentInstance.WordApp.ActiveDocument.Name + ".docx";
                    else
                    {
                        try
                        {
                            if (System.IO.Path.GetExtension(name) == "")
                                return name + ".docx";
                            else
                                return name;
                        }
                        catch
                        {
                            //Konstiga tecken i text, returnera default
                            return AddinModule.CurrentInstance.WordApp.ActiveDocument.Name + ".docx";
                        }

                    }
                }
                else if (AddinModule.CurrentInstance.ExcelApp != null)
                {
                    string name = AddinModule.CurrentInstance.ExcelApp.ActiveWorkbook.Name;
                    if (System.IO.Path.GetExtension(name) == "")
                        return name + ".xlsx";
                    else
                        return name;
                }
                else if (AddinModule.CurrentInstance.PowerPointApp != null)
                    return AddinModule.CurrentInstance.PowerPointApp.ActivePresentation.Name + ".pptx";
                else if (AddinModule.CurrentInstance.OutlookApp != null)
                    return SaveAttachmentForm.OriginalFilename;
                    throw new Exception();

            }
        }

        private void btnEditFolders_Click(object sender, EventArgs e)
        {
            contextMenuStrip1.Show(new Point(Cursor.Position.X- 30, Cursor.Position.Y));
        }

        private void fileTypeComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.fileListView1.FiletypeFilter = this.fileTypeComboBox1.SelectedExtension;
            fileListView1.Page = 1;
            fileListView1.ListFilesInFolder();
        }

        private void SaveAsDocumentForm_Deactivate(object sender, EventArgs e)
        {

        }

        private void SaveAsDocumentForm_Activated(object sender, EventArgs e)
        {

        }

        private void btnInformation_Click(object sender, EventArgs e)
        {
            this.folderTreeView1.Load();
            btnInformation.Visible = false;
            this.lblInfoTextFirstTime.Visible = false;
        }

        private void btnUpdateMyRooms_Click(object sender, EventArgs e)
        {
            btnUpdateMyRooms.Enabled = false;
            lblUpdateMyRooms.Visible = true;
            AddinModule.CurrentInstance.UpdateCache();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }
    }
}
