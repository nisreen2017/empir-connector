﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EmpirConnector.Forms
{
    public class Constants
    {
        public static readonly string EMPIR_TEMPLATE_DIR = "EmpirTemplates";
        public const string MESSAGEBOX_CAPTION = "Stadsportalen";

        public static readonly string NETWORK_ERROR_MESSAGE;

        public const string ERROR_MESSAGE = "Ett problem uppstod vid kontakt med Stadsportalen.";

        public const string INVALID_FILENAME_MESSAGE = "Ogiltigt filnamn.";
        public const string INVALID_FOLDERNAME_MESSAGE = "Ogiltigt mappnamn.";

        public const string EXISTING_FOLDERNAME = "Det finns redan en mapp med detta namn.";
        public const string NO_INTELLIGENCE_FOR_TEMPLATE = "Det finns ingen intelligens konfigurerad för vald förvaltning.";
        public const string PAGEFOOTER_OR_LOGO_MISSING = "Sidfot eller logga saknas för vald förvaltning.";
        public const string ERROR_IN_APPLICTION_MESSAGE = "Ett fel har uppsått i programmet.";

        public const int TIMEOUT_LIMIT = 120;

        public const string CONFIG_PATH = @"C:\Users\konsult-owe\documents\visual studio 2010\Projects\Empir.Connector\Empir.Connector\ConnectorSettings.xml";
        
        static Constants()
        {
            NETWORK_ERROR_MESSAGE = "Programmet kunde ej få kontakt med Stadsportalen." + System.Environment.NewLine +
            "Kontrollera din nätverksanslutning.";

            EMPIR_TEMPLATE_DIR =  Path.Combine(System.IO.Path.GetTempPath(), "EmpirTemplates");
        }
    }
}
