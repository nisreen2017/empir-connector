﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using SP = Microsoft.SharePoint.Client;
using Word = Microsoft.Office.Interop.Word;
using Office = Microsoft.Office.Core;
//using WordTools = Microsoft.Office.Tools.Word;
using System.IO;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Controls;
using Sharepoint_Doc.Entities;
using Microsoft.Office.Interop.Word;
using EmpirConnector.Entities;
using System.Threading;
using EmpirConnector;
using EmpirConnector.Forms;

namespace Sharepoint_Doc
{
    public partial class SharepointDocuments : System.Windows.Forms.Form
    {
        private struct DistanceFromLeftBottom
        {
            public int FromLeft;
            public int FromBottom;
        }

        private Size _formSize;
        private Size _splitterSize;
        private DistanceFromLeftBottom _pnlFilenameDistance;
        private DistanceFromLeftBottom _pnlOpenCloseDistance;

        private System.Windows.Forms.Timer _timer = new System.Windows.Forms.Timer();


        public enum eActionType
        {
            Open,
            Save
        }
        public eActionType ActionType { get; set; }

        public string SelectedFileUrl
        {
            set;
            get;
        }

        public string ParentWebUrl
        {
            get
            {

                return ((Sharepoint_Doc.Entities.DocumentLibrary)this.folderTreeView1.SelectedNode.Tag).Url;
            }
        }

        public string ListId
        {
            get
            {
                return ((DocumentLibrary)this.folderTreeView1.SelectedNode.Tag).ListId;
            }
        }


        private string SelectedWeb;

        public SharepointDocuments(eActionType actionType)
        {
            InitializeComponent();

            this.ActionType = actionType;
            InitControls();
        }

        private void InitControls()
        {

            //this.fileListView1.Init(this.fileTypeComboBox1);
            

            if (this.ActionType == eActionType.Open)
            {
                this.Text = "Öppna från Stadsportalen";
            }
            else
            {
                this.Text = "Spara till Sharepoint";
                this.lblFilter.Visible = false;
                this.lblDocuments.Visible = false;

            }

            this.fileListView1.FileSelected += new FileListView.FileSelectedHandler(fileListView1_FileSelected);
        }

        void fileListView1_FileSelected(string filename)
        {
            this.txtFilename.Text = filename;
            this.btnOpen.Enabled = (filename != string.Empty);
        }

      

        private void tvDocuments_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                CItem iDocument = null; // (CItem)tvDocuments.SelectedNode.Tag;

                if (iDocument.ItemType != CItem.eItemType.Document)
                {
                    return;
                }

                var wordApplication = new Microsoft.Office.Interop.Word.Application { Visible = true };

                // get full path to document
                using (SP.ClientContext context = new SP.ClientContext(iDocument.RootSite + iDocument.SubSite))
                {
                    SP.List lList = context.Web.Lists.GetById(new Guid(iDocument.ListId));
                    SP.ListItem lItem = lList.GetItemById(iDocument.DocumentId);

                    context.Load(lList);
                    context.Load(lItem);
                    context.ExecuteQuery();

                    if (lItem["FileRef"] != null && !string.IsNullOrEmpty(lItem["FileRef"].ToString()))
                    {
                        object filePath = iDocument.RootSite + lItem["FileRef"].ToString();

                        object missing = Type.Missing;
                        object readOnly = false;
                        object isVisible = true;

                        Microsoft.Office.Interop.Word.Document reportDoc = wordApplication.Documents.Open(
                            ref filePath,
                            true,
                            readOnly,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref missing,
                            ref isVisible);

                        reportDoc.Select();
                        reportDoc.Activate();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Init()
        {
            SharepointDocuments_Load(this, EventArgs.Empty);
        }


        private bool CheckPreload()
        {
            
            try
            {
                while (AddinModule.PreloadSaveCommand == null)
                {
                    System.Windows.Forms.Application.DoEvents();
                    Thread.Sleep(200);
                }

                if (AddinModule.PreloadSaveCommand.NetworkError)
                {
                    MessageBox.Show(Constants.NETWORK_ERROR_MESSAGE, "Stadsportalen", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                else if (AddinModule.PreloadSaveCommand.Error)
                {
                    MessageBox.Show(Constants.ERROR_MESSAGE, "Stadsportalen", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }

                if (!AddinModule.PreloadSaveCommand.Loaded && !AddinModule.PreloadSaveCommand.FromCache)
                {
                    this.btnUpdateMyRooms.Visible = false;
                    this.lblInfoTextFirstTime.Visible = true;
                }

                //int time = 0;
                //while (!AddinModule.PreloadSaveCommand.Loaded && time <= Constants.TIMEOUT_LIMIT)
                //{
                //    System.Windows.Forms.Application.DoEvents();
                //    Thread.Sleep(200);
                //}

                //if (time > Constants.TIMEOUT_LIMIT)
                //{
                //    MessageBox.Show("Ett problem uppstod vid kontakt med Stadsportalen." + System.Environment.NewLine +
                //       "Timeout", "Stadsportalen", MessageBoxButtons.OK, MessageBoxIcon.Error);
                //    return false;
                //}

                AddinModule.PreloadOpenCommand.UpdatedEvent += new PreloadCommand.UpdatedHandler(PreloadSaveCommand_UpdatedEvent);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.StackTrace);
            }
            return true;
        }

        void PreloadSaveCommand_UpdatedEvent()
        {
            this.Invoke((MethodInvoker)delegate
            {
                this.btnUpdateMyRooms.Enabled = true;

                this.folderTreeView1.Load();
                btnInformation.Visible = false;
                lblUpdateMyRooms.Visible = false ;

                if(!lblInfoTextFirstTime.Visible)
                    MessageBox.Show("Mina Rum har uppdaterats.", "Stadsportalen", MessageBoxButtons.OK, MessageBoxIcon.Information);

                this.lblInfoTextFirstTime.Visible = false;
            });

        }

        private void SharepointDocuments_Load(object sender, EventArgs e)
        {
            try
            {
                if (!CheckPreload())
                {
                    Waiting.Instance.PrepareHide();
                    return;
                }

                AddinModule.CurrentFunction = AddinModule.CurrentFunctionEnum.Open;

                Waiting.Instance.PrepareHide();

                //Waiting.Instance.Close();
                //Waiting.Instance = null;

               
                Cursor = Cursors.WaitCursor;

                _formSize = this.Size;
                _splitterSize = this.splitContainerEx1.Size;
                _pnlFilenameDistance.FromBottom = this.Height - this.pnlFilename.Location.Y;
                _pnlOpenCloseDistance.FromBottom =  this.Height - this.pnlOpenCancel.Location.Y;
                _pnlOpenCloseDistance.FromLeft = this.Width - this.pnlOpenCancel.Location.X;

                this.Resize += new EventHandler(SharepointDocuments_Resize);
                this.previousPageButton1.Init(this.fileListView1);
                this.nextPageButton1.Init(this.fileListView1);

                this.folderTreeView1.PreloadCommand = AddinModule.PreloadOpenCommand;
                this.folderTreeView1.Init(this.libraryFolderTreeView1, this.contextMenuStrip1);
                this.folderTreeView1.PermissionCheck = FolderTreeView.ePermissionCheck.View;
                this.fileListView1.Init(this.fileTypeComboBox1);
                this.folderTreeView1.BeforeExpand += new TreeViewCancelEventHandler(folderTreeView1_BeforeExpand);
                this.fileListView1.FileSelected +=new FileListView.FileSelectedHandler(fileListView1_FileSelected);
                this.folderTreeView1.AfterExpand += new TreeViewEventHandler(folderTreeView1_AfterExpand);
                this.libraryFolderTreeView1.BeforeDisplay += new LibraryFolderTreeView.BeforeDisplayHandler(libraryFolderTreeView1_BeforeDisplay);
                this.libraryFolderTreeView1.AfterDisplay += new LibraryFolderTreeView.AfterDisplayHandler(libraryFolderTreeView1_AfterDisplay);
                this.libraryFolderTreeView1.Init(this.fileListView1, this.contextMenuStrip1);
                this.fileListView1.OpenFile += new FileListView.OpenFileHandler(fileListView1_OpenFile);
                _timer = new System.Windows.Forms.Timer();
                _timer.Interval = 10;
                _timer.Tick += new EventHandler(_timer_Tick);
                _timer.Start();



            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void SharepointDocuments_Resize(object sender, EventArgs e)
        {
 
            this.splitContainerEx1.Width = _splitterSize.Width +( this.Size.Width - _formSize.Width);
            this.splitContainerEx1.Height = _splitterSize.Height + (this.Size.Height - _formSize.Height);

            this.pnlFilename.Top = this.Height - _pnlFilenameDistance.FromBottom;
            this.pnlOpenCancel.Top = this.Height - this._pnlOpenCloseDistance.FromBottom;
            this.pnlOpenCancel.Left = this.Width - this._pnlOpenCloseDistance.FromLeft;


        }

        void fileListView1_OpenFile()
        {
            btnOpen_Click(this, EventArgs.Empty);
        }

        void libraryFolderTreeView1_AfterDisplay()
        {
            Cursor = Cursors.Default;
        }

        void libraryFolderTreeView1_BeforeDisplay()
        {
            Cursor = Cursors.WaitCursor;
        }

        void folderTreeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
            Cursor = Cursors.WaitCursor;
        }

        void folderTreeView1_AfterExpand(object sender, TreeViewEventArgs e)
        {
            Cursor = Cursors.Default;
        }

       

        void _timer_Tick(object sender, EventArgs e)
        {
            _timer.Stop();

            this.folderTreeView1.Load();

            Cursor = Cursors.Default;
          
        }


        private void btnOpen_Click(object sender, EventArgs e)
        {
            bool hasPermissions = folderTreeView1. CheckPermissions((DocumentLibrary)this.folderTreeView1.SelectedNode.Tag,
                ((Sharepoint_Doc.Entities.Web)this.folderTreeView1.SelectedNode.Parent.Tag).Url);

            if(!hasPermissions)
            {
                MessageBox.Show("Dina rättigheter för denna lista har tagits bort.", Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
               SelectedFileUrl = fileListView1.SelectedFileUrl;
                string parentUrl = AddInGlobals.ConnectorSettings.OneDriveWebUrl; //((Sharepoint_Doc.Entities.Web)this.folderTreeView1.SelectedNode.Parent.Tag).url;
               // SelectedFileUrl = parentUrl + "?d=" + ((DocumentLibrary)this.folderTreeView1.SelectedNode.Tag).ListId;


                this.Close();
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void fileListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnOpen.Enabled = true;
        }

        private void fileTypeComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

                this.fileListView1.FiletypeFilter = this.fileTypeComboBox1.SelectedExtension;
                fileListView1.Page = 1;
                fileListView1.ListFilesInFolder();

        }

        private void btnInformation_Click(object sender, EventArgs e)
        {
            this.folderTreeView1.Load();
            btnInformation.Visible = false;
            this.lblInfoTextFirstTime.Visible = false;
        }

        private void btnUpdateMyRooms_Click(object sender, EventArgs e)
        {
            btnUpdateMyRooms.Enabled = false;
            lblUpdateMyRooms.Visible = true;
            AddinModule.CurrentInstance.UpdateCache();
        }

        private void previousPageButton1_Click(object sender, EventArgs e)
        {

        }
    }

    public class ResizerContol
    {
        private Control _control;

        public ResizerContol(Control control)
        {
            _control = control;

            _control.MouseMove += new MouseEventHandler(_control_MouseMove);
            _control.MouseLeave += new EventHandler(_control_MouseLeave);
        }

        void _control_MouseLeave(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.SizeWE;
        }

        void _control_MouseMove(object sender, MouseEventArgs e)
        {
            Console.WriteLine(_control.Location.X + _control.Size.Width);

            if (e.X == _control.Location.X + _control.Size.Width)
            {
                Cursor.Current = Cursors.SizeWE;
            }
   


        }



    }
}
