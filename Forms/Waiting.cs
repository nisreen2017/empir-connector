﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmpirConnector.Forms
{
    public partial class Waiting : Form
    {
        private DateTime _dt = DateTime.Now;
        private static Waiting _form;
        private static Timer _timer;
        private static Timer _timerStart;

        public static Waiting Instance
        {
            get
            {
                if (_form == null)
                    _form = new Waiting();

                return _form;
            }
            set
            {
                _form = value;
            }
        }

        public  void PrepareHide()
        {
            if (_timerStart != null)
            {
                _timerStart.Stop();
                _timerStart.Dispose();
            }

            _timer = new Timer();
            _timer.Interval = 20;
            _timer.Tick += new EventHandler(_timer_Tick);
            _timer.Start();
        }

        static void _timer_Tick(object sender, EventArgs e)
        {
            

            _timer.Stop();
            _timer.Dispose();

            Instance.Hide();
        }


        public Waiting()
        {
            InitializeComponent();
            this.VisibleChanged += new EventHandler(Waiting_VisibleChanged);
        }

        void Waiting_VisibleChanged(object sender, EventArgs e)
        {
            if (this.Visible)
            {
                _timerStart = new Timer();
                _timerStart.Interval = 10000;
                _timerStart.Tick += new EventHandler(_timerStart_Tick);
                _timerStart.Start();
            }
        }

        void _timerStart_Tick(object sender, EventArgs e)
        {
            this.label1.Visible = true;
        }
    }
}
