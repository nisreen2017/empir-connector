﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sharepoint_Doc.Entities;
using Microsoft.Office.Interop.Word;

namespace Sharepoint_Doc.Forms
{
    public partial class ReplaceLogoForm : Form
    {

        private void ReplaceLogotype()
        {


            List<Microsoft.Office.Interop.Word.Range> ranges = new List<Microsoft.Office.Interop.Word.Range>();

            foreach (Section aSection in AddInGlobals.WordApplication.ActiveDocument.Sections)
            {
                foreach (HeaderFooter aHeader in aSection.Headers)
                {
                    Microsoft.Office.Interop.Word.InlineShape inlineshape = null;


                    foreach (Microsoft.Office.Interop.Word.InlineShape o in aHeader.Range.InlineShapes)
                    {
                        inlineshape = o;
                        break;

                    }

                    if (inlineshape != null)
                    {
                        inlineshape.Delete();
                        aHeader.Range.InlineShapes.AddPicture(this.logosListView1.SelectedItems[0].Tag.ToString());
                        break;
                    }
                }
            }


        }

        public ReplaceLogoForm()
        {
            InitializeComponent();
        }

        private void ReplaceLogoForm_Load(object sender, EventArgs e)
        {
            this.logosListView1.DisplayLogos();
        }

        private void btnReplace_Click(object sender, EventArgs e)
        {
            ReplaceLogotype();

            this.Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }
}
