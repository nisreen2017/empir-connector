﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmpirConnector.Forms
{
    public partial class FormOpenDocument : Form
    {
        public FormOpenDocument()
        {
            InitializeComponent();
        }

        public void OpenDocument(string url)
        {
            webBrowser1.Navigate(url);
        }
    }

    
}
