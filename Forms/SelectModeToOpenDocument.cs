﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmpirConnector.Forms
{
    public partial class SelectModeToOpenDocument : Form
    {

        public bool OpenWriteProtected
        {
            get{
            return radioButton1.Checked;
            }
        }

        public SelectModeToOpenDocument()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Hide();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Hide();
        }
    }
}
