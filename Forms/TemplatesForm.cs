﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sharepoint_Doc.Entities;
using EmpirConnector;

namespace Sharepoint_Doc.Forms
{
    public partial class TemplatesForm : Form
    {
        public delegate void TemplateSelectedHandler(TemplatesForm frm);
        public event TemplateSelectedHandler TemplateSelected;

        public string SelectedTemplateUrl
        {
            get;
            set;
        }

        public bool OpenAsTemplate
        {
            get;
            set;
        }

        public TemplatesForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

            this.Close();
        }

        private void TemplatesForm_Load(object sender, EventArgs e)
        {

            AddinModule.CurrentFunction = AddinModule.CurrentFunctionEnum.Templates;

            DocumentLibrary documentLibrary = DocumentLibrary.GetTemplateLibrary();

    
            AddInGlobals.ExtensionAndFileTypes =
            Sharepoint_Doc.Controls.FileTypeComboBox.FillerBase.MakeFiller().Fill(null, false);

            this.libraryFolderTreeView1.LabelEdit = false;
            this.libraryFolderTreeView1.NoEdit = true;
            this.libraryFolderTreeView1.DefaultView = true;
            this.libraryFolderTreeView1.Init(this.fileListView1, null);
            this.libraryFolderTreeView1.DisplayFolders(documentLibrary);
            this.libraryFolderTreeView1.BeforeSelect += new TreeViewCancelEventHandler(libraryFolderTreeView1_BeforeSelect);

            if(UserHasPermissions())
            {
                this.fileListView1.FiletypeFilter = "*";
                this.fileListView1.Init(null);
                this.fileListView1.SmallImageList = this.imageList;
                this.fileListView1.LargeImageList = this.imageList;
                this.checkBox1.Enabled = documentLibrary.WritePermission;
            }
            else
            {
                libraryFolderTreeView1.Nodes.Clear();
                fileListView1.Items.Clear();
                this.checkBox1.Enabled = false;
            }

            this.fileListView1.PAGE_SIZE = 200;
        }

        void libraryFolderTreeView1_BeforeSelect(object sender, TreeViewCancelEventArgs e)
        {
            this.btnOpen.Enabled = false;
        }

        private bool UserHasPermissions()
        {
            return libraryFolderTreeView1.Nodes[0].Nodes.Count > 0;
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            if(this.fileListView1.SelectedItems.Count == 0)
            {
                MessageBox.Show("Du måste välja en mall.", "Stadsportalen", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return;
            }

            File file = (File)this.fileListView1.SelectedItems[0].Tag;
            if(file.Intelligent && !this.checkBox1.Checked)
            {
                List<string> governanceList = new List<string>();
                foreach (TreeNode n in this.libraryFolderTreeView1.Nodes[0].Nodes)
                {
                    governanceList.Add(n.Text);
                }

                EmpirConnector.Forms.SelectGovernanceForm frm = new EmpirConnector.Forms.SelectGovernanceForm();
                frm.Governances = governanceList;
                frm.ShowDialog();

                this._governance = frm.SelectedGovernance;
                EmpirConnector.Properties.Settings.Default.SelectedGovernanceIndex = frm.SelectedGovernanceIndex;
                EmpirConnector.Properties.Settings.Default.Save();

                frm = null;
            }

            OpenAsTemplate = this.checkBox1.Checked;

            SelectedTemplateUrl = this.fileListView1.SelectedFileUrl;


 
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();

            //
        }

        private void governancesTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void fileListView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.fileListView1.SelectedItems.Count > 0)
                btnOpen_Click(this, EventArgs.Empty);
        }

        private void fileListView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnOpen.Enabled = this.fileListView1.SelectedItems.Count > 0;
        }

    }
}
