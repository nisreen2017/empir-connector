﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sharepoint_Doc.Controls;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Controls.Metadata;
using Sharepoint_Doc.Entities;

namespace Sharepoint_Doc.Forms
{
    public partial class MetadataForm : System.Windows.Forms.Form
    {
        FolderTreeView _folderTreeView;
        ContentType ctToUse = null;

        public string Filename
        {
            get;
            set;
        }

        public string DocumentFullUrl
        {
            set;
            get;
        }

        public MetadataForm()
        {
            InitializeComponent();
        }

        public MetadataForm(FolderTreeView folderTreeView)
        {
            _folderTreeView = folderTreeView;

            InitializeComponent();
        }

        public void Display()
        {

            CItem cItem = null;

            string webFullUrl = "";
            string listId, listName = "";

            if (_folderTreeView != null)
            {
                cItem = (CItem)_folderTreeView.SelectedNode.Tag;
                webFullUrl = cItem.RootSite + cItem.SubSite;
                listId = cItem.ListId;
            }
            else
            {
                string[] directories = DocumentFullUrl.Split("/".ToCharArray());
                for (int i = 0; i <= directories.Length - 4; i++)
                {
                    if (directories[i] == "")
                        webFullUrl += "//";
                    else
                        if (i > 0 && !webFullUrl.EndsWith("/"))
                            webFullUrl += "/";
                        webFullUrl += directories[i];
                }

                listName = directories[directories.Length -3];
            }



            using (ClientContext context = new ClientContext(webFullUrl))
            {
                List list = null;

                Microsoft.SharePoint.Client.File spFile = null;

                if (_folderTreeView == null)
                {
                    spFile = context.Web.GetFileByServerRelativeUrl(DocumentFullUrl.Substring(DocumentFullUrl.ToLower().IndexOf("/kunder")));
                    context.Load(spFile);
                    context.ExecuteQuery();

                    list = context.Web.Lists.GetByTitle(listName);
                }
                else
                {
                    list = context.Web.Lists.GetById(new Guid(cItem.ListId));
                }

                context.Load(list);
                context.Load(list.ContentTypes);
                context.ExecuteQuery();

                

                foreach (ContentType ct in list.ContentTypes)
                {
                    if (ct.Name == "Dokument")
                    {
                        ctToUse = ct;
                        break;
                    }
                }

                int row = 1;

                context.Load(ctToUse.Fields);
                context.ExecuteQuery();
                foreach (Field field in ctToUse.Fields)
                {
                    if (!field.Hidden && !field.ReadOnlyField && field.TypeAsString != "Computed")
                    {
                        Label lblCaption = new Label();
                        lblCaption.Text = field.Title;
                        lblCaption.Location = new Point(20, 40 * row);
                        this.Controls.Add(lblCaption);

                        Control ctrl = MakeControl(field);
                        ctrl.Location = new Point(150, 40 * row);

                        ctrl.Width = 200;
                        if (ctrl is MDChoiceRadioButtons || ctrl is MDChoiceMulti || ctrl is MDNoteTextBox)
                            row += 2;

                        this.Controls.Add(ctrl);

                        if (field.InternalName == "FileLeafRef")
                        {
                            ((TextBox)ctrl).Text = this.Filename;
                        }

                        row++;
                        
                    }
                }

                Label lblDummy = new Label();
                lblDummy.Location = new Point(0, 40 * row + 10);
                this.Controls.Add(lblDummy);

                if (spFile != null)
                {
                    ListItem spFileListItem = spFile.ListItemAllFields;
                    context.Load(spFileListItem);
                    context.ExecuteQuery();

                    foreach (Control ctrl in this.Controls)
                    {
                        if (ctrl is ISaveMetadata)
                        {
                            ((ISaveMetadata)ctrl).DisplayValue   (spFileListItem);
                        }
                    }
                }
            }

        }

        private Control MakeControl(Field field)
        {
            Control ctrl = null;

            switch (field.TypeAsString)
            {
                case "File":
                    ctrl = new MDFile(field);
                    break;
                case "Text":
                    ctrl = new MDText(field);
                    break;
                case "Choice":
                    System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
                    doc.LoadXml(field.SchemaXml);
                    switch (doc.SelectSingleNode("//Field").Attributes["Format"].InnerText)
                    {
                        case "Dropdown":
                            ctrl = new MDChoice(field);
                            break;
                        case "RadioButtons":
                            ctrl = new MDChoiceRadioButtons(field);
                            break;

                    }
                    break;
                case "MultiChoice":
                    ctrl = new MDChoiceRadioButtons(field);
                    break;
                case "Note":
                    ctrl = new MDNoteTextBox(field);
                    ctrl.Height = 100;
                    break;
                case "DateTime":
                    ctrl = new MDDateTime(field);
                    break;
                case "Boolean":
                    ctrl = new MDBoolean(field);
                    break;
                default:
                    ctrl = new Label() { Text = "?????" };
                    break;
            }

            return ctrl;
        }

        private void SaveDocument()
        {
            string url = AddInGlobals.ConnectorSettings.RootUrl;
            string fullname = url + _folderTreeView.SelectedUrl + "/" +
                _folderTreeView.SelectedNode.Text + "/1/" + Filename;

            //TODO
            //AddInGlobals.WordApplication.ActiveDocument.SaveAs2(fullname);
            AddInGlobals.LastSavedDocument = fullname;

            //AddInGlobals.WordApplication.ActiveDocument.Close(true);

            CItem iView = (CItem)this._folderTreeView.SelectedNode.Tag;

            using (ClientContext context = new ClientContext(iView.RootSite + iView.SubSite))
            {
                List list = context.Web.Lists.GetById(Guid.Parse(iView.ListId));

                CamlQuery camlQuery = new CamlQuery();
                camlQuery.ViewXml = 
                    @"<View> 
                    <Query> 
                      <Where> 
                        <Eq> 
                          <FieldRef Name='FileLeafRef'/> 
                          <Value Type='Text'>" + Filename + @"</Value> 
                        </Eq> 
                      </Where> 
                      <RowLimit>1</RowLimit> 
                    </Query> 
                  </View>";

                Microsoft.SharePoint.Client.ListItemCollection collListItem1 = list.GetItems(camlQuery);
                context.Load(collListItem1);
                context.ExecuteQuery();

               
                ListItem fileItem = collListItem1[0];

                fileItem["ContentTypeId"] = ctToUse.Id;

                foreach (Control ctrl in this.Controls)
                {
                    if (ctrl is ISaveMetadata)
                    {
                        ((ISaveMetadata)ctrl).Save(fileItem);
                    }
                }

                fileItem.Update();
                context.ExecuteQuery();

                this.DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Close();

                
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveDocument();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }
    }


}
