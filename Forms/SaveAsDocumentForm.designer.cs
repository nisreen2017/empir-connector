﻿namespace Sharepoint_Doc.Forms
{
    partial class SaveAsDocumentForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SaveAsDocumentForm));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.btnCancel = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.nyttToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bytNamnToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.taBortToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSave = new Sharepoint_Doc.Controls.SaveDocumentButton();
            this.fileTypeComboBox1 = new Sharepoint_Doc.Controls.FileTypeComboBox();
            this.toolTipInfo = new System.Windows.Forms.ToolTip(this.components);
            this.btnInformation = new System.Windows.Forms.Button();
            this.splitContainerEx1 = new SplitContainerEx();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnUpdateMyRooms = new System.Windows.Forms.Button();
            this.folderTreeView1 = new Sharepoint_Doc.Controls.FolderTreeView();
            this.lblWebs = new System.Windows.Forms.Label();
            this.splitContainerEx2 = new SplitContainerEx();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.lblUpdateMyRooms = new System.Windows.Forms.Label();
            this.libraryFolderTreeView1 = new Sharepoint_Doc.Controls.LibraryFolderTreeView();
            this.lblFilter = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.fileListView1 = new Sharepoint_Doc.Controls.FileListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.lblDocuments = new System.Windows.Forms.Label();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.previousPageButton1 = new EmpirConnector.Controls.PreviousPageButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.nextPageButton1 = new EmpirConnector.Controls.NextPageButton();
            this.pnlFilename = new System.Windows.Forms.Panel();
            this.pnlOpenCancel = new System.Windows.Forms.Panel();
            this.lblInfoTextFirstTime = new System.Windows.Forms.Label();
            this.contextMenuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEx1)).BeginInit();
            this.splitContainerEx1.Panel1.SuspendLayout();
            this.splitContainerEx1.Panel2.SuspendLayout();
            this.splitContainerEx1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEx2)).BeginInit();
            this.splitContainerEx2.Panel1.SuspendLayout();
            this.splitContainerEx2.Panel2.SuspendLayout();
            this.splitContainerEx2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.pnlFilename.SuspendLayout();
            this.pnlOpenCancel.SuspendLayout();
            this.SuspendLayout();
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "itdl");
            this.imageList.Images.SetKeyName(1, "SharePointFoundation16");
            this.imageList.Images.SetKeyName(2, "save");
            this.imageList.Images.SetKeyName(3, "folder");
            this.imageList.Images.SetKeyName(4, "icdocx");
            this.imageList.Images.SetKeyName(5, "view");
            this.imageList.Images.SetKeyName(6, "word-icon");
            this.imageList.Images.SetKeyName(7, "excel-icon");
            this.imageList.Images.SetKeyName(8, "powerpoint-icon");
            this.imageList.Images.SetKeyName(9, "ICVDX.GIF");
            this.imageList.Images.SetKeyName(10, "ICVSD.GIF");
            this.imageList.Images.SetKeyName(11, "ICVSL.GIF");
            this.imageList.Images.SetKeyName(12, "ICVSS.GIF");
            this.imageList.Images.SetKeyName(13, "ICVST.GIF");
            this.imageList.Images.SetKeyName(14, "ICVSX.GIF");
            this.imageList.Images.SetKeyName(15, "ICVTX.GIF");
            this.imageList.Images.SetKeyName(16, "ICXLSX.GIF");
            this.imageList.Images.SetKeyName(17, "ICXLT.GIF");
            this.imageList.Images.SetKeyName(18, "ICXLTX.GIF");
            this.imageList.Images.SetKeyName(19, "ICXPS.GIF");
            this.imageList.Images.SetKeyName(20, "ICACCDB.GIF");
            this.imageList.Images.SetKeyName(21, "ICACCDE.GIF");
            this.imageList.Images.SetKeyName(22, "ICASAX.GIF");
            this.imageList.Images.SetKeyName(23, "ICDOC.GIF");
            this.imageList.Images.SetKeyName(24, "ICDOCX.GIF");
            this.imageList.Images.SetKeyName(25, "ICDOT.GIF");
            this.imageList.Images.SetKeyName(26, "ICDOTX.GIF");
            this.imageList.Images.SetKeyName(27, "ICEML.GIF");
            this.imageList.Images.SetKeyName(28, "ICHLP.GIF");
            this.imageList.Images.SetKeyName(29, "ICMSG.GIF");
            this.imageList.Images.SetKeyName(30, "ICONE.GIF");
            this.imageList.Images.SetKeyName(31, "ICONP.GIF");
            this.imageList.Images.SetKeyName(32, "ICONT.GIF");
            this.imageList.Images.SetKeyName(33, "ICPOT.GIF");
            this.imageList.Images.SetKeyName(34, "ICPOTX.GIF");
            this.imageList.Images.SetKeyName(35, "ICPPS.GIF");
            this.imageList.Images.SetKeyName(36, "ICPPT.GIF");
            this.imageList.Images.SetKeyName(37, "ICPPTX.GIF");
            this.imageList.Images.SetKeyName(38, "ICPUB.GIF");
            this.imageList.Images.SetKeyName(39, "ICXLS.GIF");
            this.imageList.Images.SetKeyName(40, "ICXLTM.GIF");
            this.imageList.Images.SetKeyName(41, "ICXLSM.GIF");
            this.imageList.Images.SetKeyName(42, "ICXLSB.GIF");
            this.imageList.Images.SetKeyName(43, "icvisiogeneric.gif");
            this.imageList.Images.SetKeyName(44, "icspdgeneric.gif");
            this.imageList.Images.SetKeyName(45, "ICPPTM.GIF");
            this.imageList.Images.SetKeyName(46, "ICPPSX.GIF");
            this.imageList.Images.SetKeyName(47, "ICPPSM.GIF");
            this.imageList.Images.SetKeyName(48, "ICPPSDC.GIF");
            this.imageList.Images.SetKeyName(49, "ICPOTM.GIF");
            this.imageList.Images.SetKeyName(50, "ICODT.GIF");
            this.imageList.Images.SetKeyName(51, "ICODS.GIF");
            this.imageList.Images.SetKeyName(52, "ICODP.GIF");
            this.imageList.Images.SetKeyName(53, "icinfopathgeneric.gif");
            this.imageList.Images.SetKeyName(54, "ICDOTM.GIF");
            this.imageList.Images.SetKeyName(55, "ICDOCM.GIF");
            this.imageList.Images.SetKeyName(56, "ICDOCSET.GIF");
            this.imageList.Images.SetKeyName(57, "ICODCD.GIF");
            this.imageList.Images.SetKeyName(58, "ICSADREP.GIF");
            this.imageList.Images.SetKeyName(59, "ICODCC.GIF");
            this.imageList.Images.SetKeyName(60, "ICODCT.GIF");
            this.imageList.Images.SetKeyName(61, "ICVDW.GIF");
            this.imageList.Images.SetKeyName(62, "ICTHMX.GIF");
            this.imageList.Images.SetKeyName(63, "ICHDP.GIF");
            this.imageList.Images.SetKeyName(64, "icunpinned.gif");
            this.imageList.Images.SetKeyName(65, "ICPINNED.GIF");
            this.imageList.Images.SetKeyName(66, "ICXDDOC.GIF");
            this.imageList.Images.SetKeyName(67, "ICXSN.GIF");
            this.imageList.Images.SetKeyName(68, "ICMPP.GIF");
            this.imageList.Images.SetKeyName(69, "ICVSW.GIF");
            this.imageList.Images.SetKeyName(70, "ICVSU.GIF");
            this.imageList.Images.SetKeyName(71, "ICMPD.GIF");
            this.imageList.Images.SetKeyName(72, "ICMPT.GIF");
            this.imageList.Images.SetKeyName(73, "ICHTMPUB.GIF");
            this.imageList.Images.SetKeyName(74, "ICHTMXLS.GIF");
            this.imageList.Images.SetKeyName(75, "ICHTMDOC.GIF");
            this.imageList.Images.SetKeyName(76, "ICHTMFP.GIF");
            this.imageList.Images.SetKeyName(77, "icdwt.gif");
            this.imageList.Images.SetKeyName(78, "ICACCDC.GIF");
            this.imageList.Images.SetKeyName(79, "ICSTP.GIF");
            this.imageList.Images.SetKeyName(80, "ICODC.GIF");
            this.imageList.Images.SetKeyName(81, "ICMASTER.GIF");
            this.imageList.Images.SetKeyName(82, "ICONGO01.GIF");
            this.imageList.Images.SetKeyName(83, "icongo01rtl.gif");
            this.imageList.Images.SetKeyName(84, "ICONGO02.GIF");
            this.imageList.Images.SetKeyName(85, "icongo02rtl.gif");
            this.imageList.Images.SetKeyName(86, "ICONGO03.GIF");
            this.imageList.Images.SetKeyName(87, "icongo03rtl.gif");
            this.imageList.Images.SetKeyName(88, "ICASCX.GIF");
            this.imageList.Images.SetKeyName(89, "ICASMX.GIF");
            this.imageList.Images.SetKeyName(90, "ICASP.GIF");
            this.imageList.Images.SetKeyName(91, "ICASPX.GIF");
            this.imageList.Images.SetKeyName(92, "ICBMP.GIF");
            this.imageList.Images.SetKeyName(93, "ICCAT.GIF");
            this.imageList.Images.SetKeyName(94, "ICCHANGE.GIF");
            this.imageList.Images.SetKeyName(95, "ICCHM.GIF");
            this.imageList.Images.SetKeyName(96, "ICCONFIG.GIF");
            this.imageList.Images.SetKeyName(97, "ICCSS.GIF");
            this.imageList.Images.SetKeyName(98, "ICDB.GIF");
            this.imageList.Images.SetKeyName(99, "ICDIB.GIF");
            this.imageList.Images.SetKeyName(100, "ICDISC.GIF");
            this.imageList.Images.SetKeyName(101, "ICDOCP.GIF");
            this.imageList.Images.SetKeyName(102, "ICDVD.GIF");
            this.imageList.Images.SetKeyName(103, "ICDWP.GIF");
            this.imageList.Images.SetKeyName(104, "ICEST.GIF");
            this.imageList.Images.SetKeyName(105, "ICFWP.GIF");
            this.imageList.Images.SetKeyName(106, "ICGEN.GIF");
            this.imageList.Images.SetKeyName(107, "ICGIF.GIF");
            this.imageList.Images.SetKeyName(108, "ICHTA.GIF");
            this.imageList.Images.SetKeyName(109, "ICHTMPPT.GIF");
            this.imageList.Images.SetKeyName(110, "ICHTT.GIF");
            this.imageList.Images.SetKeyName(111, "ICINF.GIF");
            this.imageList.Images.SetKeyName(112, "ICINI.GIF");
            this.imageList.Images.SetKeyName(113, "ICJFIF.GIF");
            this.imageList.Images.SetKeyName(114, "ICJPE.GIF");
            this.imageList.Images.SetKeyName(115, "ICJPEG.GIF");
            this.imageList.Images.SetKeyName(116, "ICJPG.GIF");
            this.imageList.Images.SetKeyName(117, "ICJS.GIF");
            this.imageList.Images.SetKeyName(118, "ICJSE.GIF");
            this.imageList.Images.SetKeyName(119, "ICLOG.GIF");
            this.imageList.Images.SetKeyName(120, "ICMANAGE.GIF");
            this.imageList.Images.SetKeyName(121, "ICMHT.GIF");
            this.imageList.Images.SetKeyName(122, "ICMHTML.GIF");
            this.imageList.Images.SetKeyName(123, "ICMHTPUB.GIF");
            this.imageList.Images.SetKeyName(124, "ICMPS.GIF");
            this.imageList.Images.SetKeyName(125, "ICMPW.GIF");
            this.imageList.Images.SetKeyName(126, "ICMPX.GIF");
            this.imageList.Images.SetKeyName(127, "ICMSI.GIF");
            this.imageList.Images.SetKeyName(128, "ICMSP.GIF");
            this.imageList.Images.SetKeyName(129, "ICNPIE.GIF");
            this.imageList.Images.SetKeyName(130, "ICOCX.GIF");
            this.imageList.Images.SetKeyName(131, "ICPNG.GIF");
            this.imageList.Images.SetKeyName(132, "ICPPTP.GIF");
            this.imageList.Images.SetKeyName(133, "ICPROP.GIF");
            this.imageList.Images.SetKeyName(134, "ICPSP.GIF");
            this.imageList.Images.SetKeyName(135, "ICPTM.GIF");
            this.imageList.Images.SetKeyName(136, "ICPTT.GIF");
            this.imageList.Images.SetKeyName(137, "ICREVIEW.GIF");
            this.imageList.Images.SetKeyName(138, "ICRTF.GIF");
            this.imageList.Images.SetKeyName(139, "ICSMRTPG.GIF");
            this.imageList.Images.SetKeyName(140, "ICSPGEN.GIF");
            this.imageList.Images.SetKeyName(141, "ICSPWEB.GIF");
            this.imageList.Images.SetKeyName(142, "icstorage.gif");
            this.imageList.Images.SetKeyName(143, "ICSTT.GIF");
            this.imageList.Images.SetKeyName(144, "ICTIF.GIF");
            this.imageList.Images.SetKeyName(145, "ICTIFF.GIF");
            this.imageList.Images.SetKeyName(146, "ICTXT.GIF");
            this.imageList.Images.SetKeyName(147, "ICVBE.GIF");
            this.imageList.Images.SetKeyName(148, "ICVBS.GIF");
            this.imageList.Images.SetKeyName(149, "ICWM.GIF");
            this.imageList.Images.SetKeyName(150, "ICWMA.GIF");
            this.imageList.Images.SetKeyName(151, "ICWMD.GIF");
            this.imageList.Images.SetKeyName(152, "ICWMP.GIF");
            this.imageList.Images.SetKeyName(153, "ICWMS.GIF");
            this.imageList.Images.SetKeyName(154, "ICWMV.GIF");
            this.imageList.Images.SetKeyName(155, "ICWMX.GIF");
            this.imageList.Images.SetKeyName(156, "ICWMZ.GIF");
            this.imageList.Images.SetKeyName(157, "ICWSF.GIF");
            this.imageList.Images.SetKeyName(158, "ICXLSP.GIF");
            this.imageList.Images.SetKeyName(159, "ICXML.GIF");
            this.imageList.Images.SetKeyName(160, "ICXSD.GIF");
            this.imageList.Images.SetKeyName(161, "ICXSL.GIF");
            this.imageList.Images.SetKeyName(162, "ICXSLT.GIF");
            this.imageList.Images.SetKeyName(163, "ICZIP.GIF");
            this.imageList.Images.SetKeyName(164, "ICC16.GIF");
            this.imageList.Images.SetKeyName(165, "ICHTM.GIF");
            this.imageList.Images.SetKeyName(166, "ICM16.GIF");
            this.imageList.Images.SetKeyName(167, "ICO16.GIF");
            this.imageList.Images.SetKeyName(168, "ICONGO.GIF");
            this.imageList.Images.SetKeyName(169, "AdobePDF.png");
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(118, 44);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(116, 27);
            this.btnCancel.TabIndex = 56;
            this.btnCancel.Text = "Avbryt";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 53;
            this.label2.Text = "Filtyp:";
            // 
            // txtFilename
            // 
            this.txtFilename.Location = new System.Drawing.Point(55, 2);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(323, 20);
            this.txtFilename.TabIndex = 52;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 13);
            this.label1.TabIndex = 51;
            this.label1.Text = "Filnamn:";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.BackColor = System.Drawing.SystemColors.Control;
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nyttToolStripMenuItem,
            this.bytNamnToolStripMenuItem,
            this.taBortToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(160, 94);
            this.contextMenuStrip1.Text = "Nytt";
            // 
            // nyttToolStripMenuItem
            // 
            this.nyttToolStripMenuItem.Name = "nyttToolStripMenuItem";
            this.nyttToolStripMenuItem.Size = new System.Drawing.Size(159, 30);
            this.nyttToolStripMenuItem.Text = "Ny mapp";
            // 
            // bytNamnToolStripMenuItem
            // 
            this.bytNamnToolStripMenuItem.Name = "bytNamnToolStripMenuItem";
            this.bytNamnToolStripMenuItem.Size = new System.Drawing.Size(159, 30);
            this.bytNamnToolStripMenuItem.Text = "Byt namn";
            // 
            // taBortToolStripMenuItem
            // 
            this.taBortToolStripMenuItem.Name = "taBortToolStripMenuItem";
            this.taBortToolStripMenuItem.Size = new System.Drawing.Size(159, 30);
            this.taBortToolStripMenuItem.Text = "Ta bort";
            // 
            // btnSave
            // 
            this.btnSave.DocumentDeleted = false;
            this.btnSave.Enabled = false;
            this.btnSave.Location = new System.Drawing.Point(0, 44);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(112, 28);
            this.btnSave.TabIndex = 60;
            this.btnSave.Text = "Spara";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // fileTypeComboBox1
            // 
            this.fileTypeComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fileTypeComboBox1.FormattingEnabled = true;
            this.fileTypeComboBox1.Location = new System.Drawing.Point(55, 32);
            this.fileTypeComboBox1.Name = "fileTypeComboBox1";
            this.fileTypeComboBox1.SaveAs = true;
            this.fileTypeComboBox1.Size = new System.Drawing.Size(323, 21);
            this.fileTypeComboBox1.TabIndex = 54;
            this.fileTypeComboBox1.SelectedIndexChanged += new System.EventHandler(this.fileTypeComboBox1_SelectedIndexChanged);
            // 
            // btnInformation
            // 
            this.btnInformation.Image = ((System.Drawing.Image)(resources.GetObject("btnInformation.Image")));
            this.btnInformation.Location = new System.Drawing.Point(398, 30);
            this.btnInformation.Name = "btnInformation";
            this.btnInformation.Size = new System.Drawing.Size(35, 23);
            this.btnInformation.TabIndex = 69;
            this.toolTipInfo.SetToolTip(this.btnInformation, "Trädet har uppdaterats.\r\nKlicka här för att ladda om.\r\n");
            this.btnInformation.UseVisualStyleBackColor = true;
            this.btnInformation.Visible = false;
            this.btnInformation.Click += new System.EventHandler(this.btnInformation_Click);
            // 
            // splitContainerEx1
            // 
            this.splitContainerEx1.Location = new System.Drawing.Point(13, 12);
            this.splitContainerEx1.Name = "splitContainerEx1";
            // 
            // splitContainerEx1.Panel1
            // 
            this.splitContainerEx1.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainerEx1.Panel2
            // 
            this.splitContainerEx1.Panel2.Controls.Add(this.splitContainerEx2);
            this.splitContainerEx1.Size = new System.Drawing.Size(994, 409);
            this.splitContainerEx1.SplitterDistance = 210;
            this.splitContainerEx1.SplitterWidth = 4;
            this.splitContainerEx1.TabIndex = 70;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.btnUpdateMyRooms, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.folderTreeView1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblWebs, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(210, 409);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // btnUpdateMyRooms
            // 
            this.btnUpdateMyRooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnUpdateMyRooms.Location = new System.Drawing.Point(3, 372);
            this.btnUpdateMyRooms.Name = "btnUpdateMyRooms";
            this.btnUpdateMyRooms.Size = new System.Drawing.Size(204, 34);
            this.btnUpdateMyRooms.TabIndex = 20;
            this.btnUpdateMyRooms.Text = "Uppdatera Mina Rum";
            this.btnUpdateMyRooms.UseVisualStyleBackColor = true;
            this.btnUpdateMyRooms.Click += new System.EventHandler(this.btnUpdateMyRooms_Click);
            // 
            // folderTreeView1
            // 
            this.folderTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.folderTreeView1.HideSelection = false;
            this.folderTreeView1.ImageIndex = 0;
            this.folderTreeView1.ImageList = this.imageList;
            this.folderTreeView1.Location = new System.Drawing.Point(3, 23);
            this.folderTreeView1.Name = "folderTreeView1";
            this.folderTreeView1.PreloadCommand = null;
            this.folderTreeView1.SelectedImageIndex = 0;
            this.folderTreeView1.Size = new System.Drawing.Size(204, 343);
            this.folderTreeView1.TabIndex = 18;
            // 
            // lblWebs
            // 
            this.lblWebs.AutoSize = true;
            this.lblWebs.Location = new System.Drawing.Point(3, 0);
            this.lblWebs.Name = "lblWebs";
            this.lblWebs.Size = new System.Drawing.Size(142, 13);
            this.lblWebs.TabIndex = 6;
            this.lblWebs.Text = "Webbplatser/Dokumentlistor";
            // 
            // splitContainerEx2
            // 
            this.splitContainerEx2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainerEx2.Location = new System.Drawing.Point(0, 0);
            this.splitContainerEx2.Name = "splitContainerEx2";
            // 
            // splitContainerEx2.Panel1
            // 
            this.splitContainerEx2.Panel1.Controls.Add(this.tableLayoutPanel2);
            // 
            // splitContainerEx2.Panel2
            // 
            this.splitContainerEx2.Panel2.Controls.Add(this.tableLayoutPanel3);
            this.splitContainerEx2.Size = new System.Drawing.Size(780, 409);
            this.splitContainerEx2.SplitterDistance = 200;
            this.splitContainerEx2.SplitterWidth = 4;
            this.splitContainerEx2.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.lblUpdateMyRooms, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.libraryFolderTreeView1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblFilter, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(200, 409);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // lblUpdateMyRooms
            // 
            this.lblUpdateMyRooms.AutoSize = true;
            this.lblUpdateMyRooms.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblUpdateMyRooms.Location = new System.Drawing.Point(3, 369);
            this.lblUpdateMyRooms.Name = "lblUpdateMyRooms";
            this.lblUpdateMyRooms.Size = new System.Drawing.Size(194, 40);
            this.lblUpdateMyRooms.TabIndex = 23;
            this.lblUpdateMyRooms.Text = "Uppdateringen kan ta upp till 1 minut. Vänta....";
            this.lblUpdateMyRooms.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblUpdateMyRooms.Visible = false;
            // 
            // libraryFolderTreeView1
            // 
            this.libraryFolderTreeView1.DefaultView = false;
            this.libraryFolderTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.libraryFolderTreeView1.FullRowSelect = true;
            this.libraryFolderTreeView1.HideSelection = false;
            this.libraryFolderTreeView1.ImageIndex = 0;
            this.libraryFolderTreeView1.ImageList = this.imageList;
            this.libraryFolderTreeView1.Location = new System.Drawing.Point(3, 23);
            this.libraryFolderTreeView1.Name = "libraryFolderTreeView1";
            this.libraryFolderTreeView1.NoEdit = false;
            this.libraryFolderTreeView1.RootNodeName = "Dokumentbibliotek";
            this.libraryFolderTreeView1.SelectedImageIndex = 0;
            this.libraryFolderTreeView1.Size = new System.Drawing.Size(194, 343);
            this.libraryFolderTreeView1.TabIndex = 21;
            // 
            // lblFilter
            // 
            this.lblFilter.AutoSize = true;
            this.lblFilter.Location = new System.Drawing.Point(3, 0);
            this.lblFilter.Name = "lblFilter";
            this.lblFilter.Size = new System.Drawing.Size(43, 13);
            this.lblFilter.TabIndex = 5;
            this.lblFilter.Text = "Mappar";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.fileListView1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.lblDocuments, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(576, 409);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // fileListView1
            // 
            this.fileListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.fileListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileListView1.FiletypeFilter = null;
            this.fileListView1.FullRowSelect = true;
            this.fileListView1.LargeImageList = this.imageList;
            this.fileListView1.Location = new System.Drawing.Point(3, 23);
            this.fileListView1.MultiSelect = false;
            this.fileListView1.Name = "fileListView1";
            this.fileListView1.NumberOfDocuments = 0;
            this.fileListView1.Page = 0;
            this.fileListView1.Pages = 0;
            this.fileListView1.Size = new System.Drawing.Size(570, 343);
            this.fileListView1.SmallImageList = this.imageList;
            this.fileListView1.TabIndex = 59;
            this.fileListView1.TemplateList = false;
            this.fileListView1.UnlimitedListSize = false;
            this.fileListView1.UseCompatibleStateImageBehavior = false;
            this.fileListView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Namn";
            this.columnHeader1.Width = 180;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Ändrad av";
            this.columnHeader2.Width = 200;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Ändrad";
            this.columnHeader3.Width = 150;
            // 
            // lblDocuments
            // 
            this.lblDocuments.AutoSize = true;
            this.lblDocuments.Location = new System.Drawing.Point(3, 0);
            this.lblDocuments.Name = "lblDocuments";
            this.lblDocuments.Size = new System.Drawing.Size(56, 13);
            this.lblDocuments.TabIndex = 7;
            this.lblDocuments.Text = "Dokument";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 372);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(570, 34);
            this.tableLayoutPanel4.TabIndex = 58;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.previousPageButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(279, 28);
            this.panel1.TabIndex = 60;
            // 
            // previousPageButton1
            // 
            this.previousPageButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.previousPageButton1.Location = new System.Drawing.Point(0, 0);
            this.previousPageButton1.Name = "previousPageButton1";
            this.previousPageButton1.Size = new System.Drawing.Size(279, 28);
            this.previousPageButton1.TabIndex = 59;
            this.previousPageButton1.Text = "<<";
            this.previousPageButton1.UseVisualStyleBackColor = true;
            this.previousPageButton1.Visible = false;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.nextPageButton1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(288, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(279, 28);
            this.panel2.TabIndex = 61;
            // 
            // nextPageButton1
            // 
            this.nextPageButton1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nextPageButton1.Location = new System.Drawing.Point(0, 0);
            this.nextPageButton1.Name = "nextPageButton1";
            this.nextPageButton1.Size = new System.Drawing.Size(279, 28);
            this.nextPageButton1.TabIndex = 60;
            this.nextPageButton1.Text = ">>";
            this.nextPageButton1.UseVisualStyleBackColor = true;
            this.nextPageButton1.Visible = false;
            // 
            // pnlFilename
            // 
            this.pnlFilename.Controls.Add(this.txtFilename);
            this.pnlFilename.Controls.Add(this.label1);
            this.pnlFilename.Controls.Add(this.label2);
            this.pnlFilename.Controls.Add(this.btnInformation);
            this.pnlFilename.Controls.Add(this.fileTypeComboBox1);
            this.pnlFilename.Location = new System.Drawing.Point(13, 429);
            this.pnlFilename.Name = "pnlFilename";
            this.pnlFilename.Size = new System.Drawing.Size(457, 70);
            this.pnlFilename.TabIndex = 71;
            // 
            // pnlOpenCancel
            // 
            this.pnlOpenCancel.Controls.Add(this.btnSave);
            this.pnlOpenCancel.Controls.Add(this.btnCancel);
            this.pnlOpenCancel.Location = new System.Drawing.Point(773, 427);
            this.pnlOpenCancel.Name = "pnlOpenCancel";
            this.pnlOpenCancel.Size = new System.Drawing.Size(243, 77);
            this.pnlOpenCancel.TabIndex = 72;
            // 
            // lblInfoTextFirstTime
            // 
            this.lblInfoTextFirstTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfoTextFirstTime.Location = new System.Drawing.Point(476, 80);
            this.lblInfoTextFirstTime.Name = "lblInfoTextFirstTime";
            this.lblInfoTextFirstTime.Size = new System.Drawing.Size(475, 263);
            this.lblInfoTextFirstTime.TabIndex = 73;
            this.lblInfoTextFirstTime.Text = "Systemet kopplar din användare till Stadsportalen, detta kan ta upp till en minut" +
    " första gången kopplingen skapas.  \r\nVar god vänta";
            this.lblInfoTextFirstTime.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblInfoTextFirstTime.Visible = false;
            // 
            // SaveAsDocumentForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1019, 511);
            this.Controls.Add(this.lblInfoTextFirstTime);
            this.Controls.Add(this.pnlOpenCancel);
            this.Controls.Add(this.pnlFilename);
            this.Controls.Add(this.splitContainerEx1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.SizableToolWindow;
            this.Name = "SaveAsDocumentForm";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Spara till Stadsportalen";
            this.Activated += new System.EventHandler(this.SaveAsDocumentForm_Activated);
            this.Deactivate += new System.EventHandler(this.SaveAsDocumentForm_Deactivate);
            this.Load += new System.EventHandler(this.SaveAsDocumentFormcs_Load);
            this.contextMenuStrip1.ResumeLayout(false);
            this.splitContainerEx1.Panel1.ResumeLayout(false);
            this.splitContainerEx1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEx1)).EndInit();
            this.splitContainerEx1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.splitContainerEx2.Panel1.ResumeLayout(false);
            this.splitContainerEx2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainerEx2)).EndInit();
            this.splitContainerEx2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.pnlFilename.ResumeLayout(false);
            this.pnlFilename.PerformLayout();
            this.pnlOpenCancel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ImageList imageList;
        private Controls.SaveDocumentButton btnSave;
        private System.Windows.Forms.Button btnCancel;
        private Controls.FileTypeComboBox fileTypeComboBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem nyttToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bytNamnToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem taBortToolStripMenuItem;
        private System.Windows.Forms.ToolTip toolTipInfo;
        private System.Windows.Forms.Button btnInformation;
        private SplitContainerEx splitContainerEx1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public Controls.FolderTreeView folderTreeView1;
        private System.Windows.Forms.Label lblWebs;
        private SplitContainerEx splitContainerEx2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Controls.LibraryFolderTreeView libraryFolderTreeView1;
        private System.Windows.Forms.Label lblFilter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label lblDocuments;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.Panel pnlFilename;
        private System.Windows.Forms.Panel pnlOpenCancel;
        private System.Windows.Forms.Panel panel1;
        private EmpirConnector.Controls.PreviousPageButton previousPageButton1;
        private System.Windows.Forms.Panel panel2;
        private EmpirConnector.Controls.NextPageButton nextPageButton1;
        private Controls.FileListView fileListView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Label lblInfoTextFirstTime;
        private System.Windows.Forms.Button btnUpdateMyRooms;
        private System.Windows.Forms.Label lblUpdateMyRooms;

    }
}