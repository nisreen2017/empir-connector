﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sharepoint_Doc.Entities;
using Sharepoint_Doc.Forms;

namespace EmpirConnector.Forms
{
    public partial class SaveAttachmentForm : Form
    {
        private static string _selectedFilename;
        private static string _originalFilename;

        public static string SelectedFilename
        {
            get
            {
                return _selectedFilename;
            }
        }

        public static string OriginalFilename
        {
            get
            {
                return _originalFilename;
            }
        }

        public SaveAttachmentForm()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SaveAttachmentForm_Load(object sender, EventArgs e)
        {
            ListAttachments();
        }

        private void ListAttachments()
        {
            //MessageBox.Show("1");

            Microsoft.Office.Interop.Outlook.MailItem oMail =
                AddInGlobals.MailInspector.CurrentItem as Microsoft.Office.Interop.Outlook.MailItem;

            //MessageBox.Show("2");

            foreach (Microsoft.Office.Interop.Outlook.Attachment attachment in oMail.Attachments)
            {
                //MessageBox.Show("3");
                this.listBox1.Items.Add(attachment.FileName);
               // MessageBox.Show("4");
            }


        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            _selectedFilename = this.listBox1.Text;

            Microsoft.Office.Interop.Outlook.MailItem oMail =
                AddInGlobals.MailInspector.CurrentItem as Microsoft.Office.Interop.Outlook.MailItem;

            foreach (Microsoft.Office.Interop.Outlook.Attachment attachment in oMail.Attachments)
            {
                if (attachment.FileName == this.listBox1.Text)
                {
                    _selectedFilename = Guid.NewGuid().ToString() + System.IO.Path.GetExtension(_selectedFilename);
                    _originalFilename = this.listBox1.Text;

                    attachment.SaveAsFile(System.IO.Path.GetTempPath() + _selectedFilename);

                    break;
                }

            }

            SaveAsDocumentForm frm = new SaveAsDocumentForm();
            frm.ShowDialog();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.btnSave.Enabled = this.listBox1.SelectedIndex >= 0;
        }
    }
}
