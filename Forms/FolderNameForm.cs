﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EmpirConnector.Entities;

namespace EmpirConnector.Forms
{
    public partial class FolderNameForm : Form
    {
        public enum OperationTypeEnum
        {
            NewFolder,
            EditFolder
        }

        public OperationTypeEnum OperationType
        {
            get;
            set;
        }

        public delegate bool CheckFolderNameHandler(string foldername);
        public event CheckFolderNameHandler CheckNewFolderName;
        public event CheckFolderNameHandler CheckEditFolderName;

        public string FolderName
        {
            get
            {
                return this.textBox1.Text;
            }
            set
            {
                textBox1.Text = value;
            }
        }

        private bool CheckFolderName()
        {
            if (!FileNameChecker.CheckName(FolderName))
            {
                MessageBox.Show(Constants.INVALID_FILENAME_MESSAGE, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (OperationType == FolderNameForm.OperationTypeEnum.NewFolder)
            {
                if (!CheckNewFolderName(FolderName))
                {
                    MessageBox.Show(Constants.EXISTING_FOLDERNAME, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }
            else
            {
                if (!CheckEditFolderName(FolderName))
                {
                    MessageBox.Show(Constants.EXISTING_FOLDERNAME, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return false;
                }
            }

            return true;
        }

        public FolderNameForm()
        {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Hide();
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (CheckFolderName())
            {
                DialogResult = System.Windows.Forms.DialogResult.OK;
                this.Hide();
            }

        }
    }
}
