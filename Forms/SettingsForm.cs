﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EmpirConnector.Entities;

namespace EmpirConnector.Forms
{
    public partial class SettingsForm : Form
    {
        public SettingsForm()
        {
            InitializeComponent();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            lblMessage.Visible = this.chkShowDIP.Checked;
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void SettingsForm_Load(object sender, EventArgs e)
        {
            MySettings.Instance.LoadSettings();
            this.chkShowDIP.Checked = MySettings.Instance.ShowDIP;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            MySettings.Instance.ShowDIP = this.chkShowDIP.Checked;
            MySettings.Instance.Save();

            this.Close();
        }
    }
}
