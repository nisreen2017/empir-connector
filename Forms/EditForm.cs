﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;


namespace Sharepoint_Doc.Forms
{
    public partial class EditForm : Form
    {

        public bool DocumentDeleted
        {
            set;
            get;
        }
        public bool Cancel { get; private set; }

        public string parentWeb;
        public string serverSideUrl;
        public string filename;

        private bool _editFormDisplayed = false;
        private Timer _timer = new Timer();

        public EditForm()
        {
            InitializeComponent();

            this.Load += new EventHandler(EditForm_Load);
        }

        void EditForm_Load(object sender, EventArgs e)
        {
            //SHDocVw.WebBrowser instance = (SHDocVw.WebBrowser)webBrowser1.ActiveXInstance;

            SuppressScriptErrorsOnly(webBrowser1);


        }

        public void Display(string url)
        {
            this.webBrowser1.Navigate(url);
            this.webBrowser1.Navigated += new WebBrowserNavigatedEventHandler(webBrowser1_Navigated);
            this.webBrowser1.DocumentCompleted += WebBrowser1_DocumentCompleted;
            _timer.Interval = 1000;

        }

        private void WebBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

            if (webBrowser1.Document != null)
            {
                var htmlDoc = webBrowser1.Document;
                htmlDoc.Click += HtmlDoc_Click;
            }
        }

        private void HtmlDoc_Click(object sender, HtmlElementEventArgs e)
        {
            Cancel = ((System.Windows.Forms.HtmlDocument)sender).ActiveElement.OuterHtml.Contains("Avbryt");

            if(Cancel)
                this.Close();
        }



        void webBrowser1_Navigated(object sender, WebBrowserNavigatedEventArgs e)
        {
            string adress = e.Url.PathAndQuery;

            if (adress.ToLower().Contains("allitems.aspx")|| adress.ToLower().Contains("start.aspx"))
            {
                var dal = new DAL.FileDAL();
               var fileData =  dal.LoadFile(serverSideUrl, parentWeb, "", filename);

                if(fileData == null)
                {
                    DocumentDeleted = true;
                }
                
                _timer.Stop();
                _timer.Dispose();

                this.Close();
            }
        }

        private void SuppressScriptErrorsOnly(WebBrowser browser)
        {
            // Ensure that ScriptErrorsSuppressed is set to false.
            browser.ScriptErrorsSuppressed = true;

            // Handle DocumentCompleted to gain access to the Document object.
            browser.DocumentCompleted +=
                new WebBrowserDocumentCompletedEventHandler(
                    browser_DocumentCompleted);

            browser.Navigating += new WebBrowserNavigatingEventHandler(browser_Navigating);
            browser.DocumentCompleted+=new WebBrowserDocumentCompletedEventHandler(browser_DocumentCompleted);
        }



        void browser_Navigating(object sender, WebBrowserNavigatingEventArgs e)
        {
         
        }

        private bool ClickEventHandler(mshtml.IHTMLEventObj e)
        {


            if (e.srcElement is mshtml.IHTMLInputElement)
            {
                mshtml.IHTMLInputElement inputButton = (mshtml.IHTMLInputElement)e.srcElement;
                if (inputButton.value == "Avbryt")
                {
                    _timer.Stop();
                    _timer.Dispose();

                    this.Close();
                }
            }
            return true;
        }

        private void browser_DocumentCompleted(object sender,
            WebBrowserDocumentCompletedEventArgs e)
        {
            //if (webBrowser1.Document != null && webBrowser1.Document.Body != null)
            //{
            //    string innerHtml = webBrowser1.Document.Body.InnerHtml;

            //    if (innerHtml == null)
            //    {
            //        _timer.Stop();
            //        _timer.Dispose();

            //        this.Close();
            //        return;
            //    }

            //}

            //System.Threading.Thread.Sleep(1000);

            //mshtml.HTMLDocument doc;
            //doc = (mshtml.HTMLDocument)webBrowser1.Document.DomDocument;
            //mshtml.HTMLDocumentEvents2_Event iEvent;
            //iEvent = (mshtml.HTMLDocumentEvents2_Event)doc;
            //iEvent.onclick -= new mshtml.HTMLDocumentEvents2_onclickEventHandler(ClickEventHandler);
            //iEvent.onclick += new mshtml.HTMLDocumentEvents2_onclickEventHandler(ClickEventHandler);
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }
    }
}
