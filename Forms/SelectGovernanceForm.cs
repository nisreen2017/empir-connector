﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sharepoint_Doc.Entities;
using EmpirConnector.Entities;

namespace EmpirConnector.Forms
{
    public partial class SelectGovernanceForm : Form
    {
        public string SelectedGovernance
        {
            get
            {
                return this.listBox1.Text;
            }
        }
        public int SelectedGovernanceIndex
        {
            get
            {
                return this.listBox1.SelectedIndex;
            }
        }

        public List<string> Governances
        {
            get;
            set;
        }

        public SelectGovernanceForm()
        {
            InitializeComponent();
        }

        private void SelectGovernanceForm_Load(object sender, EventArgs e)
        {
            ListGovernances();
        }

        private void ListGovernances()
        {
            TemplateIntelligenceList templateIntelligenceList = TemplateIntelligenceList.GetList();

            foreach (TemplateIntelligence ti in templateIntelligenceList)
            {
                if (templateIntelligenceList.IsGovernancePrepared(ti.ID.ToString()))
                {
                    this.listBox1.Items.Add(ti.Title);
                }
            }


            //foreach (var folder in Governances) //.OrderBy(f => f.Title)
            //{
            //    TemplateIntelligence currentTemplateIntelligence = null;
            //    foreach (TemplateIntelligence ti in templateIntelligenceList)
            //    {
            //        if (ti.Title == folder)
            //        {
            //            currentTemplateIntelligence = ti;
            //            break;
            //        }
            //    }

            //    if (templateIntelligenceList.IsGovernancePrepared(currentTemplateIntelligence.ID.ToString()))
            //    {
            //        this.listBox1.Items.Add(folder);
            //    }
            //}

            if (Properties.Settings.Default.SelectedGovernance != "")
                this.listBox1.Text = Properties.Settings.Default.SelectedGovernance;
            else
                this.listBox1.SelectedIndex = 0;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.SelectedGovernance = this.listBox1.Text;
            Properties.Settings.Default.Save();

            this.Hide();
        }
    }
}
