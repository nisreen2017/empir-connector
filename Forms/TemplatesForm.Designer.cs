﻿using Sharepoint_Doc.Entities;
namespace Sharepoint_Doc.Forms
{
    partial class TemplatesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private string _governance = "";

        public string Governance
        {
            get
            {
                return _governance;
            }
        }

        public string Filename
        {
            get
            {
                return this.fileListView1.SelectedItems[0].Text;
            }
        }

        public bool Intelligent
        {
            get
            {
                return ((File)this.fileListView1.SelectedItems[0].Tag).Intelligent;
            }
        }


        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TemplatesForm));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.fileListView1 = new Sharepoint_Doc.Controls.FileListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.libraryFolderTreeView1 = new Sharepoint_Doc.Controls.LibraryFolderTreeView();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.fileListView1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.libraryFolderTreeView1, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(710, 382);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // fileListView1
            // 
            this.fileListView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2});
            this.fileListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fileListView1.FiletypeFilter = "*";
            this.fileListView1.FullRowSelect = true;
            this.fileListView1.HideSelection = false;
            this.fileListView1.LargeImageList = this.imageList;
            this.fileListView1.Location = new System.Drawing.Point(358, 3);
            this.fileListView1.Name = "fileListView1";
            this.fileListView1.NumberOfDocuments = 0;
            this.fileListView1.Page = 0;
            this.fileListView1.Pages = 0;
            this.fileListView1.Size = new System.Drawing.Size(349, 376);
            this.fileListView1.SmallImageList = this.imageList;
            this.fileListView1.TabIndex = 1;
            this.fileListView1.TemplateList = true;
            this.fileListView1.UnlimitedListSize = true;
            this.fileListView1.UseCompatibleStateImageBehavior = false;
            this.fileListView1.View = System.Windows.Forms.View.Details;
            this.fileListView1.SelectedIndexChanged += new System.EventHandler(this.fileListView1_SelectedIndexChanged);
            this.fileListView1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.fileListView1_MouseDoubleClick);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Namn";
            this.columnHeader1.Width = 163;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Ändrad";
            this.columnHeader2.Width = 85;
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "itdl");
            this.imageList.Images.SetKeyName(1, "SharePointFoundation16");
            this.imageList.Images.SetKeyName(2, "save");
            this.imageList.Images.SetKeyName(3, "folder");
            this.imageList.Images.SetKeyName(4, "icdocx");
            this.imageList.Images.SetKeyName(5, "view");
            this.imageList.Images.SetKeyName(6, "word-icon");
            this.imageList.Images.SetKeyName(7, "excel-icon");
            this.imageList.Images.SetKeyName(8, "powerpoint-icon");
            this.imageList.Images.SetKeyName(9, "ICVDX.GIF");
            this.imageList.Images.SetKeyName(10, "ICVSD.GIF");
            this.imageList.Images.SetKeyName(11, "ICVSL.GIF");
            this.imageList.Images.SetKeyName(12, "ICVSS.GIF");
            this.imageList.Images.SetKeyName(13, "ICVST.GIF");
            this.imageList.Images.SetKeyName(14, "ICVSX.GIF");
            this.imageList.Images.SetKeyName(15, "ICVTX.GIF");
            this.imageList.Images.SetKeyName(16, "ICXLSX.GIF");
            this.imageList.Images.SetKeyName(17, "ICXLT.GIF");
            this.imageList.Images.SetKeyName(18, "ICXLTX.GIF");
            this.imageList.Images.SetKeyName(19, "ICXPS.GIF");
            this.imageList.Images.SetKeyName(20, "ICACCDB.GIF");
            this.imageList.Images.SetKeyName(21, "ICACCDE.GIF");
            this.imageList.Images.SetKeyName(22, "ICASAX.GIF");
            this.imageList.Images.SetKeyName(23, "ICDOC.GIF");
            this.imageList.Images.SetKeyName(24, "ICDOCX.GIF");
            this.imageList.Images.SetKeyName(25, "ICDOT.GIF");
            this.imageList.Images.SetKeyName(26, "ICDOTX.GIF");
            this.imageList.Images.SetKeyName(27, "ICEML.GIF");
            this.imageList.Images.SetKeyName(28, "ICHLP.GIF");
            this.imageList.Images.SetKeyName(29, "ICMSG.GIF");
            this.imageList.Images.SetKeyName(30, "ICONE.GIF");
            this.imageList.Images.SetKeyName(31, "ICONP.GIF");
            this.imageList.Images.SetKeyName(32, "ICONT.GIF");
            this.imageList.Images.SetKeyName(33, "ICPOT.GIF");
            this.imageList.Images.SetKeyName(34, "ICPOTX.GIF");
            this.imageList.Images.SetKeyName(35, "ICPPS.GIF");
            this.imageList.Images.SetKeyName(36, "ICPPT.GIF");
            this.imageList.Images.SetKeyName(37, "ICPPTX.GIF");
            this.imageList.Images.SetKeyName(38, "ICPUB.GIF");
            this.imageList.Images.SetKeyName(39, "ICXLS.GIF");
            this.imageList.Images.SetKeyName(40, "ICXLTM.GIF");
            this.imageList.Images.SetKeyName(41, "ICXLSM.GIF");
            this.imageList.Images.SetKeyName(42, "ICXLSB.GIF");
            this.imageList.Images.SetKeyName(43, "icvisiogeneric.gif");
            this.imageList.Images.SetKeyName(44, "icspdgeneric.gif");
            this.imageList.Images.SetKeyName(45, "ICPPTM.GIF");
            this.imageList.Images.SetKeyName(46, "ICPPSX.GIF");
            this.imageList.Images.SetKeyName(47, "ICPPSM.GIF");
            this.imageList.Images.SetKeyName(48, "ICPPSDC.GIF");
            this.imageList.Images.SetKeyName(49, "ICPOTM.GIF");
            this.imageList.Images.SetKeyName(50, "ICODT.GIF");
            this.imageList.Images.SetKeyName(51, "ICODS.GIF");
            this.imageList.Images.SetKeyName(52, "ICODP.GIF");
            this.imageList.Images.SetKeyName(53, "icinfopathgeneric.gif");
            this.imageList.Images.SetKeyName(54, "ICDOTM.GIF");
            this.imageList.Images.SetKeyName(55, "ICDOCM.GIF");
            this.imageList.Images.SetKeyName(56, "ICDOCSET.GIF");
            this.imageList.Images.SetKeyName(57, "ICODCD.GIF");
            this.imageList.Images.SetKeyName(58, "ICSADREP.GIF");
            this.imageList.Images.SetKeyName(59, "ICODCC.GIF");
            this.imageList.Images.SetKeyName(60, "ICODCT.GIF");
            this.imageList.Images.SetKeyName(61, "ICVDW.GIF");
            this.imageList.Images.SetKeyName(62, "ICTHMX.GIF");
            this.imageList.Images.SetKeyName(63, "ICHDP.GIF");
            this.imageList.Images.SetKeyName(64, "icunpinned.gif");
            this.imageList.Images.SetKeyName(65, "ICPINNED.GIF");
            this.imageList.Images.SetKeyName(66, "ICXDDOC.GIF");
            this.imageList.Images.SetKeyName(67, "ICXSN.GIF");
            this.imageList.Images.SetKeyName(68, "ICMPP.GIF");
            this.imageList.Images.SetKeyName(69, "ICVSW.GIF");
            this.imageList.Images.SetKeyName(70, "ICVSU.GIF");
            this.imageList.Images.SetKeyName(71, "ICMPD.GIF");
            this.imageList.Images.SetKeyName(72, "ICMPT.GIF");
            this.imageList.Images.SetKeyName(73, "ICHTMPUB.GIF");
            this.imageList.Images.SetKeyName(74, "ICHTMXLS.GIF");
            this.imageList.Images.SetKeyName(75, "ICHTMDOC.GIF");
            this.imageList.Images.SetKeyName(76, "ICHTMFP.GIF");
            this.imageList.Images.SetKeyName(77, "icdwt.gif");
            this.imageList.Images.SetKeyName(78, "ICACCDC.GIF");
            this.imageList.Images.SetKeyName(79, "ICSTP.GIF");
            this.imageList.Images.SetKeyName(80, "ICODC.GIF");
            this.imageList.Images.SetKeyName(81, "ICMASTER.GIF");
            this.imageList.Images.SetKeyName(82, "ICONGO01.GIF");
            this.imageList.Images.SetKeyName(83, "icongo01rtl.gif");
            this.imageList.Images.SetKeyName(84, "ICONGO02.GIF");
            this.imageList.Images.SetKeyName(85, "icongo02rtl.gif");
            this.imageList.Images.SetKeyName(86, "ICONGO03.GIF");
            this.imageList.Images.SetKeyName(87, "icongo03rtl.gif");
            this.imageList.Images.SetKeyName(88, "ICASCX.GIF");
            this.imageList.Images.SetKeyName(89, "ICASMX.GIF");
            this.imageList.Images.SetKeyName(90, "ICASP.GIF");
            this.imageList.Images.SetKeyName(91, "ICASPX.GIF");
            this.imageList.Images.SetKeyName(92, "ICBMP.GIF");
            this.imageList.Images.SetKeyName(93, "ICCAT.GIF");
            this.imageList.Images.SetKeyName(94, "ICCHANGE.GIF");
            this.imageList.Images.SetKeyName(95, "ICCHM.GIF");
            this.imageList.Images.SetKeyName(96, "ICCONFIG.GIF");
            this.imageList.Images.SetKeyName(97, "ICCSS.GIF");
            this.imageList.Images.SetKeyName(98, "ICDB.GIF");
            this.imageList.Images.SetKeyName(99, "ICDIB.GIF");
            this.imageList.Images.SetKeyName(100, "ICDISC.GIF");
            this.imageList.Images.SetKeyName(101, "ICDOCP.GIF");
            this.imageList.Images.SetKeyName(102, "ICDVD.GIF");
            this.imageList.Images.SetKeyName(103, "ICDWP.GIF");
            this.imageList.Images.SetKeyName(104, "ICEST.GIF");
            this.imageList.Images.SetKeyName(105, "ICFWP.GIF");
            this.imageList.Images.SetKeyName(106, "ICGEN.GIF");
            this.imageList.Images.SetKeyName(107, "ICGIF.GIF");
            this.imageList.Images.SetKeyName(108, "ICHTA.GIF");
            this.imageList.Images.SetKeyName(109, "ICHTMPPT.GIF");
            this.imageList.Images.SetKeyName(110, "ICHTT.GIF");
            this.imageList.Images.SetKeyName(111, "ICINF.GIF");
            this.imageList.Images.SetKeyName(112, "ICINI.GIF");
            this.imageList.Images.SetKeyName(113, "ICJFIF.GIF");
            this.imageList.Images.SetKeyName(114, "ICJPE.GIF");
            this.imageList.Images.SetKeyName(115, "ICJPEG.GIF");
            this.imageList.Images.SetKeyName(116, "ICJPG.GIF");
            this.imageList.Images.SetKeyName(117, "ICJS.GIF");
            this.imageList.Images.SetKeyName(118, "ICJSE.GIF");
            this.imageList.Images.SetKeyName(119, "ICLOG.GIF");
            this.imageList.Images.SetKeyName(120, "ICMANAGE.GIF");
            this.imageList.Images.SetKeyName(121, "ICMHT.GIF");
            this.imageList.Images.SetKeyName(122, "ICMHTML.GIF");
            this.imageList.Images.SetKeyName(123, "ICMHTPUB.GIF");
            this.imageList.Images.SetKeyName(124, "ICMPS.GIF");
            this.imageList.Images.SetKeyName(125, "ICMPW.GIF");
            this.imageList.Images.SetKeyName(126, "ICMPX.GIF");
            this.imageList.Images.SetKeyName(127, "ICMSI.GIF");
            this.imageList.Images.SetKeyName(128, "ICMSP.GIF");
            this.imageList.Images.SetKeyName(129, "ICNPIE.GIF");
            this.imageList.Images.SetKeyName(130, "ICOCX.GIF");
            this.imageList.Images.SetKeyName(131, "ICPNG.GIF");
            this.imageList.Images.SetKeyName(132, "ICPPTP.GIF");
            this.imageList.Images.SetKeyName(133, "ICPROP.GIF");
            this.imageList.Images.SetKeyName(134, "ICPSP.GIF");
            this.imageList.Images.SetKeyName(135, "ICPTM.GIF");
            this.imageList.Images.SetKeyName(136, "ICPTT.GIF");
            this.imageList.Images.SetKeyName(137, "ICREVIEW.GIF");
            this.imageList.Images.SetKeyName(138, "ICRTF.GIF");
            this.imageList.Images.SetKeyName(139, "ICSMRTPG.GIF");
            this.imageList.Images.SetKeyName(140, "ICSPGEN.GIF");
            this.imageList.Images.SetKeyName(141, "ICSPWEB.GIF");
            this.imageList.Images.SetKeyName(142, "icstorage.gif");
            this.imageList.Images.SetKeyName(143, "ICSTT.GIF");
            this.imageList.Images.SetKeyName(144, "ICTIF.GIF");
            this.imageList.Images.SetKeyName(145, "ICTIFF.GIF");
            this.imageList.Images.SetKeyName(146, "ICTXT.GIF");
            this.imageList.Images.SetKeyName(147, "ICVBE.GIF");
            this.imageList.Images.SetKeyName(148, "ICVBS.GIF");
            this.imageList.Images.SetKeyName(149, "ICWM.GIF");
            this.imageList.Images.SetKeyName(150, "ICWMA.GIF");
            this.imageList.Images.SetKeyName(151, "ICWMD.GIF");
            this.imageList.Images.SetKeyName(152, "ICWMP.GIF");
            this.imageList.Images.SetKeyName(153, "ICWMS.GIF");
            this.imageList.Images.SetKeyName(154, "ICWMV.GIF");
            this.imageList.Images.SetKeyName(155, "ICWMX.GIF");
            this.imageList.Images.SetKeyName(156, "ICWMZ.GIF");
            this.imageList.Images.SetKeyName(157, "ICWSF.GIF");
            this.imageList.Images.SetKeyName(158, "ICXLSP.GIF");
            this.imageList.Images.SetKeyName(159, "ICXML.GIF");
            this.imageList.Images.SetKeyName(160, "ICXSD.GIF");
            this.imageList.Images.SetKeyName(161, "ICXSL.GIF");
            this.imageList.Images.SetKeyName(162, "ICXSLT.GIF");
            this.imageList.Images.SetKeyName(163, "ICZIP.GIF");
            this.imageList.Images.SetKeyName(164, "ICC16.GIF");
            this.imageList.Images.SetKeyName(165, "ICHTM.GIF");
            this.imageList.Images.SetKeyName(166, "ICM16.GIF");
            this.imageList.Images.SetKeyName(167, "ICO16.GIF");
            this.imageList.Images.SetKeyName(168, "ICONGO.GIF");
            // 
            // libraryFolderTreeView1
            // 
            this.libraryFolderTreeView1.DefaultView = false;
            this.libraryFolderTreeView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.libraryFolderTreeView1.FullRowSelect = true;
            this.libraryFolderTreeView1.HideSelection = false;
            this.libraryFolderTreeView1.Location = new System.Drawing.Point(3, 3);
            this.libraryFolderTreeView1.Name = "libraryFolderTreeView1";
            this.libraryFolderTreeView1.NoEdit = false;
            this.libraryFolderTreeView1.RootNodeName = "Kommungemensamma mallar";
            this.libraryFolderTreeView1.Size = new System.Drawing.Size(349, 376);
            this.libraryFolderTreeView1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnOpen);
            this.panel2.Controls.Add(this.btnCancel);
            this.panel2.Controls.Add(this.checkBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 391);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(710, 46);
            this.panel2.TabIndex = 3;
            // 
            // btnOpen
            // 
            this.btnOpen.Enabled = false;
            this.btnOpen.Location = new System.Drawing.Point(508, 10);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(85, 27);
            this.btnOpen.TabIndex = 3;
            this.btnOpen.Text = "Öppna";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(610, 10);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(85, 27);
            this.btnCancel.TabIndex = 4;
            this.btnCancel.Text = "Avbryt";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(9, 15);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(90, 17);
            this.checkBox1.TabIndex = 4;
            this.checkBox1.Text = "Redigera mall";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 88.18182F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.81818F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(716, 440);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // TemplatesForm
            // 
            this.AcceptButton = this.btnOpen;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(716, 440);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "TemplatesForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Mallar";
            this.Load += new System.EventHandler(this.TemplatesForm_Load);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Controls.FileListView fileListView1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private Controls.LibraryFolderTreeView libraryFolderTreeView1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ImageList imageList;

    }
}