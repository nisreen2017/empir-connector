﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EmpirConnector
{
    public class DesignTimeChecker
    {



        public static bool IsDesignMode
        {
            get
            {

                System.Diagnostics.Process process = System.Diagnostics.Process.GetCurrentProcess();
                bool res = process.ProcessName == "devenv";
                process.Dispose();
                return res;

            }
        }


    }
}
