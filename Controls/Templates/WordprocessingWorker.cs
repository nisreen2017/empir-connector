﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;
using System.Diagnostics;
using DocumentFormat.OpenXml.Wordprocessing;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using A = DocumentFormat.OpenXml.Drawing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;

namespace EmpirConnector.Controls.Templates
{
    public class WordprocessingWorker : IDisposable
    {
        //HEIGT = 2,26cm
        //WIDTH = 3,44cm;

        System.IO.Stream data = null;
        private string _sourceFile;
        private string _outputFile;
        private WordprocessingDocument _inMemoryDocument;
        private MemoryStream _inMemoryStream;
        public MemoryStream _logoStream;
        public MemoryStream _footerStream;

        public int logoHeight;
        public int logoWidth;

        /// <summary>
        /// Returns True if the document has been opened.
        /// </summary>
        public bool IsOpen
        {
            get { return (_inMemoryDocument != null); }
        }

        /// <summary>
        /// Simple constructor
        /// </summary>
        public WordprocessingWorker()
        {
        }

        #region IDisposable Members

        /// <summary>
        /// This class wraps objects that implement IDisposable, so we better comply
        /// to ensure their resources get released when ours do.
        /// </summary>
        public void Dispose()
        {
            Close();
        }

        #endregion



        /// <summary>
        /// Opens the specified Document file.
        /// This loads it in memory for later modification.
        /// </summary>
        /// <param name="fileName"></param>
        public void Open(string fileName)
        {
            //if the document is already Open well close it first to free up resources.
            if (IsOpen)
            {
                Close();
            }

            _sourceFile = fileName;

            //populate a MemoryStream with byte array of the Document
            byte[] sourceBytes = File.ReadAllBytes(_sourceFile);
            _inMemoryStream = new MemoryStream();
            _inMemoryStream.Write(sourceBytes, 0, (int)sourceBytes.Length);

            //Create the in-memory Document from the stream for modification
            _inMemoryDocument = WordprocessingDocument.Open(_inMemoryStream, true);
        }

        public void Open(MemoryStream stream)
        {
            //Create the in-memory Document from the stream for modification
            _inMemoryStream = stream;
            _inMemoryDocument = WordprocessingDocument.Open(_inMemoryStream, true);
        }


        /// <summary>
        /// Validate the in-memory Document. Throws an exception if it is invalid.
        /// </summary>
        public void Validate()
        {
            if (!IsOpen)
            {
                throw new DocumentNotOpenException("The object must be Open before calling Validate()");
            }

            if (_inMemoryDocument != null)
            {
                //_inMemoryDocument.Validate(null);
            }
        }

        /// <summary>
        /// Takes the in-memory Document and saves it to the specified file. Overwriting if necessary.
        /// </summary>
        /// <param name="fileName"></param>
        public void SaveAs(string fileName)
        {
            if (!IsOpen)
            {
                throw new DocumentNotOpenException("The object must be Open before calling SaveAs()");
            }

            _outputFile = fileName;
            if (!string.IsNullOrEmpty(_outputFile))
            {
                //Close the in-memory document to ensure the memory stream is ready for saving
                CloseInMemoryDocument();
                try
                {

                    //Now save the stream to file
                    using (FileStream fileStream = new FileStream(_outputFile, System.IO.FileMode.Create))
                    {
                        _inMemoryStream.WriteTo(fileStream);
                    }

                }
                finally
                {
                    //Now close the memory stream. 
                    //The in-memory document has already been closed above 
                    //and there's no point having one without the other!
                    CloseInMemoryStream();
                }
            }
        }

        /// <summary>
        /// Closes and disposes the in-memory document
        /// </summary>
        private void CloseInMemoryDocument()
        {
            if (_inMemoryDocument != null)
            {
                _inMemoryDocument.Close();
                _inMemoryDocument.Dispose();
                _inMemoryDocument = null;
            }
        }

        /// <summary>
        /// Closes and disposes the memory stream
        /// </summary>
        private void CloseInMemoryStream()
        {
            if (_inMemoryStream != null)
            {
                _inMemoryStream.Close();
                _inMemoryStream.Dispose();
                _inMemoryStream = null;
            }
        }

        public void Close()
        {
            CloseInMemoryDocument();
            CloseInMemoryStream();
        }


        private void GetFooter()
        {


        }

        /// <summary>
        /// 
        /// </summary>
        public void ApplyHeaderAndFooter()
        {
            if (!IsOpen)
            {
                throw new DocumentNotOpenException("The object must be Open before calling ApplyHeaderAndFooter()");
            }

            MainDocumentPart mainPart = _inMemoryDocument.MainDocumentPart;

            if (mainPart == null)
            {
                mainPart = _inMemoryDocument.AddMainDocumentPart();
                mainPart.Document = MakeEmpyDocument();
            }
            else
            {
                // Delete the existing header and Footer parts.
                //mainPart.DeleteParts(mainPart.HeaderParts);
                mainPart.DeleteParts(mainPart.FooterParts);
            }

            GetFooter();

            //Multi-sectioned documents seem to have their SectionProperties hidden away inside paragraphs.
            //Go through all these first, and set a flag if we find any.
            bool paragraphSectionPropertiesFound = false;
            IEnumerable<Paragraph> paragraphElements = mainPart.Document.Body.Elements<Paragraph>().AsEnumerable();
            foreach (Paragraph paragraph in paragraphElements)
            {
                ParagraphProperties paragraphProperties = paragraph.Elements<ParagraphProperties>().LastOrDefault();
                if (paragraphProperties != null)
                {
                    SectionProperties sectionProps = paragraphProperties.Elements<SectionProperties>().LastOrDefault();
                    if (sectionProps != null)
                    {
                        ApplyHeaderToSectionProperties(mainPart, sectionProps);
                        ApplyFooterToSectionProperties(mainPart, sectionProps);
                        paragraphSectionPropertiesFound = true;
                    }
                }
            }

            //documents can also have SectionProperties under the body. Particularly single section Documents.
            SectionProperties bodySectionProperties = mainPart.Document.Body.Elements<SectionProperties>().LastOrDefault();

            //if no SectionProperties were found in paragraphs, we will populate the SectionProperties found in the Body instead
            if (paragraphSectionPropertiesFound)
            {
                //SectionProperties were set in the paragraph(s) which should be sufficient, 
                //We'll just remove any header/footer references we find in the Body SectionProperties
                if (bodySectionProperties != null)
                {
                    bodySectionProperties.RemoveAllChildren<HeaderReference>();
                    bodySectionProperties.RemoveAllChildren<FooterReference>();
                }
            }
            else
            {
                if (bodySectionProperties == null)
                {
                    //not there? create it
                    bodySectionProperties = new SectionProperties();
                    ApplyHeaderToSectionProperties(mainPart, bodySectionProperties);
                    ApplyFooterToSectionProperties(mainPart, bodySectionProperties);
                    mainPart.Document.Body.Append(bodySectionProperties);
                }
                else
                {
                    ApplyHeaderToSectionProperties(mainPart, bodySectionProperties);
                    ApplyFooterToSectionProperties(mainPart, bodySectionProperties);
                }
            }

            mainPart.Document.Save();
        }


        public void ReplaceLogoInHeader(string filename)
        {
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(filename, true))
            {
                MainDocumentPart mainPart2 = wordDoc.MainDocumentPart;

                bool imgFound = false;

                System.IO.FileStream fs = new System.IO.FileStream(System.IO.Path.GetTempPath() + "logo.png", System.IO.FileMode.Open);
                foreach (HeaderPart headerPart in mainPart2.HeaderParts)
                {
                    foreach (ImagePart ip in headerPart.ImageParts)
                    {
                        ip.FeedData(fs);


                        string docTextHeader = null;
                        using (StreamReader sr = new StreamReader(headerPart.GetStream()))
                        {
                            docTextHeader = sr.ReadToEnd();
                        }

                        Regex regexText = null;
                        regexText = new Regex("1238250");
                        docTextHeader = regexText.Replace(docTextHeader, this.logoWidth.ToString()); //1270000
                        regexText = new Regex("813600"); //812670
                        docTextHeader = regexText.Replace(docTextHeader, this.logoHeight.ToString());

                        using (StreamWriter sw = new StreamWriter(headerPart.GetStream(FileMode.Create)))
                        {
                            sw.Write(docTextHeader);
                        }

                        imgFound = true;
                        break;

                    }

                    if (imgFound)
                        break;
                }




            }
        }

        private void ApplyHeaderToSectionProperties(MainDocumentPart mainPart, SectionProperties sectionProps)
        {
            if (_logoStream != null)
                MakeHeader(_logoStream, mainPart, sectionProps);

        }

        private void ApplyFooterToSectionProperties(MainDocumentPart mainPart, SectionProperties sectionProps)
        {
            if (_footerStream == null)
                return;

            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(_footerStream, false))
            {
                MainDocumentPart mainPart2 = wordDoc.MainDocumentPart;

                int index = 0;
                long maxLength = 0;
                int maxIndex = 0;

                foreach (FooterPart fp in mainPart2.FooterParts)
                {
                    string xml = fp.RootElement.OuterXml;
                    if (xml.Length > maxLength)
                    {
                        maxIndex = index;
                        maxLength = xml.Length;
                    }

                    index++;
                }

                data = mainPart2.FooterParts.ElementAt(maxIndex).GetStream();

                //foreach (FooterPart fp in mainPart2.FooterParts)
                //{
                //    string id = mainPart2.GetIdOfPart(fp);

                //    string xml = fp.RootElement.OuterXml;

                //    if (xml.Contains("___"))
                //        data = fp.GetStream();

                //}

                // Create a new Footer part and grab its id for the Footer reference.
                FooterPart footerPart = mainPart.AddNewPart<FooterPart>();
                footerPart.FeedData(data);
                string rId = mainPart.GetIdOfPart(footerPart);

                //create our Footer reference
                FooterReference footerRef = new FooterReference();
                footerRef.Id = rId;

                sectionProps.RemoveAllChildren<FooterReference>();
                sectionProps.Append(footerRef);

                //Now populate the Footer contents
                //footerPart.Footer = MakeFooter();
                footerPart.Footer.Save();
            }


        }




        public Document MakeEmpyDocument()
        {
            Document doc = new Document();

            Body body = new Body();
            Paragraph paragraph = new Paragraph();
            Run run = new Run();
            Text text = new Text("");
            run.Append(text);
            paragraph.Append(run);
            body.Append(paragraph);
            doc.Append(body);

            return doc;
        }


        public void MakeHeader(MemoryStream sourceStream, MainDocumentPart mainPart, SectionProperties sectionProps)
        {
            System.Drawing.Image image = System.Drawing.Image.FromStream(_logoStream);
            logoHeight = image.Height * 9525;
            logoWidth = image.Width * 9525;

            FileStream file = new FileStream(System.IO.Path.GetTempPath() + "logo.png", FileMode.Create, System.IO.FileAccess.Write);

            sourceStream.WriteTo(file);
            sourceStream.Close();
            file.Close();

            return;

            file = new FileStream(System.IO.Path.GetTempPath() + "loga.png", FileMode.Open);

            foreach (HeaderPart headerPart in mainPart.HeaderParts)
            {
                if (headerPart.ImageParts.Count() > 0)
                {
                    headerPart.ImageParts.First().FeedData(file);
                    break;
                }
            }


            return;



            const string wordmlNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
            const string relationshipNamespace = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
            string reference = "w:headerReference";



            HeaderPart headPart = mainPart.AddNewPart<HeaderPart>();

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(mainPart.GetStream());

            HeaderXmlLoader xmlLoader = new HeaderXmlLoader();
            XmlDocument header = xmlLoader.GetDocument();

            header.Save(headPart.GetStream());

            ImagePart imgpart = headPart.AddNewPart<ImagePart>("image/png", null);

            //ImagePart imgpart = headPart.AddImagePart(ImagePartType.Png);
            using (Stream targetStream = imgpart.GetStream())
            {
                byte[] buffer = new byte[1024];
                int nrBytesWritten = sourceStream.Read(buffer, 0, 1024);

                while (nrBytesWritten > 0)
                {
                    targetStream.Write(buffer, 0, nrBytesWritten);
                    nrBytesWritten = sourceStream.Read(buffer, 0, 1024);
                }
            }



            //string imgId = headPart.GetIdOfPart(imgpart);
            //string relId = mainPart.GetIdOfPart(headPart);

            //NameTable nt = new NameTable();
            //XmlNamespaceManager nsManager1 = new XmlNamespaceManager(nt);
            //nsManager1.AddNamespace("a", "http://schemas.openxmlformats.org/drawingml/2006/main");

            //XmlNode blip = header.SelectSingleNode("//a:blip", nsManager1);
            //XmlAttribute embed = blip.Attributes["embed", "http://schemas.openxmlformats.org/officeDocument/2006/relationships"];
            //embed.Value = imgId;

            //XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);
            //nsManager.AddNamespace("w", wordmlNamespace);
            //XmlNode targetNode = xDoc.SelectSingleNode("//w:sectPr", nsManager);
            //XmlElement node = xDoc.CreateElement(reference, wordmlNamespace);
            //XmlAttribute attr = node.Attributes.Append(xDoc.CreateAttribute("r:id", relationshipNamespace));
            //attr.Value = relId;

            //targetNode.InsertBefore(node, targetNode.FirstChild);


            //headPart.Header.Save();
            //xDoc.Save(mainPart.GetStream());






            //Header header = new Header();
            //Paragraph paragraph = new Paragraph();
            //Run run = new Run();
            //DocumentFormat.OpenXml.Wordprocessing.
            //Text text = new Text();
            //text.Text = "Simple header";
            //run.Append(text);
            //paragraph.Append(run);
            //header.Append(paragraph);

            //return header;
        }

        public Footer MakeFooter()
        {
            return null;


        }
    }

    public class HeaderXmlLoader
    {
        public XmlDocument GetDocument()
        {
            string[] names = this.GetType().Assembly.GetManifestResourceNames();
            string result = string.Empty;

            string xmlResouce = "EmpirConnector.Controls.Templates.WordHeader.xml";
            using (Stream s = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(xmlResouce))
            {
                using (StreamReader sr = new StreamReader(s))
                {
                    result = sr.ReadToEnd();
                }
            }


            XmlDocument doc = new XmlDocument();
            doc.LoadXml(result);
            return doc;
        }
    }
}
