﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;
using System.IO;
using Sharepoint_Doc.Entities;
using EmpirConnector.Entities;

namespace Sharepoint_Doc.Controls.Templates
{
    public class TemplateListView: ListView
    {
        public bool Kommungemensamma
        {
            set;
            get;
        }


        public Governance Governance
        {
            set;
            get;
        }

        public void ListGovernanceTemplates()
        {

        }

        public void ListTemplates()
        {
            string url = AddInGlobals.ConnectorSettings.TemplateListWebsUrl;

            if (Kommungemensamma)
            {
                using (ClientContext context = new ClientContext(url))
                {
                    List list = context.Web.Lists.GetByTitle("Kommungemensamma");
                    context.Load(list);
                    context.ExecuteQuery();

                    string filter = "";
                    filter += "<Eq>";
                    filter += "<FieldRef Name=\"File_x0020_Type\"/>";
                    filter += "<Value Type=\"Text\">dotx</Value>";
                    filter += "</Eq>";

                    CamlQuery camlQuery = new CamlQuery();
                    camlQuery.ViewXml = "<View Scope='Recursive'><Query><Where>" + filter + "</Where></Query></View>";

                    ListItemCollection items = list.GetItems(camlQuery);
                    context.Load(items);

                    context.ExecuteQuery();

                    foreach (ListItem item in items)
                    {
                        string filename = item["FileLeafRef"].ToString();
                        ListViewItem lvwItem = this.Items.Add(System.IO.Path.GetFileNameWithoutExtension(filename));
                        lvwItem.ImageKey = "WordTemplate";
                        lvwItem.Tag = url + filename;
                    }
                }
            }

        }
    }
}
