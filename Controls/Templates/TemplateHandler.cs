﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
//using DocumentFormat.OpenXml.Packaging;
//using DocumentFormat.OpenXml.Wordprocessing;
using System.Text.RegularExpressions;
using Microsoft.SharePoint.Client;
using System.Net;
using Sharepoint_Doc.Entities;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using EmpirConnector.Entities;
using DocumentFormat.OpenXml.Drawing;
using System.Xml;

namespace Sharepoint_Doc.Controls.Templates
{
    class TemplateHandler
    {
        public static void PopulateTemplate(string path, string governance)
        {
            TemplateIntelligenceList list = TemplateIntelligenceList.GetList();
            TemplateIntelligence templateIntelligence = list.Where(t => t.Title == governance).First();

            byte[] logo = templateIntelligence.Logo;

           MemoryStream logoStream = new MemoryStream(logo);
           FileStream logoWriter = new FileStream(System.IO.Path.GetTempPath() + "logo.bmp", FileMode.Create);
            logoWriter.Write(logo, 0, logo.Length);
            logoWriter.Close();
            logoWriter.Dispose();

            System.Drawing.Image image = System.Drawing.Image.FromStream(logoStream);
            int logoHeight = image.Height * 12700;
            int logoWidth = image.Width * 12700;

            AddHeader(path, System.IO.Path.GetTempPath() + "logo.bmp", logoHeight, logoWidth);

            string s = System.Guid.NewGuid().ToString();

            Byte[] docBytes = System.IO.File.ReadAllBytes(path);
            using (MemoryStream ms = new MemoryStream())
            {
                ms.Write(docBytes, 0, docBytes.Length);

                using (WordprocessingDocument wpdoc = WordprocessingDocument.Open(ms, true))
                {
                    MainDocumentPart mainPart = wpdoc.MainDocumentPart;
                    Document doc = mainPart.Document;

                    HeaderPart headerPart = mainPart.HeaderParts.First();
                    FooterPart footerPart = mainPart.FooterParts.First();

                    string docTextHeader = null;
                    using (StreamReader sr = new StreamReader(headerPart.GetStream()))
                    {
                        docTextHeader = sr.ReadToEnd();
                    }

                    string docText = null;
                    using (StreamReader sr = new StreamReader(footerPart.GetStream()))
                    {
                        docText = sr.ReadToEnd();
                    }

                    Regex regexText = null;

                    regexText = new Regex("Sidfot");
                    //docText = regexText.Replace(docText, templateIntelligence.Footer.Replace("<div>", "").Replace("</div>", ""));



                    regexText = new Regex("xx");
                    docTextHeader = regexText.Replace(docTextHeader, logoWidth.ToString()); //1270000
                    regexText = new Regex("yy");
                    docTextHeader = regexText.Replace(docTextHeader, logoHeight.ToString());

                    using (StreamWriter sw = new StreamWriter(footerPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docText);
                    }

                    using (StreamWriter sw = new StreamWriter(headerPart.GetStream(FileMode.Create)))
                    {
                        sw.Write(docTextHeader);
                    }
                }

                if (System.IO.File.Exists(path))
                    System.IO.File.Delete(path);

                FileStream resultFile = new FileStream(path, FileMode.CreateNew);
                resultFile.Write(ms.GetBuffer(), 0, (int)ms.Length);
                resultFile.Close();
                ms.Close();
            }


        }

        private static void AddHeader(string docPath, string imgPath, int logoHeight, int logoWidth)
        {
           const string wordmlNamespace = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
            const string relationshipNamespace = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
            string reference = "w:headerReference";
            using (WordprocessingDocument wordDoc = WordprocessingDocument.Open(docPath, true))
            {

                MainDocumentPart mainPart = wordDoc.MainDocumentPart;
                XmlDocument xDoc = new XmlDocument();
                xDoc.Load(mainPart.GetStream());

                HeaderPart headPart = mainPart.AddNewPart<HeaderPart>();



                XmlDocument header = new XmlDocument();
                header.Load(@"C:\Users\konsult-owe\Documents\Visual Studio 2010\Projects\Empir.Connector\Empir.Connector\HeaderXML\WordHeader.xml");

                header.Save(headPart.GetStream());

                ImagePart imgpart = headPart.AddImagePart(ImagePartType.Bmp);

                //add image file to the image part
                using (Stream targetStream = imgpart.GetStream())
                {
                    using (FileStream sourceStream = new FileStream(imgPath,
                            FileMode.Open, FileAccess.Read))
                    {
                        byte[] buffer = new byte[1024];
                        int nrBytesWritten = sourceStream.Read(buffer, 0, 1024);
                        while (nrBytesWritten > 0)
                        {
                            targetStream.Write(buffer, 0, nrBytesWritten);
                            nrBytesWritten = sourceStream.Read(buffer, 0, 1024);
                        }
                    }
                }

                string imgId = headPart.GetIdOfPart(imgpart);



                string relId = mainPart.GetIdOfPart(headPart);

                NameTable nt = new NameTable();
                XmlNamespaceManager nsManager1 = new XmlNamespaceManager(nt);

                nsManager1.AddNamespace("a", "http://schemas.openxmlformats.org/drawingml/2006/main");



                XmlNode blip = header.SelectSingleNode("//a:blip", nsManager1);
                XmlAttribute embed = blip.Attributes["embed", "http://schemas.openxmlformats.org/officeDocument/2006/relationships"];
                embed.Value = imgId;

                header.Save(headPart.GetStream());


                XmlNamespaceManager nsManager = new XmlNamespaceManager(nt);

                nsManager.AddNamespace("w", wordmlNamespace);


                XmlNode targetNode = xDoc.SelectSingleNode("//w:sectPr", nsManager);


                XmlElement node = xDoc.CreateElement(reference, wordmlNamespace);

                XmlAttribute attr = node.Attributes.Append(xDoc.CreateAttribute("r:id", relationshipNamespace));

                attr.Value = relId;

                //   node.Attributes.Append(attr);

                targetNode.InsertBefore(node, targetNode.FirstChild);

                //xDoc.Save(mainPart.GetStream());

                //wordDoc.Close();
            }

        }

        private static string GetLogo()
        {
            return "";


        }
    }
}
