﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using EmpirConnector.Entities;
using Sharepoint_Doc.Controls.Templates;

namespace EmpirConnector.Controls.GovernancesTabControl
{
    public class GovernancesTabControl: TabControl
    {
        public void ListGovernances()
        {
            Entities.GovernancesList list = Entities.GovernancesList.GetList();

            foreach (Governance governance in list)
            {
                TabPage tabPage = new TabPage(governance.Name);
                TemplateListView listView = new TemplateListView();
                listView.Dock = DockStyle.Fill;
                tabPage.Controls.Add(listView);
                this.TabPages.Add(tabPage);
            }

            this.SelectedIndexChanged += new EventHandler(GovernancesTabControl_SelectedIndexChanged);

        }

        void GovernancesTabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            TemplateListView listView = (TemplateListView)this.TabPages[this.SelectedIndex].Controls[0];
        }


    }
}
