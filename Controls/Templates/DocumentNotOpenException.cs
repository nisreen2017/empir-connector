﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EmpirConnector.Controls.Templates
{
    public class DocumentNotOpenException : ApplicationException
    {
        public DocumentNotOpenException(string message)
            : base(message)
        {
        }

        public DocumentNotOpenException(string message, Exception innerException)
            : base(message, innerException)
        {
        }
    }
}
