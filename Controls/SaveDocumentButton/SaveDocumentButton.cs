﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sharepoint_Doc.Entities;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Forms;
using System.Net;
using Sharepoint_Doc.DAL;
using EmpirConnector;
using EmpirConnector.Entities.Office;
using EmpirConnector.Entities;
using EmpirConnector.Forms;

namespace Sharepoint_Doc.Controls
{

    public class SaveDocumentButton: Button
    {
        public delegate void CloseHandler();
        public event CloseHandler Close;

        private FolderTreeView _folderTreeView;
        private LibraryFolderTreeView _libraryFolderTreeView;

        private TextBox _txtFilename;
        private FileTypeComboBox _fileTypeComboBox;

        public bool DocumentDeleted
        {
            set;
            get;
        }

        public void Init(FolderTreeView folderTreeView, TextBox txtFilename, LibraryFolderTreeView libraryFolderTreeView, FileTypeComboBox fileTypeComboBox)
        {
            _folderTreeView = folderTreeView;
            _libraryFolderTreeView = libraryFolderTreeView;
            _txtFilename = txtFilename;
            _fileTypeComboBox = fileTypeComboBox;

            _libraryFolderTreeView.AfterSelect += new TreeViewEventHandler(_libraryFolderTreeView_AfterSelect);
            _libraryFolderTreeView.ListCleared += new LibraryFolderTreeView.ListClearedHandler(_libraryFolderTreeView_ListCleared);
      

            this.Click += new EventHandler(SaveDocumentButton_Click);
            txtFilename.TextChanged += new EventHandler(txtFilename_TextChanged);
        }

        private void CheckEnable()
        {
            this.Enabled = _libraryFolderTreeView.SelectedNode != null && _txtFilename.Text != "";

        }

        void txtFilename_TextChanged(object sender, EventArgs e)
        {
           CheckEnable();
        }

        void _libraryFolderTreeView_ListCleared()
        {
            CheckEnable();
        }

        void _libraryFolderTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            CheckEnable();
        }

        void SaveDocumentButton_Click(object sender, EventArgs e)
        {
            try
            {
                bool hasPermissions = _folderTreeView.CheckPermissions((DocumentLibrary)_folderTreeView.SelectedNode.Tag,
                    ((Sharepoint_Doc.Entities.Web)_folderTreeView.SelectedNode.Parent.Tag).Url);

                if (!hasPermissions)
                    MessageBox.Show("Dina rättigheter för denna lista har tagits bort.", Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (SaveDocument())
                        this.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ett fel uppstod i programmet:" + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private bool SaveDocument()
        {
            int fileId = 0;
            string uniqueId = "";

            OfficeDocument officeDocument = OfficeDocument.GetDocument();
            DocumentLibrary library = (DocumentLibrary)this._folderTreeView.SelectedNode.Tag;
            Entities.Folder folder = (Entities.Folder)_libraryFolderTreeView.SelectedNode.Tag;

            string filename = _txtFilename.Text;

            if (!FileNameChecker.CheckName(filename))
            {
                MessageBox.Show(Constants.INVALID_FILENAME_MESSAGE, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                return false;
            }

            if (System.IO.Path.GetExtension(filename) == "")
            {
                filename = System.IO.Path.ChangeExtension(filename, _fileTypeComboBox.SelectedExtension);
                //filename = string.Concat(filename, ".", _fileTypeComboBox.SelectedExtension);
            }

            FileDAL dal = new FileDAL();
            Microsoft.SharePoint.Client.File checkDocument = dal.LoadFile(folder.ServerRelativeUrl, folder.ParentWebUrl, folder.ListId, filename);
            if (checkDocument != null)
            {
                DialogResult dialogResult = MessageBox.Show("Det finns redan ett dokument med samma namn. Vill du ersätta detta?", "Connector", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (dialogResult == System.Windows.Forms.DialogResult.No)
                    return false;

                dal.DeleteFile(folder.ServerRelativeUrl, folder.ParentWebUrl, folder.ListId, filename);
            }


            string fullname;

            if (folder.IsOneDrive)
            { 
            var fullUrl = folder.ServerRelativeUrl;
            string userName = ConnectorUser.Username;
            var result = fullUrl.ToLower().Substring(fullUrl.IndexOf(userName.ToLower()) + ConnectorUser.Username.Length);
             fullname = folder.ParentWebUrl + result + "/" + filename;
            }

            else
            { 

            fullname = folder.WebApplication + folder.ServerRelativeUrl + "/" + filename;
            }

            AddInGlobals.LastSavedDocument = fullname;

            //Make sure i's set
            OutlookDocument.ParentWebUrl = folder.ParentWebUrl;
            OutlookDocument.ListName = library.Title;

            officeDocument.Extension = this._fileTypeComboBox.SelectedExtension;
            officeDocument.Save(fullname,  this._fileTypeComboBox.SelectedFileType);
            officeDocument.Close();

            FileDAL fileDAL = new FileDAL();
            fileDAL.SaveFileInSharePoint2(folder.ParentWebUrl, folder.ServerRelativeUrl + "/" + filename, System.IO.Path.GetTempPath() + officeDocument.TempFilename);
            System.IO.File.Delete(System.IO.Path.GetTempPath() + officeDocument.TempFilename);

            Microsoft.SharePoint.Client.File document = dal.LoadFile(folder.ServerRelativeUrl, folder.ParentWebUrl, folder.ListId, filename);
            fileId = int.Parse(document.ListItemAllFields["ID"].ToString());
            //uniqueId = document.ListItemAllFields["uniqueId"].ToString();

            dal.SetDefaultContentType(folder.ServerRelativeUrl, folder.ParentWebUrl, folder.ListId, filename);
    
            string listName = this._folderTreeView.SelectedNode.Text.Replace(" ", "%20");
            string rootFolder = folder.ServerRelativeUrl;


            string editFormUrl = library.DefaultEditFormUrl + "?ID={0}&RootFolder={1}&IsDlg=1&Source="+rootFolder; //added by me
            editFormUrl = string.Format(editFormUrl, fileId, rootFolder.Replace("/", "%2F").Replace(" ", "%20"));

            bool checkInOK = false;
            bool retVal = true; //why setting it here and not changed?


            if(folder.IsOneDrive)
            {
                officeDocument.Open(fullname, OfficeDocument.OpenModeEnum.None);
            }
            else
            {
                EditForm frm = new EditForm();
                frm.parentWeb = folder.ParentWebUrl;
                frm.serverSideUrl = folder.ServerRelativeUrl;
                frm.filename = filename;

                frm.Display(editFormUrl );
                frm.ShowDialog();
 
                //if(frm.ShowDialog ()==DialogResult.Cancel)
                //    return retVal;

                if (frm.DocumentDeleted || frm.Cancel)
                {
                    if(frm.Cancel)
                    {
                        dal.DeleteFile(folder.ServerRelativeUrl, folder.ParentWebUrl, folder.ListId, filename);
                    }
                    DocumentDeleted = true;
                }
                else
                {
                    filename = dal.GetFilenameByID(folder.ParentWebUrl, fileId, folder.ListId);
                    fullname = folder.WebApplication + folder.ServerRelativeUrl + "/" + filename;

                   // SelectedFileUrl = ((Sharepoint_Doc.Entities.Web)this.folderTreeView1.SelectedNode.Parent.Tag).Url + "?d=" + ((DocumentLibrary)this.folderTreeView1.SelectedNode.Tag).ListId;


                    checkInOK = dal.CheckFileIn(folder.WebApplication, folder.ServerRelativeUrl, folder.ParentWebUrl, folder.ListId, filename);

                    if (!checkInOK)
                    {
                        MessageBox.Show("Dokumentet har sparats men ej checkats in." + Environment.NewLine + "Komplettera dokumentegenskaperna för dokumentet via Stadsportalen.", Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else
                    {
                        officeDocument.Open(fullname, OfficeDocument.OpenModeEnum.None);
                    }
                }

            }


            return retVal;
        }

    }
}
