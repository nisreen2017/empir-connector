﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;

namespace Sharepoint_Doc.Controls.Metadata
{
    public interface ISaveMetadata
    {
        void Save(ListItem listItem);
        void DisplayValue(ListItem listItem);
    }
}
