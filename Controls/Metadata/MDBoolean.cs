﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;

namespace Sharepoint_Doc.Controls.Metadata
{
    public class MDBoolean : CheckBox, ISaveMetadata
    {
        private Field _field;

        public MDBoolean(Field field)
        {
            _field = field;
        }

        public void Save(ListItem listItem)
        {
            listItem[_field.StaticName] = this.Checked;
        }


        public void DisplayValue(ListItem listItem)
        {
            if (listItem[_field.StaticName] != null)
                this.Checked = bool.Parse(listItem[_field.StaticName].ToString());
        }
    }
}
