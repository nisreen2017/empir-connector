﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;
using System.Xml;

namespace Sharepoint_Doc.Controls.Metadata
{
    public class MDChoiceRadioButtons: ListBox
    {

        private Field _field;

        public MDChoiceRadioButtons(Field field)
        {
            _field = field;

            LoadChoices(field.SchemaXml);
            
        }

        private void LoadChoices(string xml)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(xml);

            XmlNodeList  nodeList = doc.SelectNodes("//CHOICE");

            foreach(XmlNode node in nodeList)
            {
                this.Items.Add(node.InnerText);
            }


        }
    }
}
