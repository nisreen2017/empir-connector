﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;

namespace Sharepoint_Doc.Controls.Metadata
{
    public class MDNoteTextBox: TextBox, ISaveMetadata
    {
         Field _field;

         public MDNoteTextBox(Field field)
        {
            _field = field;

            this.Multiline = true;
        }

         public void Save(ListItem listItem)
         {
             listItem[_field.StaticName] = this.Text;
         }

         public void DisplayValue(ListItem listItem)
         {
             if (listItem[_field.StaticName] != null)
                this.Text = listItem[_field.StaticName].ToString();
         }
    }
}