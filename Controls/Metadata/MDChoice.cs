﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;
using System.Xml;

namespace Sharepoint_Doc.Controls.Metadata
{
    public class MDChoice: ComboBox, ISaveMetadata
    {
        private Field _field;

        public MDChoice(Field field)
        {
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            _field = field;

            LoadChoices(field.SchemaXml);
            
        }

        private void LoadChoices(string xml)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            doc.LoadXml(xml);

            XmlNodeList  nodeList = doc.SelectNodes("//CHOICE");

            this.Items.Add("");
            foreach(XmlNode node in nodeList)
            {
                this.Items.Add(node.InnerText);
            }


        }

        public void Save(ListItem listItem)
        {
            listItem[_field.StaticName] = this.SelectedText;
        }

        public void DisplayValue(ListItem listItem)
        {
            if(listItem[_field.StaticName] != null)
            {
                string text = listItem[_field.StaticName].ToString();
                this.SelectedText = text;
            }
        }
    }
}
