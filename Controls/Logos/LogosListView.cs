﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;
using System.IO;
using System.Drawing;
using System.Net;
using Sharepoint_Doc.Entities;

namespace Sharepoint_Doc.Controls.Logos
{
    public class LogosListView: ListView
    {
        ImageList imageList1 = new ImageList();
        int baseValue = 0;

        public void DisplayLogos()
        {

            this.LargeImageList = imageList1;

            using (ClientContext context = new ClientContext(AddInGlobals.ConnectorSettings.TemplateListWebsUrl))
            {
                List list = context.Web.Lists.GetByTitle("Loggor");
                context.Load(list);
                context.ExecuteQuery();

                CamlQuery camlQuery = new CamlQuery();
                camlQuery.ViewXml = "<View Scope='Recursive'><Query></Query></View>";

                ListItemCollection items = list.GetItems(camlQuery);
                context.Load(items);

                context.ExecuteQuery();

                imageList1.ImageSize = new Size(100, 100);

                foreach (ListItem item in items)
                {
                    string filename = item["FileLeafRef"].ToString();

                    string logo = AddInGlobals.ConnectorSettings.TemplateListWebsUrl + "/" + filename;

                    NetworkCredential credentials = new NetworkCredential("olandgren", "123OOOooo!!!", "empircloud.se");
                    WebClient webClient = new WebClient();
                    webClient.Credentials = credentials;
                    webClient.DownloadFile(logo, System.IO.Path.GetTempPath() + filename);

                    addImage(System.IO.Path.GetTempPath() + filename);
                }
            }
        }

        private void addImage(string imageToLoad)
        {
            if (imageToLoad != "")
            {
                imageList1.Images.Add(Image.FromFile(imageToLoad));
                this.BeginUpdate();
                ListViewItem listViewItem = this.Items.Add(imageToLoad, baseValue++);
                listViewItem.Text = Path.GetFileNameWithoutExtension(imageToLoad);
                listViewItem.Tag = imageToLoad;
                this.EndUpdate();
            }
        } 

    }
}
