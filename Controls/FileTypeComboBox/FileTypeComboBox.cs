﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Reflection;
using Sharepoint_Doc.Entities;
using EmpirConnector;

namespace Sharepoint_Doc.Controls
{
    public class FileTypeComboBox: ComboBox
    {
        public List<List<string>> Extensions;
        public TextBox _txtFilename;

        public bool SaveAs { get; set; }

        public class CItem
        {
            public enum eItemType
            {
                Web,
                Document
            }

            public string WebId { get; set; }
            public string ListId { get; set; }
            public string ViewId { get; set; }
            public int DocumentId { get; set; }
            public string RootSite { get; set; }
            public string SubSite { get; set; }
            public string Title { get; set; }
            public eItemType ItemType { get; set; }

            public CItem()
            {
            }

            public override string ToString()
            {
                return Title;
            }

        }

        public string SelectedExtension
        {
            get
            {
                if (this.Items[0].ToString().ToLower().Contains("alla"))
                {
                    if (this.SelectedIndex == 0)
                        return "*";
                    else
                        return AddInGlobals.ExtensionAndFileTypes.Extensions[this.SelectedIndex - 1];
                }
                return AddInGlobals.ExtensionAndFileTypes.Extensions[this.SelectedIndex];
            }
        }

        public string SelectedFileType
        {
            get
            {
                if (AddInGlobals.ExtensionAndFileTypes.FileTypes == null)
                    return "";
                else
                    return AddInGlobals.ExtensionAndFileTypes.FileTypes[this.SelectedIndex];
            }
        }


        public FileTypeComboBox()
        {
            this.DropDownStyle = ComboBoxStyle.DropDownList;
            this.SelectedIndexChanged += new EventHandler(FileTypeComboBox_SelectedIndexChanged);
        }

        void FileTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (_txtFilename != null)
            {
                string filename = System.IO.Path.GetFileNameWithoutExtension(_txtFilename.Text);

                _txtFilename.Text = filename + "." + AddInGlobals.ExtensionAndFileTypes.Extensions[this.SelectedIndex];
            }
        }

        public void Init(TextBox txtFilename)
        {
            _txtFilename = txtFilename;
        }

        public void SelectFileType(string fullname)
        {
            string fileExtension = System.IO.Path.GetExtension(fullname).Substring(1);
            int index = 0;

            foreach (string extension in AddInGlobals.ExtensionAndFileTypes.Extensions)
            {
                if (fileExtension == extension)
                {
                    this.SelectedIndex = index;
                    break;
                }
                index++;
            }
        }

        private void Fill()
        {
            this.Items.Clear();

            Extensions = new List<List<string>>();

            AddInGlobals.ExtensionAndFileTypes = FillerBase.MakeFiller().Fill(this, SaveAs);


            //if(AddinModule.CurrentInstance.WordApp != null)
            //{
            //        if (!SaveAs)
            //        {
            //            this.Items.Add("Alla Word dokument (*.docx, *.docm, *.dotx, )");
            //        }
            //        this.Items.Add("Word dokument (*.docx)");

            //        List<string> list = new List<string>();
            //        list.Add("docx");
            //        list.Add("docm");
            //        list.Add("dotx");
            //        list.Add("dotm");
            //        list.Add("doc");
            //        list.Add("dot");
            //        list.Add("rtf");
            //        Extensions.Add(list);

            //        AddInGlobals.Extensions = list;

            //        break;
            //}

            if(this.Items.Count > 0)
                this.SelectedIndex = 0;

        }
        protected override void OnCreateControl()
        {
            if(!DesignTimeChecker.IsDesignMode)
                Fill();
            base.OnCreateControl();
        }

        public abstract class FillerBase
        {
            public class ExtensionAndFileTypes
            {
                public List<string> Extensions;
                public List<string> FileTypes;
            }

            public static FillerBase MakeFiller()
            {

                if (AddinModule.CurrentInstance.WordApp != null)
                    return new WordFiller();
                else if (AddinModule.CurrentInstance.ExcelApp != null)
                    return new ExcelFiller();
                else if (AddinModule.CurrentInstance.PowerPointApp != null)
                    return new PowerPointFiller();
                else
                    return new OutlookFiller();
            }

            public abstract ExtensionAndFileTypes Fill(ComboBox comboBox, bool saveAs);
        }

        private class WordFiller: FillerBase
        {

            public override ExtensionAndFileTypes Fill(ComboBox comboBox, bool saveAs)
            {
                if (comboBox != null)
                {
                    if (!saveAs)
                    {
                        comboBox.Items.Add("Alla Word dokument (*.docx, *.docm, *.dotx, *.dotm, *.doc, *.dot, *.rtf)");
                    }
                    comboBox.Items.Add("Word-dokument (*.docx)");
                    comboBox.Items.Add("Makroaktiverat Word-dokument (*.docm)");
                    comboBox.Items.Add("Word-mall (*.dotx)");
                    comboBox.Items.Add("Makroaktiverad Word-mall (*.dotm)");
                    comboBox.Items.Add("Word 97-2003-dokument (*.doc)");
                    comboBox.Items.Add("Word 97-2003-mall (*.dot)");
                    comboBox.Items.Add("PDF (*.pdf)"); 
                    comboBox.Items.Add("XPS-dokument (*.xps)");
                    comboBox.Items.Add("Enkel webbsidefil (*.mht, *.mhtml)");
                    comboBox.Items.Add("Web-sida (*.htm)");
                    comboBox.Items.Add("Web-sida, filtrerad (*.htm)");
                    comboBox.Items.Add("RTF (*.rtf)");
                    comboBox.Items.Add("Oformaterad text (*.txt)");
                    comboBox.Items.Add("Word XML-dokument (*.xml)");
                    comboBox.Items.Add("Word 2003 XML-dokument (*.xml)");
                    comboBox.Items.Add("OpenDocument-text (*.odt)");
                    comboBox.Items.Add("Works 6.0-9-0 (*.wps)");
                    comboBox.DropDownStyle = ComboBoxStyle.DropDownList;
                }

                List<string> extensions = new List<string>();
                List<string> internalFileTypes = new List<string>();

                extensions.Add("docx"); internalFileTypes.Add("docx");
                extensions.Add("docm"); internalFileTypes.Add("docm");
                extensions.Add("dotx"); internalFileTypes.Add("dotx");
                extensions.Add("dotm"); internalFileTypes.Add("dotm");
                extensions.Add("doc");  internalFileTypes.Add("doc");
                extensions.Add("dot");  internalFileTypes.Add("dot");
                extensions.Add("pdf");  internalFileTypes.Add("pdf");
                extensions.Add("xps");  internalFileTypes.Add("xps");
                extensions.Add("mht");  internalFileTypes.Add("mht");
                extensions.Add("htm");  internalFileTypes.Add("htm");
                extensions.Add("htm");  internalFileTypes.Add("htm2");
                extensions.Add("rtf");  internalFileTypes.Add("rtf");
                extensions.Add("txt");  internalFileTypes.Add("txt");
                extensions.Add("xml");  internalFileTypes.Add("xml");
                extensions.Add("xml");  internalFileTypes.Add("xml2");
                extensions.Add("odt");  internalFileTypes.Add("odt");
                extensions.Add("wps");  internalFileTypes.Add("wps");

                ExtensionAndFileTypes extensionsAndFileTypes = new ExtensionAndFileTypes();
                extensionsAndFileTypes.Extensions = extensions;
                extensionsAndFileTypes.FileTypes = internalFileTypes;

                return extensionsAndFileTypes;
            }
        }

        private class PowerPointFiller : FillerBase
        {
            public override ExtensionAndFileTypes Fill(ComboBox comboBox, bool saveAs)
            {
                if (comboBox != null)
                {
                    if (!saveAs)
                    {
                        comboBox.Items.Add("Alla PowerPoint-presentationer (*.pptx, *.ppt, *.pptm, *.ppsx, *.pps, *ppsm)");
                    }
                    comboBox.Items.Add("PowerPoint-presentation (*.pptx)");
                    comboBox.Items.Add("Makroaktiverad PowerPoint-presentation (*.pptm)");
                    comboBox.Items.Add("PowerPoint-97-2003-presentation (*.ppt)");
                    comboBox.Items.Add("PDF (*.pdf)");
                    comboBox.Items.Add("XPS-dokument (*.xps)");
                    comboBox.Items.Add("PowerPoint-mall (*.potx)");
                    comboBox.Items.Add("Makroaktiverad PowerPoint-mall (*.potm)");
                    comboBox.Items.Add("PowerPoint-97-2003-mall (*.pot)");
                    comboBox.Items.Add("Office-tema (*.thmx)");
                    comboBox.Items.Add("PowerPoint-Bildspel (*.ppsx)");
                    comboBox.Items.Add("Makroaktiverat PowerPoint-Bildspel (*.ppsm)");
                    comboBox.Items.Add("PowerPoint-97-2003-bildpsel (*.pps)");
                    comboBox.Items.Add("PowerPoint-tillägg (*.ppam)");
                    comboBox.Items.Add("PowerPoint 97-2003-tillägg (*.ppa)");
                    comboBox.Items.Add("PowerPoint XML-Presentation (*.xml)");
                    comboBox.Items.Add("Enkel webbsidefil (*.mht;*mhtml)");
                    comboBox.Items.Add("Webbsida (*.htm;*.html)");
                    comboBox.Items.Add("GIF Graphics Interchange Format (*.gif)");
                    comboBox.Items.Add("JPEG filutbytes Format (*.jpeg)");
                    comboBox.Items.Add("PNG Portable Network Graphics-format (*.png)");
                    comboBox.Items.Add("TIFF Tag Image File format (*.tif)");
                    comboBox.Items.Add("Oberoende bitmapp (*.bmp)");
                    comboBox.Items.Add("Windows metafiler (*.wmf)");
                    comboBox.Items.Add("Enhanced Windows Metafile (*.emf)");
                    comboBox.Items.Add("Disposition/RTF (*.rtf)");
                    comboBox.Items.Add("OpenDocument-presentation (*.odp)");
                }

                List<string> internalFileTypes = new List<string>();
                List<string> extensions = new List<string>();
                extensions.Add("pptx"); internalFileTypes.Add("pptx");
                extensions.Add("pptm"); internalFileTypes.Add("pptm");
                extensions.Add("ppt"); internalFileTypes.Add("ppt");
                extensions.Add("pdf"); internalFileTypes.Add("pdf");
                extensions.Add("xps"); internalFileTypes.Add("xps");
                extensions.Add("potx"); internalFileTypes.Add("potx");
                extensions.Add("potm"); internalFileTypes.Add("potm");
                extensions.Add("pot"); internalFileTypes.Add("pot");
                extensions.Add("thmx"); internalFileTypes.Add("thmx");
                extensions.Add("ppsx"); internalFileTypes.Add("ppsx");
                extensions.Add("ppsm"); internalFileTypes.Add("ppsm");
                extensions.Add("pps"); internalFileTypes.Add("pps");
                extensions.Add("ppam"); internalFileTypes.Add("ppam");
                extensions.Add("ppa"); internalFileTypes.Add("ppa");
                extensions.Add("xml"); internalFileTypes.Add("xml");
                extensions.Add("mht"); internalFileTypes.Add("mht");
                extensions.Add("htm"); internalFileTypes.Add("htm");
                extensions.Add("gif"); internalFileTypes.Add("gif");
                extensions.Add("jpg"); internalFileTypes.Add("jpg");
                extensions.Add("png"); internalFileTypes.Add("png");
                extensions.Add("tif"); internalFileTypes.Add("tif");
                extensions.Add("bmp"); internalFileTypes.Add("bmp");
                extensions.Add("wmf"); internalFileTypes.Add("wmf");
                extensions.Add("emf"); internalFileTypes.Add("emf");
                extensions.Add("rtf"); internalFileTypes.Add("rtf");
                extensions.Add("odp"); internalFileTypes.Add("odp");

                ExtensionAndFileTypes extensionsAndFileTypes = new ExtensionAndFileTypes();
                extensionsAndFileTypes.Extensions = extensions;
                extensionsAndFileTypes.FileTypes = internalFileTypes;

                return extensionsAndFileTypes;
            }
        }

        private class OutlookFiller : FillerBase
        {
            public override ExtensionAndFileTypes Fill(ComboBox comboBox, bool saveAs)
            {
                if (comboBox != null)
                {

                        comboBox.Items.Add("Alla dokument (*.*)");

                }

                List<string> list = new List<string>();
                list.Add("*");

                ExtensionAndFileTypes extensionsAndFileTypes = new ExtensionAndFileTypes();
                extensionsAndFileTypes.Extensions = list;

                return extensionsAndFileTypes;
            }
        }

        private class ExcelFiller : FillerBase
        {

            public override ExtensionAndFileTypes Fill(ComboBox comboBox, bool saveAs)
            {
                if(comboBox != null)
                {
                    if (!saveAs)
                    {
                        comboBox.Items.Add("Alla Excel dokument (*.xslx, *.xlsm, *.dotx, *.xlsb, *.xls, *.xml, *.xltx, *.xltm, *.xlt)");
                    }
                    comboBox.Items.Add("Excel arbetsbok (*.xlsx)");
                    comboBox.Items.Add("Makroaktiverad Excel arbetsbok (*.xlsm)");
                    comboBox.Items.Add("Binär Excel arbetsbok (*.xlsb)");
                    comboBox.Items.Add("Excel 97-2003-arbetsbok (*.xls)");
                    comboBox.Items.Add("Xml-data (*.xml)");
                    comboBox.Items.Add("Excel-mall (*.xltx)");
                    comboBox.Items.Add("Makroaktiverad Excel-mall (*.xltm)");
                    comboBox.Items.Add("Excel 97-2003-mall (*.xlt)");
                    comboBox.Items.Add("Text (tabbavgränsad) (*.txt)");
                    comboBox.Items.Add("Unicode-text (*.txt)");
                    comboBox.Items.Add("Xml kalkylblad-2003 (*.xml)");
                    comboBox.Items.Add("Microsoft Excel(5.0/95) arbetsbok (*.xls)");
                    comboBox.Items.Add("CSV (kommaavgränsad) (*.csv)");
                    comboBox.Items.Add("Blankstegsformaterad text (*.prn)");
                    comboBox.Items.Add("Text (Machintosh)(*.txt)");
                    comboBox.Items.Add("Text (MS-DOS)(*.txt)");
                    comboBox.Items.Add("CSV (Machintosh)(*.csv)");
                    comboBox.Items.Add("CSV (MS-DOS)(*.csv)");
                    comboBox.Items.Add("PDF (*.pdf)");
                    comboBox.Items.Add("XPS-dokument *.xps)");
                    comboBox.Items.Add("OpenDocument-kalkylblad (*.ods)");
                }

                List<string> internalFileTypes = new List<string>();
                List<string> extensions = new List<string>();

                internalFileTypes.Add("xlsx"); extensions.Add("xlsx");
                internalFileTypes.Add("xlsm"); extensions.Add("xlsm");
                internalFileTypes.Add("xlsb"); extensions.Add("xlsb");
                internalFileTypes.Add("xls");  extensions.Add("xls");
                internalFileTypes.Add("xml");  extensions.Add("xml");
                internalFileTypes.Add("xltx"); extensions.Add("xltx");
                internalFileTypes.Add("xltm"); extensions.Add("xltm");
                internalFileTypes.Add("xlt");  extensions.Add("xlt");
                internalFileTypes.Add("txt");  extensions.Add("txt");
                internalFileTypes.Add("txt2"); extensions.Add("txt");
                internalFileTypes.Add("xml2"); extensions.Add("xml");
                internalFileTypes.Add("xls2"); extensions.Add("xls");
                internalFileTypes.Add("csv");  extensions.Add("csv");
                internalFileTypes.Add("prn");  extensions.Add("prn");
                internalFileTypes.Add("txt2"); extensions.Add("txt");
                internalFileTypes.Add("txt3"); extensions.Add("txt");
                internalFileTypes.Add("csv2"); extensions.Add("csv");
                internalFileTypes.Add("csv3"); extensions.Add("csv");
                internalFileTypes.Add("pdf");  extensions.Add("pdf");
                internalFileTypes.Add("xps");  extensions.Add("xps");
                internalFileTypes.Add("ops");  extensions.Add("ods");

                ExtensionAndFileTypes extensionsAndFileTypes = new ExtensionAndFileTypes();
                extensionsAndFileTypes.Extensions = extensions;
                extensionsAndFileTypes.FileTypes = internalFileTypes;

                return extensionsAndFileTypes;
            }
        }

    }


}
