﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sharepoint_Doc.Controls;

namespace EmpirConnector.Controls
{
    public class NextPageButton: Button
    {
        private FileListView _fileListView;

        public void Init(FileListView fileListView)
        {
            _fileListView = fileListView;

            _fileListView.PageDisplayed += new FileListView.PageDisplayedHandler(_fileListView_PageDisplayed);
            this.Click += new EventHandler(NextPageButton_Click);
        }

        void NextPageButton_Click(object sender, EventArgs e)
        {
            _fileListView.Page++;
            _fileListView.ListFilesInFolder();
        }

        void _fileListView_PageDisplayed(int page)
        {
            this.Visible = (page < _fileListView.Pages);
        }




    }
}
