﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Sharepoint_Doc.Controls;

namespace EmpirConnector.Controls
{
    public class PreviousPageButton: Button
    {
        private FileListView _fileListView;

        public void Init(FileListView fileListView)
        {
            _fileListView = fileListView;

            _fileListView.PageDisplayed += new FileListView.PageDisplayedHandler(_fileListView_PageDisplayed);
            this.Click += new EventHandler(PrevPageButton_Click);
        }

        void PrevPageButton_Click(object sender, EventArgs e)
        {
            _fileListView.Page--;
            _fileListView.ListFilesInFolder();
        }

        void _fileListView_PageDisplayed(int page)
        {
            this.Visible = (page>1);
        }
    }
}
