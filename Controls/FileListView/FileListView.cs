﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;
using EmpirConnector;


namespace Sharepoint_Doc.Controls
{
    public class FileListView: ListView
    {

        private delegate void CountItemsDelegate();
        private delegate void PresentCountDelegate(int count);
        
        public delegate void FileSelectedHandler(string filename);
        public event FileSelectedHandler FileSelected;

        public delegate void OpenFileHandler();
        public event OpenFileHandler OpenFile;

        private FileTypeComboBox _fileTypeComboBox;

        private CamlQuery _cq;
        private Entities.Folder _folder;


        public delegate void PageDisplayedHandler(int page);
        public event PageDisplayedHandler PageDisplayed;

        public int PAGE_SIZE = 10;

        public bool UnlimitedListSize
        {
            set;
            get;
        }

        public int Page
        {
            set;
            get;
        }

        public int NumberOfDocuments
        {
            set;
            get;
        }

        public int Pages
        {
            set;
            get;
        }

        public string FiletypeFilter
        {
            set;
            get;
        }

        public bool TemplateList
        {
            set;
            get;
        }

        public string SelectedFileUrl
        {
            get
            {
            
                return ((Entities.File)this.SelectedItems[0].Tag).Url;
            }
        }
           

        public void Init(FileTypeComboBox fileTypeComboBox)
        {
            PAGE_SIZE = Sharepoint_Doc.Entities.AddInGlobals.ConnectorSettings.PageCount;

            this.View = System.Windows.Forms.View.Details;

            this.SelectedIndexChanged += new EventHandler(FileListView_SelectedIndexChanged);
            this.DoubleClick += new EventHandler(FileListView_DoubleClick);

            _fileTypeComboBox = fileTypeComboBox;           
            this.ColumnClick += new ColumnClickEventHandler(FileListView_ColumnClick);

            this.HideSelection = false;
        }

        public void FilterByFiletype(string extension)
        {

        }

        void FileListView_ColumnClick(object sender, ColumnClickEventArgs e)
        {
            if (this.Columns[e.Column].Tag == null)
                this.Columns[e.Column].Tag = true;

            this.Columns[e.Column].Tag = !bool.Parse(this.Columns[e.Column].Tag.ToString());

            if (this._folder == null)
                return;

            this._folder.Files.Sort(this.Columns[e.Column].Text, bool.Parse(this.Columns[e.Column].Tag.ToString()));

            this.Page = 1;
            this.ListFilesInFolder();
        }

        void FileListView_DoubleClick(object sender, EventArgs e)
        {
            if (this.SelectedItems.Count > 0)
                if(OpenFile!=null)
                    OpenFile();
        }

        void FileListView_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (FileSelected != null && this.SelectedItems.Count > 0)
                if(FileSelected != null)
                    FileSelected(this.SelectedItems[0].Text);
            else
                if (FileSelected != null)
                    FileSelected("");
        }

        void _scrollBar_Scroll(object sender, ScrollEventArgs e)
        {
            if(e.NewValue!=e.OldValue)
                DisplayPage(e.NewValue, "");
        }

        public void Load(Folder folder)
        {
            

        }

        public void ClearList()
        {
            this.Items.Clear();
            FileSelected("");
        }

        public void ListFilesInFolder()
        {
            if (this._folder == null)
                return;

            ListFilesInFolder(this._folder);
        }

        public void ListFilesInFolder(Entities.Folder folder)
        {
            _folder = folder;

            this.Items.Clear();


            Entities.File[] files = folder.Files.ToArray();

            if (this.FiletypeFilter != null && this.FiletypeFilter != "" && this.FiletypeFilter != "*")
            {
                files = files.Where(s => s.Name.ToLower().EndsWith(this.FiletypeFilter)).ToArray();
            }
            else if (this.FiletypeFilter == "*")
            {
                if(AddinModule.CurrentInstance.ExcelApp != null)
                    files = files.Where(s => s.Name.ToLower().Contains(".xl")).ToArray();
                else if (AddinModule.CurrentInstance.WordApp != null)
                    files = files.Where(s => s.Name.ToLower().Contains(".do")).ToArray();
                else if (AddinModule.CurrentInstance.PowerPointApp != null)
                    files = files.Where(s => s.Name.ToLower().Contains(".pp")).ToArray();
            }
            NumberOfDocuments = files.Count();
            if (UnlimitedListSize)
                PAGE_SIZE = int.MaxValue;

            Pages = (int)Math.Ceiling((double)this.NumberOfDocuments / this.PAGE_SIZE);
    

            if (files.Count() == 0)
            {
                if (PageDisplayed != null)
                {
                    PageDisplayed(Page);
                }
                return;
            }

  
            string imageKey = GetImageKey();

            int stop = ((Page - 1) * PAGE_SIZE) + PAGE_SIZE;
            for (int index = ((Page-1) * PAGE_SIZE); index < stop; index++)
            {
                if (index >= files.Length)
                    break;

                Entities.File file = files[index];
                ListViewItem listViewItem = this.Items.Add(file.Name);
                if(this.Columns.Count>2)
                    listViewItem.SubItems.Add(file.Author);
                listViewItem.SubItems.Add(file.Modified.ToString("yyyy-MM-dd HH:mm"));
                listViewItem.Tag = file;

                listViewItem.ImageKey = GetImageKey(file.Name);
            }

            if (this.Columns.Count == 2)
            {
                this.Columns[0].Width = 145;
                this.Columns[1].Width = 110;
            }

            if (PageDisplayed != null)
            {
                
                PageDisplayed(Page);
            }

            this.AutoResizeColumns(ColumnHeaderAutoResizeStyle.ColumnContent);


        }

        private void ListFolders()
        {
            CItem iView = null;

            using (ClientContext context = new ClientContext(iView.RootSite + iView.SubSite))
            {
                List list = context.Web.Lists.GetById(new Guid(iView.ListId));

                CamlQuery query = new CamlQuery();

                query.ViewXml = @"<queryOptions><QueryOptions><ViewAttributes Scope='RecursiveAll'/><Eq><FieldRef Name='ContentType' /><Value Type='Text'>Folder</Value></Eq></QueryOptions></queryOptions>";

                ListItemCollection items = list.GetItems(query);

                context.Load(items);

                context.ExecuteQuery();

                int i = 0;
                foreach (ListItem item in items)
                {
                    if (item.FileSystemObjectType == FileSystemObjectType.Folder)
                    {
                        CItem iDocument = new CItem();
                        iDocument.RootSite = iView.RootSite;
                        iDocument.SubSite = iView.SubSite;
                        iDocument.ListId = iView.ListId;
                        iDocument.WebId = iView.WebId;
                        iDocument.ViewId = iView.ViewId;
                        iDocument.DocumentId = item.Id;
                        iDocument.ItemType = CItem.eItemType.Folder;


                        if (item["FileLeafRef"] != null && !string.IsNullOrEmpty(item["FileLeafRef"].ToString()))
                        {
                            iDocument.Title = item["FileLeafRef"].ToString();
                        }
                        else
                        {
                            iDocument.Title = "{Can not get folder name!}";
                        }


                        ListViewItem listViewItem = this.Items.Add(iDocument.Title);

                        listViewItem.Tag = iDocument;
                    }
                    else
                        break;
                }
            }




        }

        private string GetImageKey(string filename)
        {
            string extension = "";
            if(System.IO.Path.GetExtension(filename).ToLower() == ".pdf")
                extension = "AdobePDF.png";
            else
                extension = "ic" + System.IO.Path.GetExtension(filename).Substring(1) + ".gif";

            return extension;
        }

        private string GetImageKey()
        {
            if (AddinModule.CurrentInstance.WordApp != null)
                return "word-icon";
            else if (AddinModule.CurrentInstance.ExcelApp != null)
                return "excel-icon";
            else if (AddinModule.CurrentInstance.PowerPointApp != null)
                return "powerpoint-icon";
            else
                return "file-icon";
        }
   
        private void DisplayPage(int page, string query)
        {
            this.Items.Clear();

            if (page == 1)
                ListFolders();

            CItem iView = null;

            //string SelectedSite = ctx.Url;
            using (ClientContext context = new ClientContext(iView.RootSite + iView.SubSite))
            {
                // get list
                List list = context.Web.Lists.GetById(new Guid(iView.ListId));
                Microsoft.SharePoint.Client.View view = context.Web.Lists.GetById(new Guid(iView.ListId)).GetView(new Guid(iView.ViewId));

                if(_cq == null)
                    _cq = CreateQuery(page, query);

                ListItemCollection items  = list.GetItems(_cq);
                context.Load(items);

                context.ExecuteQuery();

                foreach (ListItem item in items)
                {
                  
                    CItem iDocument = new CItem();
                    iDocument.RootSite = iView.RootSite;
                    iDocument.SubSite = iView.SubSite;
                    iDocument.ListId = iView.ListId;
                    iDocument.WebId = iView.WebId;
                    iDocument.ViewId = iView.ViewId;
                    iDocument.DocumentId = item.Id;
                    iDocument.ItemType = CItem.eItemType.Document;
                    iDocument.Url = iView.RootSite + iView.SubSite +  "/" + iView.ListName + "/1/" + item["FileLeafRef"].ToString();

                    string s = item["Last_x0020_Modified"].ToString();

                    if (item["FileLeafRef"] != null && !string.IsNullOrEmpty(item["FileLeafRef"].ToString()))
                    {
                        iDocument.Title = item["FileLeafRef"].ToString();
                    }
                    else
                    {
                        iDocument.Title = "{Can not get document name!}";
                    }


                    ListViewItem listViewItem = this.Items.Add(iDocument.Title);

                    
                    listViewItem.SubItems.Add(((FieldUserValue)item["Author"]).LookupValue);
                    listViewItem.SubItems.Add(item["Modified"].ToString());
                    listViewItem.Tag = iDocument;
                }

                _cq.ListItemCollectionPosition = items.ListItemCollectionPosition;
            }
        }

        public string GetFileTypeFilter()
        {
            string hsx = "";

            hsx += "<Or>";

            int i = 0;

            foreach (string fileType in _fileTypeComboBox.Extensions[_fileTypeComboBox.SelectedIndex])
            {

                if (i>= 2)
                {
                    hsx += "</Or>";
                    hsx = "<Or>" + hsx;
                }
                hsx += "<Eq>";
                hsx += "<FieldRef Name=\"File_x0020_Type\"/>";
                hsx += "<Value Type=\"Text\">" + fileType + "</Value>";
                hsx += "</Eq>";
                i++;

            }

            hsx += "</Or>";

            return hsx;
        }

        public CamlQuery CreateQuery(int page, string query)
        {
            CamlQuery cq = new CamlQuery();

            string filter = GetFileTypeFilter();

            //if (query.Contains("Where"))
            //{
            //    string where = 
            //    query.Replace("<Where>", "
            //}
            //else
            //{


            cq.ViewXml = "<View Scope='Recursive'><Query><Where>" + filter + "</Where>" + query + "</Query></View>";
            return cq;

  

            //ListItemCollectionPosition position = new ListItemCollectionPosition();
            //position.PagingInfo = page.ToString();

            //cq.ListItemCollectionPosition = position;

            //return cq;

        }

        public void GetItemListCount()
        {
            CItem iView = null;

            //string SelectedSite = ctx.Url;
             using (ClientContext context = new ClientContext(iView.RootSite + iView.SubSite))
             {
                 // get list
                 List list = context.Web.Lists.GetById(new Guid(iView.ListId));
                 Microsoft.SharePoint.Client.View view = context.Web.Lists.GetById(new Guid(iView.ListId)).GetView(new Guid(iView.ViewId));

                 context.Load(list);
                 context.Load(view);
                 context.ExecuteQuery();

                 CamlQuery cq = new CamlQuery();

                 string hsx = "<View Name=\"{7F735059-58A1-427A-AE66-7494F6B92E5C}\">";
                 hsx += "<ViewFields>";
                 hsx += "<FieldRef Name=\"File_x0020_Type\" />";
                 hsx += "</ViewFields>";
                 hsx += "</View>";

                 cq.ViewXml = hsx;

                 ListItemCollection items = context.Web.Lists.GetById(new Guid(iView.ListId)).GetItems(cq);

                 context.Load(items);
                 context.ExecuteQuery();

                 this.Invoke(new PresentCountDelegate(PresentCount), items.Count);
             }

        }

        private void PresentCount(int count)
        {


        }

        private void _tvViews_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //string query = ((CItem)e.Node.Tag).Query;
            //Load( query);
        }
 
    }

    public class CItem
    {
        public enum eItemType
        {
            Web,
            Document,
            Folder
        }

        public string WebId { get; set; }
        public string ListId { get; set; }
        public string ListName { get; set; }
        public string ViewId { get; set; }
        public int DocumentId { get; set; }
        public string RootSite { get; set; }
        public string SubSite { get; set; }
        public string Title { get; set; }
        public string Query { set; get; }
        public string Url { set; get; }
        public eItemType ItemType { get; set; }
        
        public CItem()
        {
        }

        public override string ToString()
        {
            return Title;
        }
    }
}
