﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;
using System.Net;
using Sharepoint_Doc.Entities;
using EmpirConnector.Forms;
using EmpirConnector.Entities;
using EmpirConnector.Controls.FolderTreeView;
using System.Threading.Tasks;
using System.Threading;

namespace Sharepoint_Doc.Controls
{
    public class LibraryFolderTreeView : LibraryFolderTreeViewBase
    {
        private enum EditMode
        {
            New,
            Edit,
        }

        EditMode _editMode;

        FileListView _fileListView;
        DocumentLibrary _documentLibrary;

        ContextMenuStrip _mnu;
        System.Windows.Forms.Timer _timerNewNode;
        TreeNode _tnNewNode;

        public delegate void BeforeDisplayHandler();
        public delegate void AfterDisplayHandler();
        public delegate void ReDisplayFoldersHandler();
        public delegate void ListRemovedHandler();
        public delegate bool HasPermissionHandler();
        public delegate void ListClearedHandler();

        public event BeforeDisplayHandler BeforeDisplay;
        public event AfterDisplayHandler AfterDisplay;
        public event ReDisplayFoldersHandler ReDisplayFolders;
        public event ListClearedHandler ListCleared;
        public event ListRemovedHandler ListRemoved;
        public event HasPermissionHandler HasPermission;

        System.Windows.Forms.Timer _redisplayFoldersTimer;
        string _reloadedFullpath = "";
        string _oldFolderName = "";
        string _startName = "";
        bool _escape = false;

        public bool DefaultView
        {
            get;
            set;
        }

        public string RootNodeName
        {
            set;
            get;
        }



        public LibraryFolderTreeView()
        {

            this.FullRowSelect = true;
        }

        public void Init(FileListView fileListView,  ContextMenuStrip mnu)
        {

            _mnu = mnu;
            _fileListView = fileListView;

            this.NodeMouseClick += new TreeNodeMouseClickEventHandler(LibraryFolderTreeView_NodeMouseClick);
            this.AfterSelect += new TreeViewEventHandler(LibraryFolderTreeView_AfterSelect);
            this.BeforeLabelEdit +=new NodeLabelEditEventHandler(LibraryFolderTreeView_BeforeLabelEdit);
            this.AfterLabelEdit += new NodeLabelEditEventHandler(LibraryFolderTreeView_AfterLabelEdit);

            this.ValidateLabelEdit += new ValidateLabelEditEventHandler(LibraryFolderTreeView_ValidateLabelEdit);
           
            if (mnu != null)
            {
                _mnu.Opening += new System.ComponentModel.CancelEventHandler(_mnu_Opening);
                mnu.Items[0].Click += new EventHandler(_mnu_Click);
                mnu.Items[1].Click += new EventHandler(_mnu_Click);
                mnu.Items[2].Click += new EventHandler(_mnu_Click);
            }
            else
                LabelEdit = false;
       
        }

        void LibraryFolderTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {

            Cursor = Cursors.WaitCursor;

            try
            {

                bool hasPermissions = false;
                if (HasPermission != null)
                    hasPermissions = HasPermission();
                else
                    hasPermissions = true;

                if (!hasPermissions)
                {
                    ClearList();
                    MessageBox.Show("Dina rättigheter för denna lista har tagits bort.", Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
                else
                {
                    bool enableEdit = _documentLibrary.EnableFolderCreation;
                    _oldFolderName = this.SelectedNode.Text;

                    this.ContextMenuStrip = _mnu;

                    if (!enableEdit)
                        this.ContextMenuStrip.Items[0].Enabled = false;

                    if (e.Node.Tag != null)
                        ListFiles(e.Node);
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                Cursor = Cursors.Default;
            }

        }



        void LibraryFolderTreeView_ValidateLabelEdit(object sender, ValidateLabelEditEventArgs e)
        {
            if (e.Label == null)
                return;
            if (e.Label.Trim() == "") //e.Label == null || 
            {
                MessageBox.Show("Du måste ange ett namn",
                     Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Cancel = true;
                return;
            }
            if (e.Label.IndexOfAny(new char[] { '\\', '/', ':', '*', '?', '"', '<', '>', '|' }) != -1)
            {
                MessageBox.Show("Felaktigt namn.\n" +
                    "Mappens namn får inte innehålla följande bokstäver:\n \\ / : * ? \" < > |",
                     Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Cancel = true;
                return;
            }

            foreach(TreeNode tn in this.SelectedNode.Parent.Nodes)
            {
                if (tn.Text.ToLower() == e.Label.ToLower() && tn != this.SelectedNode)
                {
                    MessageBox.Show("Det finns redan en mapp med detta namn.",
                         Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    e.Cancel = true;
                    return;
                }
            }



        }



        void _mnu_Opening(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if(this.Nodes.Count > 0) //addedddddddddddddddddd by me
            { 
            _mnu.Items[1].Enabled = !(this.SelectedNode.Index == 0 && this.SelectedNode.Level == 0);
            _mnu.Items[2].Enabled = !(this.SelectedNode.Index == 0 && this.SelectedNode.Level == 0);

            }
        }

        protected override void OnDrawNode(DrawTreeNodeEventArgs e)
        {
            TreeNodeStates state = e.State;
            Font font = e.Node.NodeFont ?? e.Node.TreeView.Font;
            Color fore = e.Node.ForeColor;
            if (fore == Color.Empty) fore = e.Node.TreeView.ForeColor;
            if (e.Node == e.Node.TreeView.SelectedNode)
            {
                fore = SystemColors.HighlightText;
                e.Graphics.FillRectangle(SystemBrushes.Highlight, e.Bounds);
                ControlPaint.DrawFocusRectangle(e.Graphics, e.Bounds, fore, SystemColors.Highlight);
                TextRenderer.DrawText(e.Graphics, e.Node.Text, font, e.Bounds, fore, TextFormatFlags.GlyphOverhangPadding);
            }
            else
            {
                e.Graphics.FillRectangle(SystemBrushes.Window, e.Bounds);
                TextRenderer.DrawText(e.Graphics, e.Node.Text, font, e.Bounds, fore, TextFormatFlags.GlyphOverhangPadding);
            }
        }


        void LibraryFolderTreeView_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {

            try
            {
                string name = e.Label;
                if (e.Label == null)
                {
                    name = _startName;
                }

                if (_editMode == EditMode.New)
                {
                    EditFolder("", name, true, e.Node);
                    this.SelectedNode.Text = name;
                    LibraryFolderTreeView_NodeMouseClick(this, new TreeNodeMouseClickEventArgs(e.Node, System.Windows.Forms.MouseButtons.Left, 0, 0, 0));
                    LibraryFolderTreeView_AfterSelect(this, new TreeViewEventArgs(this.SelectedNode));
                    _fileListView.ListFilesInFolder();
                }
                else
                {
                    string folderName = _oldFolderName;
                    string serverRelativeUrl = ((Folder)this.SelectedNode.Tag).ServerRelativeUrl;

                    DAL.ListDAL dal = new DAL.ListDAL();
                    dal.RenameFolder(_documentLibrary.Url, serverRelativeUrl, name, folderName, _documentLibrary.Title);

                    Folder f = (Folder)this.SelectedNode.Tag;
                    f.Name = e.Label;
                    f.ServerRelativeUrl = f.ServerRelativeUrl.Replace(folderName, name);

                    this.SelectedNode.Text = name;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        void LibraryFolderTreeView_BeforeLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            if (NoEdit)
            {
                e.CancelEdit = true;
                return;
            }
            if (this.LabelEdit)
            {
                _editMode = EditMode.Edit;
                _startName = e.Node.Text;
            }
            
        }

        void LibraryFolderTreeView_Click(object sender, EventArgs e)
        {

        }

        void EditFolder(string oldName, string name, bool isNew, TreeNode tn)
        {

            try
            {
                string serverRelativeUrl = "";

                if (isNew)
                {
                    serverRelativeUrl = ((Folder)this.SelectedNode.Parent.Tag).ServerRelativeUrl;

                    DAL.ListDAL dal = new DAL.ListDAL();
                    Sharepoint_Doc.DAL.ListDAL.SharePointFolder sharePointFolder = dal.AddFolder(_documentLibrary.Url, serverRelativeUrl, name, this._documentLibrary.ListId);

                    Folder folder = Folder.GetFolder(sharePointFolder, _documentLibrary.ListId, _documentLibrary.Url, _documentLibrary.WebApplication);
                    tn.Tag = folder;
                    ((Folder)tn.Parent.Tag).SubFolders.Add(folder);
                }
                else
                {
                    DAL.ListDAL dal = new DAL.ListDAL();
                    dal.RenameFolder(_documentLibrary.Url, serverRelativeUrl, name, oldName, _documentLibrary.Title);

                    Folder f = (Folder)tn.Tag;
                    f.Name = name;
                    f.ServerRelativeUrl = f.ServerRelativeUrl.Replace(oldName, name);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        void _redisplayFoldersTimer_Tick(object sender, EventArgs e)
        {
            _redisplayFoldersTimer.Stop();

           

            ReDisplayFolders();

            

        }



        public void DisplayFolders(DocumentLibrary documentLibrary)
        {
            try
            {
                _documentLibrary = documentLibrary;

                if (BeforeDisplay != null)
                    BeforeDisplay();

                this.Nodes.Clear();

                ManualResetEvent waitHandle = new ManualResetEvent(false);

                Files prevLoadFiles = null;
                if (documentLibrary.Folders == null)
                {
                    Task.Factory.StartNew(() =>
                    {
                        Folder tempFolder = new Folder();
                        tempFolder.ServerRelativeUrl = new DAL.ListDAL().GetRootFolderServerRelativeUrl(documentLibrary.ListId, documentLibrary.Url);
                        tempFolder.WebApplication = documentLibrary.WebApplication;
                        tempFolder.ListId = documentLibrary.ListId;
                        tempFolder.ParentWebUrl = documentLibrary.Url;

                        prevLoadFiles = tempFolder.Files;
                        waitHandle.Set();
                    });
                }
                else
                    waitHandle.Set();


                if (DefaultView)
                    AddFolders(null, documentLibrary.TemplateFolders);
                else
                {
                    if (documentLibrary.Folders == null)
                        documentLibrary.ReloadFolders();
                    AddFolders(null, documentLibrary.Folders);
                }

                waitHandle.WaitOne();
                if(prevLoadFiles != null)
                    documentLibrary.Folders[0].Files = prevLoadFiles;

                this.Nodes[0].Expand();

                this.SelectedNode = this.Nodes[0];
     

                if (AfterDisplay != null)
                    AfterDisplay();

                this.LabelEdit = documentLibrary.EnableFolderCreation && _mnu != null & !NoEdit;
            }
            catch (Csla.DataPortalException ex)
            {
                if (ex.BusinessException.Message.Contains("Listan finns inte."))
                {
                    MessageBox.Show("Listan har tagits bort eller bytt namn.", Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void AddFolders(TreeNode tn, Folders folders)
        {
            try
            {
                object sortedFolders = null;
                if (DefaultView)
                    sortedFolders = folders;
                else
                    sortedFolders = folders.OrderBy(s => s.Name);

                foreach (Folder folder in folders) //.OrderBy(s => s.Name)
                {
                    TreeNode tnFolder = null;

                    if (tn == null)
                    {
                        tnFolder = this.Nodes.Add(RootNodeName);
                    }
                    else
                        tnFolder = tn.Nodes.Add(folder.Name);

                    tnFolder.ImageKey = "folder";
                    tnFolder.SelectedImageKey = "folder";
                    tnFolder.Tag = folder;

                    AddFolders(tnFolder, folder.SubFolders);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ListFiles(TreeNode tn)
        {

            //((Folder)tn.Tag).Files.Count 
          
                _fileListView.Page = 1;

                Folder folder = (Folder)tn.Tag;

                _fileListView.ListFilesInFolder(folder);
                _fileListView.Pages =  (int)Math.Ceiling((double)_fileListView.NumberOfDocuments / _fileListView.PAGE_SIZE);
         
        }

        public void ClearList()
        {
            this.Nodes.Clear();
            _fileListView.ClearList();

            if (ListCleared != null)
                ListCleared();

            //_fileListView.Items.Clear();
        }

        void LibraryFolderTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            LabelEdit = this.SelectedNode.Parent != null && !NoEdit  ;
            

        }

        void _mnu_Click(object sender, EventArgs e)
        {

            try
            {
                string name = ((ToolStripDropDownItem)sender).Name;


                if (name == "nyttToolStripMenuItem")
                {

                    string folderName = GetUniqueFolderName();

                    this.LabelEdit = true;
                    TreeNode tn = this.SelectedNode.Nodes.Add(folderName);
                    tn.ImageKey = "folder";
                    tn.SelectedImageKey = "folder";
                    this.SelectedNode.Expand();

                    _startName = folderName;
                    _tnNewNode = tn;
                    _timerNewNode = new System.Windows.Forms.Timer();
                    _timerNewNode.Interval = 50;
                    _timerNewNode.Tick += new EventHandler(_timerNewNode_Tick);
                    _timerNewNode.Start();

                   
                    return;

                    



                }
                else if (name == "bytNamnToolStripMenuItem")
                {
                    
                    this.LabelEdit = true;
                    TreeNode tn = this.SelectedNode;
                    _startName = tn.Text;
                    tn.BeginEdit();

                    _editMode = EditMode.Edit;

                }
                else
                {
                    if (MessageBox.Show(string.Format("Ta bort '{0}'?", this.SelectedNode.Text), "Stadsportalen", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        string serverRelativeUrl = ((Folder)this.SelectedNode.Tag).ServerRelativeUrl;

                        DAL.ListDAL dal = new DAL.ListDAL();
                        dal.DeleteFolder(_documentLibrary.Url, serverRelativeUrl, this.SelectedNode.Text, _documentLibrary.Title);

                        Folder parentFolder = (Folder)this.SelectedNode.Parent.Tag;
                        parentFolder.SubFolders.Remove((Folder)this.SelectedNode.Tag);

                        this.SelectedNode.Remove();

                        this.SelectedNode = this.Nodes[0];

                        LibraryFolderTreeView_NodeMouseClick(this, new TreeNodeMouseClickEventArgs(this.Nodes[0], System.Windows.Forms.MouseButtons.Left, 0, 0, 0));

                    }
                }

       
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
        }

        void _timerNewNode_Tick(object sender, EventArgs e)
        {
            _timerNewNode.Stop();
            this.LabelEdit = true;
            this.SelectedNode = _tnNewNode;
            _tnNewNode.BeginEdit();

            _editMode = EditMode.New;
            
        }

        bool frm_CheckEditFolderName(string foldername)
        {
            foreach (TreeNode tn in this.SelectedNode.Parent.Nodes)
            {
                if (foldername.ToLower() == tn.Text.ToLower())
                    return false;
            }

            return true;
        }

        bool frm_CheckNewFolderName(string foldername)
        {
            foreach (TreeNode tn in this.SelectedNode.Nodes)
            {
                if (foldername.ToLower() == tn.Text.ToLower())
                    return false;
            }

            return true;
        }

        private string GetUniqueFolderName()
        {
            bool unique = true;
            string checkName = "";

            int index = 0;

            checkName = "Ny mapp";

            do
            {
                unique = true;
                index++;

                foreach (TreeNode tnExistsing in this.SelectedNode.Nodes)
                {
                    checkName = "Ny mapp";
                    if (index > 1)
                        checkName += " " + index.ToString();

                    if (tnExistsing.Text.ToLower() == checkName.ToLower())
                        unique = false;
                }
            }
            while (!unique);

            return checkName;
        }
    }


}
