﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SharePoint.Client;
using System.Drawing;
using System.Net;
using Sharepoint_Doc.Entities;
using System.Threading.Tasks;
using Sharepoint_Doc.DAL;
using Empir.OfficeConnector.Entities;
using EmpirConnector.Entities;
using EmpirConnector.Forms;
using EmpirConnector;

namespace Sharepoint_Doc.Controls
{
    public class FolderTreeView: TreeView
    {


    
        private delegate void SubSiteHandler(TreeNode tn, Grouping grouping);
        private delegate void SetDefaultCursorHandler();
        private delegate void AddTreeNodeHandler(TreeNode tn);
        private delegate void AddTreeNodeHandler2(TreeNode tnParent, TreeNode tn);

        private LibraryFolderTreeView _libraryFolderTreeView;
        private ContextMenuStrip _mnuStrip;
 

        public enum ePermissionCheck
        {
            Write,
            View
        }
        public ePermissionCheck PermissionCheck = ePermissionCheck.View;

        public PreloadCommand PreloadCommand
        {
            set;
            get;
        }

        public string SelectedUrl
        {
            get
            {
                TreeNode node = this.SelectedNode;
                DocumentLibrary iList = (DocumentLibrary)node.Tag;

                return  iList.Url;
            }
        }

        public string SelectedWebUrl
        {
            get
            {
                TreeNode node = this.SelectedNode.Parent;
                Sharepoint_Doc.Entities.Web web = (Sharepoint_Doc.Entities.Web)node.Tag;

                return  web.ServerRelativeUrl;
            }
        }

        public void Init(LibraryFolderTreeView libraryFolderTreeView, ContextMenuStrip mnuStrip)
        {

            _libraryFolderTreeView = libraryFolderTreeView;
            _libraryFolderTreeView.HasPermission += new LibraryFolderTreeView.HasPermissionHandler(_libraryFolderTreeView_HasPermission);
            _mnuStrip = mnuStrip;

        }

        bool _libraryFolderTreeView_HasPermission()
        {
            DocumentLibrary library = (DocumentLibrary)this.SelectedNode.Tag;
            return CheckPermissions(library, ((Sharepoint_Doc.Entities.Web)this.SelectedNode.Parent.Tag).Url);

        }



        void _libraryFolderTreeView_ReDisplayFolders()
        {
            DocumentLibrary documentLibrary = (DocumentLibrary)this.SelectedNode.Tag;
            documentLibrary.ReloadFolders();
        }


        public void Load()
        {
            try
            {
                this.Nodes.Clear();

                GetAllSubSites(null);

                this.BeforeExpand += new TreeViewCancelEventHandler(FolderTreeView_BeforeExpand);
                this.NodeMouseClick += new TreeNodeMouseClickEventHandler(FolderTreeView_NodeMouseClick);
                this.AfterSelect += new TreeViewEventHandler(FolderTreeView_AfterSelect);
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void FolderTreeView_AfterSelect(object sender, TreeViewEventArgs e)
        {
            try
            {

                Cursor = Cursors.WaitCursor;
                _libraryFolderTreeView.ClearList();
               
                Application.DoEvents();
                GetParentForm(this.Parent).SuspendLayout();

                if (e.Node.Tag is DocumentLibrary)
                {

                    DocumentLibrary library = (DocumentLibrary)e.Node.Tag;


                    bool hasPermissions = true; // CheckPermissions(library, ((Sharepoint_Doc.Entities.Web)e.Node.Parent.Tag).Url);

                    if (hasPermissions)
                    {
                        _libraryFolderTreeView.DisplayFolders(library);
                        this._mnuStrip.Items[0].Enabled = library.EnableFolderCreation;
                    }
                    else
                    {
                        MessageBox.Show("Dina rättigheter för denna lista har tagits bort.", Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        this._libraryFolderTreeView.ClearList();
                        this.Nodes.Remove(e.Node);

                    }

                    this._mnuStrip.Enabled = library.EnableFolderCreation;
                }
                //else
                //{
                //    _libraryFolderTreeView.ClearList();
                //}
            }
            catch (Exception ex)
            {
                MessageBox.Show(Constants.ERROR_IN_APPLICTION_MESSAGE + Environment.NewLine + ex.Message, Constants.MESSAGEBOX_CAPTION, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            finally
            {
                GetParentForm(this).ResumeLayout();
                Cursor = Cursors.Default;
            }
        }

        private System.Windows.Forms.Form GetParentForm(Control parent)
        {
            System.Windows.Forms.Form form = parent as System.Windows.Forms.Form;
            if (form != null)
            {
                return form;
            }
            if (parent != null)
            {
                // Walk up the control hierarchy
                return GetParentForm(parent.Parent);
            }
            return null; // Control is not on a Form
        }

        void FolderTreeView_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {

       

        }

        void FolderTreeView_BeforeExpand(object sender, TreeViewCancelEventArgs e)
        {
             
            if (e.Node.Nodes[0].Text == "_hidden")
            {
                TreeNode hiddenNode = e.Node.Nodes[0];

                GetOneSubSites(e.Node, ((Sharepoint_Doc.Entities.Web)e.Node.Tag).Grouping );

                hiddenNode.Remove();

                e.Node.Expand();
            }
            else if (e.Node.Nodes[0].Text == "_hiddenRootWeb")
            {
                TreeNode tnWeb = e.Node;
                Sharepoint_Doc.Entities.Web web = (Sharepoint_Doc.Entities.Web)tnWeb.Tag;
 
                if (web == null)
                {
                    web = (Sharepoint_Doc.Entities.Web)e.Node.Nodes[0].Tag; //Collection based
                }
                //web = Sharepoint_Doc.Entities.Web.GetWeb(new Entities.Web.WebCriteria(web.Url, web.Grouping));

                e.Node.Nodes[0].Remove();

                foreach (DocumentLibrary documentLibrary in web.DocumentLibraries)
                {
 
            
                        TreeNode tnList = new TreeNode();
                        tnList.Text = documentLibrary.Title;
                        tnList.ImageIndex = 0;
                        tnList.SelectedImageIndex = 0;
                        tnList.StateImageIndex = 0;


                        tnList.Tag = documentLibrary;

                        AddTreeNodeHandler2 treeNodeWebHandler2 = new AddTreeNodeHandler2(AddTreeNode);
                        this.Invoke(treeNodeWebHandler2, tnWeb, tnList);
                    
                }

                if (web.SubWebs != null && web.IncludeSubSitesInListing)
                {
                    foreach (Sharepoint_Doc.Entities.Web subWeb in web.SubWebs)
                    {

                        TreeNode tnSubWeb = new TreeNode();
                        tnSubWeb.Text = subWeb.Title;
                        tnSubWeb.ForeColor = Color.Black;
                        tnSubWeb.Tag = subWeb;
                        tnSubWeb.ImageIndex = 1;
                        tnSubWeb.SelectedImageIndex = 1;
                        tnSubWeb.StateImageIndex = 1;
                        tnSubWeb.Tag = subWeb;

                        AddTreeNodeHandler2 treeNodeWebHandler = new AddTreeNodeHandler2(AddTreeNode);
                        this.Invoke(treeNodeWebHandler, tnWeb, tnSubWeb);

                        TreeNode tnHidden = new TreeNode("_hidden");
                        this.Invoke(treeNodeWebHandler, tnSubWeb, tnHidden);

                    }
                }

            }

        }

        private void GetAllSubSites(TreeNode tn)
        {

            //if(PermissionCheck== ePermissionCheck.View || 
            //    HasWritePermissionsInWebCommand.Execute((new DAL.WebDAL()).GetWeb(AddInGlobals.ConnectorSettings.RootUrl)))
                    AddRootNodeWebExplicit("Stadsportalen", AddInGlobals.ConnectorSettings.RootUrl);

            AddRootNodeWebExplicit("OneDrive", AddInGlobals.ConnectorSettings.OneDriveUrl);

            foreach (PreloadCommand.PreloadedWebs webGroup in PreloadCommand._list)
            {
                if (AddInGlobals.ExcludeSiteListNameList.Any(g => g.Title.ToLower() == webGroup.Url.ToLower()))
                    continue;

                TreeNode tnGrouping = new TreeNode(webGroup.TemplateID);
                this.Nodes.Add(tnGrouping);

                    foreach (Sharepoint_Doc.Entities.Web web in webGroup.Webs.OrderBy(s => s.Title).ToList())
                    {
                        TreeNode tnWeb = tnGrouping.Nodes.Add(web.Title);
                   
                        tnWeb.Tag = web;
                        tnWeb.ImageKey = "SharePointFoundation16";
                        tnWeb.SelectedImageKey = "SharePointFoundation16";

                        TreeNode tnHiddenWeb = new TreeNode("_hiddenRootWeb");
                        tnWeb.Nodes.Add(tnHiddenWeb);
                    }               
            }
        }

        private void AddRootNodeWebExplicit(string name, string url)
        {
            TreeNode tn = new TreeNode(name);
            var webS = Sharepoint_Doc.Entities.Web.NewWeb();
            webS.Url = url;
            webS.IncludeSubSitesInListing = false;
            tn.Tag = webS;
            tn.ImageKey = "SharePointFoundation16";
            tn.SelectedImageKey = "SharePointFoundation16";
            TreeNode tnHiddenWebStadsportalen = new TreeNode("_hiddenRootWeb");
            tn.Nodes.Add(tnHiddenWebStadsportalen);
            this.Nodes.Add(tn);
        }

        private delegate void AddWebToTreeDelegate(TreeNode tnWeb, string url, Grouping grouping);

        private void GetGroupingSite(TreeNode tnWeb, Grouping grouping)
        {

            SiteCollectionList sites = SiteCollectionList.GetList(grouping);
   
            foreach (SiteCollection siteCollection in sites)
            {
                Task task = new Task(delegate
                    {
                        this.Invoke(new AddWebToTreeDelegate(AddWebToTree), tnWeb, siteCollection.Url, grouping);
                    });
                       
                    


                task.Start();

            }
   
        }

        private void AddWebToTree(TreeNode tnWeb, string url, Grouping grouping)
        {

            Entities.Web web = Entities.Web.GetWeb(url);
            web.Grouping = grouping;
            web.Url = url;

            tnWeb = tnWeb.Nodes.Add(web.Title);

            tnWeb.Tag = web;
            tnWeb.ImageKey = "SharePointFoundation16";
            tnWeb.SelectedImageKey = "SharePointFoundation16";

            TreeNode tnHiddenWeb = new TreeNode("_hiddenRootWeb");
            tnWeb.Nodes.Add(tnHiddenWeb);

 
        }
 
        private void GetOneSubSites(TreeNode tn, Grouping grouping)
        {
            Entities.Web web = null;
            TreeNode tnWeb = new TreeNode();

            if (tn == null)
            {
                web = Entities.Web.GetWeb(new Entities.Web.WebCriteria(grouping.WebApplikation, grouping, false));
                tnWeb = new TreeNode();
                if (grouping != null)
                    tnWeb.Text = grouping.TemplateID;
                else
                    tnWeb.Text = web.Title;
                tnWeb.ForeColor = Color.Black;
                tnWeb.Tag = web;
                tnWeb.ImageIndex = 1;
                tnWeb.SelectedImageIndex = 1;
                tnWeb.StateImageIndex = 1;
  
                AddTreeNodeHandler treeNodeWebHandler = new AddTreeNodeHandler(AddTreeNode);
                this.Invoke(treeNodeWebHandler, tnWeb);
            }
            else
            {
                string url = AddInGlobals.ConnectorSettings.RootUrl + ((Sharepoint_Doc.Entities.Web)tn.Tag).ServerRelativeUrl;
                web = (Sharepoint_Doc.Entities.Web)tn.Tag; // Entities.Web.GetWeb(new Entities.Web.WebCriteria(url, grouping, true));
                tnWeb = tn;
            }

            foreach (DocumentLibrary documentLibrary in web.DocumentLibraries)
            {
                TreeNode tnList = new TreeNode();
                tnList.Text = documentLibrary.Title;
                tnList.ImageIndex = 0;
                tnList.SelectedImageIndex = 0;
                tnList.StateImageIndex = 0;

                tnList.Tag = documentLibrary;

                //tnWeb.Nodes.Add(tnList);
                bool hasPermissions = true; // CheckPermissions(documentLibrary, web.Url);

                if (hasPermissions)
                {
                    AddTreeNodeHandler2 treeNodeWebHandler = new AddTreeNodeHandler2(AddTreeNode);
                    this.Invoke(treeNodeWebHandler, tnWeb, tnList);
                }

            }

            if (web.SubWebs != null)
            {
                foreach (Sharepoint_Doc.Entities.Web subWeb in web.SubWebs)
                {

                    TreeNode tnSubWeb = new TreeNode();
                    tnSubWeb.Text = subWeb.Title;
                    tnSubWeb.ForeColor = Color.Black;
                    tnSubWeb.Tag = subWeb;
                    tnSubWeb.ImageIndex = 1;
                    tnSubWeb.SelectedImageIndex = 1;
                    tnSubWeb.StateImageIndex = 1;
                    tnSubWeb.Tag = subWeb;

                    AddTreeNodeHandler2 treeNodeWebHandler = new AddTreeNodeHandler2(AddTreeNode);
                    this.Invoke(treeNodeWebHandler, tnWeb, tnSubWeb);

                    TreeNode tnHidden = new TreeNode("_hidden");
                    this.Invoke(treeNodeWebHandler, tnSubWeb, tnHidden);

                }
            }

        }


        private void AddTreeNode(TreeNode tnWeb)
        {
            tnWeb.Expand();
            this.Nodes.Add(tnWeb);
        }

        private void AddTreeNode(TreeNode tnParent, TreeNode tn)
        {
            tnParent.Nodes.Add(tn);
        }

        public bool CheckPermissions(DocumentLibrary documentLibrary, string parentWebUrl)
        {
            DAL.ListDAL listDAL = new ListDAL();

  
            if (PermissionCheck == ePermissionCheck.View)
                return listDAL.CheckPermissions(parentWebUrl, documentLibrary.ListId, false);
                //return documentLibrary.ReadPermission;
            else
                return listDAL.CheckPermissions(parentWebUrl, documentLibrary.ListId, true);
                //return documentLibrary.WritePermission;
        }


    }
}
