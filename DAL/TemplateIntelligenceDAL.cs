﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Entities;
using Sharepoint_Doc.DAL;
using System.IO;

namespace EmpirConnector.DAL
{
    public class TemplateIntelligenceDAL: DALBase
    {
        public enum PartType
        {
            Logo,
            Footer
        }

        public ListItemCollection GetList(out FolderCollection attachmentFolders)
        {
            string parentWebUrl = AddInGlobals.ConnectorSettings.IntelligenceListWebUrl;

            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                List list = ctx.Web.Lists.GetByTitle(AddInGlobals.ConnectorSettings.IntelligenceListName); //"Connector - mallintelligens"

                var query = new CamlQuery();


                ctx.Load(list.Views);
                ctx.ExecuteQuery();

   
                var view = list.Views[0];
                ctx.Load(view);
                ctx.ExecuteQuery();

                query.ViewXml = "<View Scope=\"RecursiveAll\"> " +
                    "<Query>" +
                    view.ViewQuery +
                    "</Query>" +
                    "</View>";

            
                var folderItems = list.GetItems(query);
                ctx.Load(folderItems);
                ctx.Load(list.RootFolder);
                ctx.ExecuteQuery();

                Microsoft.SharePoint.Client.Folder folder = ctx.Web.GetFolderByServerRelativeUrl(
                         list.RootFolder.ServerRelativeUrl + "/Attachments"); 
                ctx.Load(folder.Folders);
                ctx.ExecuteQuery();
                foreach (var f in folder.Folders)
                {
                    ctx.Load(f.Files);
                }
                ctx.ExecuteQuery();

                attachmentFolders = folder.Folders;

                return folderItems;

            }
        }

        public bool IsGovernanceCorrectlyPrepared(string parentWebUrl, int itemId)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                List list = ctx.Web.Lists.GetByTitle(AddInGlobals.ConnectorSettings.IntelligenceListName);
                ctx.Load(list);
                ctx.Load(list.RootFolder);
                ctx.ExecuteQuery();

                // get the item's attachments folder 
                Microsoft.SharePoint.Client.Folder attFolder = ctx.Web.GetFolderByServerRelativeUrl(list.RootFolder.ServerRelativeUrl + "/Attachments/" + itemId.ToString());
                FileCollection files = attFolder.Files;
                ctx.Load(files);

                ctx.ExecuteQuery();

                return files.Count == 2;
            }
        }

        public byte[] GetAttachments(string parentWebUrl, string fv, PartType partType)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                List list = ctx.Web.Lists.GetByTitle(AddInGlobals.ConnectorSettings.IntelligenceListName);
                // these properties can be loaded in advance, outside of this method

                ctx.Load(list);
                ctx.Load(list.RootFolder);
                //ctx.Load(list, l => l.RootFolder.ServerRelativeUrl);
                //ctx.Load(ctx.Site, s => s.Url);
                ctx.ExecuteQuery();



                CamlQuery camlQuery = new CamlQuery();
                camlQuery.ViewXml = "<View><RowLimit>100</RowLimit></View>";
                ListItemCollection items = list.GetItems(camlQuery);
                ctx.Load(items);
                ctx.ExecuteQuery();

                ListItem item = null;

                foreach (ListItem lIetm in items)
                {
                    if (lIetm.FieldValues["Title"].ToString() == fv)
                    {
                        item = lIetm;
                        break;
                    }
                }
               
  
                // get the item's attachments folder 
                Microsoft.SharePoint.Client.Folder attFolder = ctx.Web.GetFolderByServerRelativeUrl(list.RootFolder.ServerRelativeUrl + "/Attachments/" + item.Id);
                FileCollection files = attFolder.Files;
                ctx.Load(files);

                ctx.ExecuteQuery();

                MemoryStream fileData = null;

                int partIndex = 0;
                string filename = files[0].Name.ToLower();

                if (partType == PartType.Logo)
                {
                    if (filename.EndsWith(".docx"))
                    {
                        partIndex = 1;
                    }
                }
                else
                {
                    if (filename.EndsWith(".png") || filename.EndsWith(".jpg"))
                    {
                        partIndex = 1;
                    }
                }

                if (partIndex > files.Count - 1)
                    return null;

                FileInformation fi = Microsoft.SharePoint.Client.File.OpenBinaryDirect(ctx, files[partIndex].ServerRelativeUrl);
                fileData = new MemoryStream();

                CopyStream(fi.Stream, fileData);
                  

                return fileData.ToArray();
            }
        }

        private void CopyStream(Stream source, Stream destination)
        {
            byte[] buffer = new byte[32768];
            int bytesRead;
            do
            {
                bytesRead = source.Read(buffer, 0, buffer.Length);
                destination.Write(buffer, 0, bytesRead);
            } while (bytesRead != 0);
        }

    }
}
