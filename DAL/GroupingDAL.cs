﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Entities;

namespace Sharepoint_Doc.DAL
{
    public class GroupingDAL: DALBase
    {
        public ListItemCollection GetList()
        {
            string parentWebUrl = AddInGlobals.ConnectorSettings.GroupingListWebUrl;

            using (ClientContext ctx = GetContext(parentWebUrl))
            {
           
                    var query = new CamlQuery();

                    query.ViewXml = "<View Scope=\"RecursiveAll\"> " +
                        "<Query>" +
                        "<Where>" +
                        "</Where>" +
                        "</Query>" +
                        "</View>";

                    List list = ctx.Web.Lists.GetByTitle(AddInGlobals.ConnectorSettings.GroupingListName); //"Connector - Sitegruppering"

                    var folderItems = list.GetItems(query);
                    ctx.Load(folderItems);
                    ctx.ExecuteQuery();

                    return folderItems;
            }
        }
    }
}
