﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Entities;
using Sharepoint_Doc.DAL;
using System.IO;

namespace EmpirConnector.DAL
{
    public class ExcludeListNameDAL : DALBase
    {
        public ListItemCollection GetList()
        {
            string parentWebUrl = AddInGlobals.ConnectorSettings.ExcludeListWebUrl;

            using (ClientContext ctx = GetContext(parentWebUrl))
            {

                var query = new CamlQuery();

                query.ViewXml = "<View Scope=\"RecursiveAll\"> " +
                    "<Query>" +
                    "<Where>" +
                    "</Where>" +
                    "</Query>" +
                    "</View>";

                List list = ctx.Web.Lists.GetByTitle(AddInGlobals.ConnectorSettings.ExcludeListName); 

                var folderItems = list.GetItems(query);
                ctx.Load(folderItems);
                ctx.ExecuteQuery();

                return folderItems;
            }
        }
    }
}
