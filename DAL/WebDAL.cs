﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Entities;


namespace Sharepoint_Doc.DAL
{
    public class WebDAL: DALBase
    {
        static object _lock = new object();

        public class SubWebsAndListsResult
        {
            public WebCollection SubWebs;
            public ListCollection Lists;
        }

        public string GetTemplate(string url)
        {
            string template = "";

  
            using (ClientContext ctx = GetContext(url))
            {
                try
                {
                   
                    ctx.Load(ctx.Web);
                    //ctx.Load(ctx.Web.Fields);
                    ctx.Load(ctx.Web.AllProperties);
                    ctx.ExecuteQuery();


                    if (ctx.Web.AllProperties.FieldValues.ContainsKey("WebTemplate"))
                    {
                        string webTemplate = ctx.Web.AllProperties.FieldValues["WebTemplate"].ToString();
                        string[] values = webTemplate.Split('.');
                        template = values[values.Length - 1];
                    }
                }
                catch { }
            }

            return template;
        }

        public SubWebsAndListsResult GetSubs(string url, bool readPermission, bool loadAllProperties)
        {
            SubWebsAndListsResult result = new SubWebsAndListsResult();

            ClientContext clientContext = new ClientContext(url);
            clientContext.Load(clientContext.Web);
            clientContext.ExecuteQuery();

            clientContext.Load(clientContext.Web,  ww => ww.Lists.Include(tt => tt.EffectiveBasePermissions, list => list.Id, list => list.Title, list => list.ParentWebUrl, list => list.Hidden, list => list.BaseType, list => list.EnableFolderCreation, ll => ll.DefaultEditFormUrl));

            //Todo continue
            WebCollection subWebCollection =  clientContext.Web.GetSubwebsForCurrentUser(null);
            //clientContext.Load(subWebCollection, n => n.Include(ww => ww.Id, ww => ww.Title, ww => ww.ServerRelativeUrl, ww => ww.AllProperties, pp => pp.AllProperties["WebTemplate"], ww => ww.Lists.Include(tt => tt.EffectiveBasePermissions, list => list.Id, list => list.Title, list => list.ParentWebUrl, list => list.Hidden, list => list.BaseType, list => list.EnableFolderCreation, ll => ll.DefaultEditFormUrl)));

            clientContext.Load(subWebCollection, n => n.Include(ww => ww.Id, ww => ww.Title, ww => ww.ServerRelativeUrl, ww=>ww.AllProperties, ww => ww.Lists.Include(tt => tt.EffectiveBasePermissions, list => list.Id, list => list.Title, list => list.ParentWebUrl, list => list.Hidden, list => list.BaseType, list => list.EnableFolderCreation, ll => ll.DefaultEditFormUrl))); 
            clientContext.ExecuteQuery();

            //if (loadAllProperties)
            //{
            //    foreach (Microsoft.SharePoint.Client.Web w in subWebCollection)
            //    {
            //        clientContext.Load(w.AllProperties);
            //    }

            //    clientContext.ExecuteQuery();
            //}

            result.SubWebs = subWebCollection;
            result.Lists = clientContext.Web.Lists;

            return result;

        }

        public Microsoft.SharePoint.Client.Web GetWeb(string url)
        {

            try
            {
                using (ClientContext ctx = GetContext(url))
                {
                    ctx.Load(ctx.Web);
                    //ctx.Load(ctx.Web.Fields);
                    ctx.Load(ctx.Web.Lists);
                    
                     //ctx.Load(ctx.Web.AllProperties);
                    ctx.ExecuteQuery();

                    foreach (List list in ctx.Web.Lists)
                    {
                        if (list.BaseType == BaseType.DocumentLibrary && !list.Hidden && !DocumentLibraries.ExcludedLibrary(list))
                            ctx.Load(list, tt => tt.EffectiveBasePermissions);
                    }
                    ctx.ExecuteQuery();

                    return ctx.Web;

                }
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message);
            }
        
        }

        public WebCollection GetSubWebs(Microsoft.SharePoint.Client.Web web, string url)
        {

            //string url = "";
            //if (web.ServerRelativeUrl != "/")
            //    url = web.ServerRelativeUrl;

            using (ClientContext ctx = GetContext(url))
            {
                ctx.Load( ctx.Site);
                ctx.Load(ctx.Web);
                ctx.ExecuteQuery();
                //ctx.Load(ctx.Web.Webs);
                ctx.Load(ctx.Web.SiteGroups);
                ctx.ExecuteQuery();

                var subWebs = ctx.Web.GetSubwebsForCurrentUser(new SubwebQuery());
                ctx.Load(subWebs);
                ctx.ExecuteQuery();

                foreach (Microsoft.SharePoint.Client.Web subweb in subWebs)
                {
                    ctx.Load(subweb);
                    ctx.Load(subweb.AllProperties);
                    ctx.Load(subweb.Fields);
                    ctx.Load(subweb.Lists);
                    System.Diagnostics.Debug.WriteLine(subweb.Url);
                   
                }

                ctx.ExecuteQuery();

                foreach (Microsoft.SharePoint.Client.Web subweb in subWebs)
                {
                    foreach (List list in subweb.Lists)
                    {
                        ctx.Load(list, tt => tt.EffectiveBasePermissions);
                    }
                }
                ctx.ExecuteQuery();

                return subWebs;
            }
        }

        public Microsoft.SharePoint.Client.Web LoadWeb(Microsoft.SharePoint.Client.Web web)
        {
            Microsoft.SharePoint.Client.Web thisWeb = null;

            string url = "";
            if (web.ServerRelativeUrl != "/")
                url = web.ServerRelativeUrl;

            using (ClientContext ctx = GetContext(Sharepoint_Doc.Entities.AddInGlobals.ConnectorSettings.RootUrl + url))
            {
                thisWeb = ctx.Web;
                ctx.Load(thisWeb);
                ctx.Load(thisWeb.AllProperties);
                ctx.Load(thisWeb.Fields);
                ctx.Load(thisWeb.Lists);
                ctx.ExecuteQuery();

                foreach (List list in thisWeb.Lists)
                {
                    ctx.Load(list, tt => tt.EffectiveBasePermissions);
                }
                ctx.ExecuteQuery();
            }

            return thisWeb;
        }

      

    }
}
