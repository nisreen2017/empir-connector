﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;


namespace Sharepoint_Doc.DAL
{
    public class ListDAL: DALBase
    {
        public class ListRemovedException : ApplicationException
        {

        }

        public class SharePointFolder
        {
            public string Name{set; get;}
            public string FileRef { set; get; }
            public List<SharePointFolder> Folders { set; get; }
        }

        public List GetTemplateLibrary(string listName, string parentWebUrl)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                List list = ctx.Web.Lists.GetByTitle(listName);
                ctx.Load(list);
                ctx.Load(list, tt => tt.EffectiveBasePermissions, tt => tt.DefaultEditFormUrl);
                ctx.ExecuteQuery();

                return list;
            }
        }

        public string GetRootFolderServerRelativeUrl(string listId, string parentWebUrl)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                List list = ctx.Web.Lists.GetById(new Guid(listId));
                ctx.Load(list);
                ctx.Load(list.RootFolder);
                ctx.ExecuteQuery();
                return list.RootFolder.ServerRelativeUrl;
            }
        }


        public void LoadFolderCollection(FolderCollection folderCollection, string parentWebUrl)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                ctx.Load(folderCollection);
                ctx.ExecuteQuery();
            }
        }

   
        public SharePointFolder LoadFolders(string listId, string parentWebUrl, bool defaultView)
        {
            CamlQuery query = new CamlQuery();


           
            SharePointFolder sharePointFolder = new SharePointFolder();
            //splist.Views[0].ViewQuery

            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                List list = ctx.Web.Lists.GetById(new Guid(listId));
                ctx.Load(list);
                ctx.ExecuteQuery();

                try
                {
                    ctx.Load(list.RootFolder);
                }
                catch (Exception ex)
                {
                    throw new ListRemovedException();
                }
                ctx.ExecuteQuery();

                if (defaultView)
                {
                    ctx.Load(list.Views);
                    ctx.ExecuteQuery();
                    query.ViewXml = "<View><Query>" + list.Views[0].ViewQuery  
                               + "   <Where>"
                      + "      <Eq><FieldRef Name='FSObjType' /><Value Type='Integer'>1</Value></Eq>"
                      + "   </Where>"    +           
                        
                        "</Query></View>"; 
                    
                }
                else
                {
                    query.ViewXml = "<View Scope='RecursiveAll'>"
                    + "<Query>"
                             + "   <OrderBy>"
                      + "   <FieldRef Name='FileRef' />"
                       + "   </OrderBy>"
                      + "   <Where>"
                      + "      <Eq><FieldRef Name='FSObjType' /><Value Type='Integer'>1</Value></Eq>"
                      + "   </Where>"

                      + "</Query>"
                      + "</View>";


                }
                var folders = list.GetItems(query);
                ctx.Load(folders);
                ctx.ExecuteQuery();

                SharePointFolder rootFolder = new SharePointFolder();
                rootFolder.FileRef = list.RootFolder.ServerRelativeUrl;
                rootFolder.Folders = new List<SharePointFolder>();
                sharePointFolder = rootFolder;

                foreach (var f in folders)
                {
                    string fileDirRef = f.FieldValues["FileDirRef"].ToString();
                    string fileRef = f.FieldValues["FileRef"].ToString();

                    SharePointFolder folder = new SharePointFolder();
                    folder.FileRef = fileRef;
                    string[] nameparts = fileRef.Split('/');
                    folder.Name = nameparts[nameparts.Count()-1];
                    folder.Folders = new List<SharePointFolder>();
                    FindSharePointFolder(rootFolder, fileDirRef).Folders.Add(folder);
                }

                return sharePointFolder;
            }
        }

        private SharePointFolder FindSharePointFolder(SharePointFolder rootFolder, string fileRef)
        {
            SharePointFolder resultFolder = null;

            if (rootFolder.FileRef == fileRef)
                resultFolder = rootFolder;
            else
            {
                foreach (SharePointFolder f in rootFolder.Folders)
                {
                    if (f.FileRef == fileRef)
                    {
                        resultFolder = f;
                        break;
                    }
                    else
                    {
                        resultFolder = FindSharePointFolder(f, fileRef);
                        if (resultFolder != null)
                            break;
                    }
                }
            }
            return resultFolder;
        }

        private void LoadFolders(ClientContext ctx, Folder parentFolder)
        {
            ctx.Load(parentFolder.Folders);
            ctx.ExecuteQuery();

            foreach (Folder folder in parentFolder.Folders)
            {
                LoadFolders(ctx, folder);
            }
        }

        public void LoadFolders(List list, Folder folder, string parentWebUrl)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                _clientContext.Load(folder.Folders);
                _clientContext.ExecuteQuery();
            }
        }

        public Sharepoint_Doc.DAL.ListDAL.SharePointFolder AddFolder(string parentWebUrl, string relativeUrl, string name, string listId)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                ctx.Load(ctx.Web);
                ctx.ExecuteQuery();

                Folder fo = ctx.Web.GetFolderByServerRelativeUrl(relativeUrl);

                ctx.Load(fo);
                ctx.Load(fo.Folders);
                ctx.ExecuteQuery();
                Folder foo = fo.Folders.Add(name);
                ctx.ExecuteQuery();

                ctx.Load(foo);
                ctx.Load(foo.Folders);
                ctx.ExecuteQuery();

                Sharepoint_Doc.DAL.ListDAL.SharePointFolder retFolder = new SharePointFolder();
                retFolder.FileRef = foo.ServerRelativeUrl;
                return retFolder;



            }
        }

        public Folder RenameFolder(string parentWebUrl, string relativeUrl, string name, string oldName, string listName)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                Web web = ctx.Web;
                List list = web.Lists.GetByTitle(listName);

                string FolderFullPath = "";

                CamlQuery query = new CamlQuery();
                query.ViewXml = "<View Scope=\"RecursiveAll\"> " +
                                "<Query>" +
                                    "<Where>" +
                                        //"<And>" +
                                            "<Eq>" +
                                                "<FieldRef Name=\"FSObjType\" />" +
                                                "<Value Type=\"Integer\">1</Value>" +
                                             "</Eq>" +
                                              
                                      //  "</And>" +
                                     "</Where>" +
                                "</Query>" +
                                "</View>";

                //"<Eq>" +
                //                                "<FieldRef Name=\"Title\"/>" +
                //                                "<Value Type=\"Text\">" + oldName + "</Value>" +
                //                              "</Eq>" +

                int pos = relativeUrl.LastIndexOf('/');
                query.FolderServerRelativeUrl = relativeUrl.Substring(0, pos); //"/ksf/arbetsrum/test-arbetsrum/dokument/Ny mapp 3"; // 

            //if (relativePath.Equals(string.Empty))
            //{
            //    query.FolderServerRelativeUrl = "/lists/" + listName;
            //}
            //else
            //{
            //    query.FolderServerRelativeUrl = "/lists/" + listName + "/" + relativePath;
            //}
        
           var folders = list.GetItems(query);

            ctx.Load(list);
            ctx.Load(list.Fields);
            ctx.Load(folders, fs => fs.Include(fi => fi["Title"],
                fi => fi["DisplayName"],
                fi => fi["FileLeafRef"]));
            ctx.ExecuteQuery();



            if (folders.Count >0)
            {
                ctx.Load(folders);
                ctx.ExecuteQuery();

                for (int i = 0; i < folders.Count; i++)
                {
                    string oldTitle = folders[i]["FileLeafRef"].ToString();

                    if (oldTitle == oldName)
                    {
                        folders[i]["Title"] = name;
                        folders[i]["FileLeafRef"] = name;
                        folders[i].Update();
                        ctx.ExecuteQuery();
                        break;
                    }
                }

            }



            }

            return null;
        }

        public void DeleteFolder(string parentWebUrl, string relativeUrl, string name, string listName)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                Web web = ctx.Web;
                List list = web.Lists.GetByTitle(listName);

    
                CamlQuery query = new CamlQuery();
                query.ViewXml = "<View Scope=\"RecursiveAll\"> " +
                                "<Query>" +
                                    "<Where>" +
                    //"<And>" +
                                            "<Eq>" +
                                                "<FieldRef Name=\"FSObjType\" />" +
                                                "<Value Type=\"Integer\">1</Value>" +
                                             "</Eq>" +

                                      //  "</And>" +
                                     "</Where>" +
                                "</Query>" +
                                "</View>";

                //"<Eq>" +
                //                                "<FieldRef Name=\"Title\"/>" +
                //                                "<Value Type=\"Text\">" + oldName + "</Value>" +
                //                              "</Eq>" +

                int pos = relativeUrl.LastIndexOf('/');
                query.FolderServerRelativeUrl = relativeUrl.Substring(0, pos); //"/ksf/arbetsrum/test-arbetsrum/dokument/Ny mapp 3"; // 

                //if (relativePath.Equals(string.Empty))
                //{
                //    query.FolderServerRelativeUrl = "/lists/" + listName;
                //}
                //else
                //{
                //    query.FolderServerRelativeUrl = "/lists/" + listName + "/" + relativePath;
                //}

                var folders = list.GetItems(query);

                ctx.Load(list);
                ctx.Load(list.Fields);
                ctx.Load(folders, fs => fs.Include(fi => fi["Title"],
                    fi => fi["DisplayName"],
                    fi => fi["FileLeafRef"]));
                ctx.ExecuteQuery();



                if (folders.Count > 0)
                {
                    ctx.Load(folders);
                    ctx.ExecuteQuery();

                    for (int i = 0; i < folders.Count; i++)
                    {
                        string oldTitle = folders[i]["FileLeafRef"].ToString();

                        if (oldTitle == name)
                        {
                            folders[i].DeleteObject();
                            ctx.ExecuteQuery();
                            break;
                        }
                    }

                }



            }


        }

        public bool CheckPermissions(string parentWebUrl, string listId, bool writePermissions)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                List list = ctx.Web.Lists.GetById(new Guid(listId));

                ctx.Load(list, tt => tt.EffectiveBasePermissions);
                ctx.ExecuteQuery();

                if (writePermissions)
                    return list.EffectiveBasePermissions.Has(PermissionKind.AddListItems);
                else
                    return list.EffectiveBasePermissions.Has(PermissionKind.ViewListItems);
            
            }

        }

       
      
    }
}
