﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using System.IO;
using EmpirConnector;




namespace Sharepoint_Doc.DAL
{
    public class FileDAL : DALBase
    {
        public void SaveFileInSharePoint2(string parentWebUrl, string serverRelativeUrl, string filename)
        {
 
            System.IO.FileStream fileStream = new System.IO.FileStream(filename, System.IO.FileMode.Open); //(System.IO.Path.GetTempPath() + 

            using (ClientContext ctx = GetContext(parentWebUrl))
            {
              
                Microsoft.SharePoint.Client.File.SaveBinaryDirect(ctx, serverRelativeUrl, fileStream, true);

            }

            fileStream.Close();
            fileStream.Dispose();
        }


        public byte[] DownloadDocument(string parentWebUrl, string listName, string filename)
        {
            using (var context = GetContext(parentWebUrl))
            {
                if (context != null)
                {
                    List sharedDocumentList = context.Web.Lists.GetByTitle(listName);
                    CamlQuery camlQuery = new CamlQuery();
                    camlQuery.ViewXml =
                        @"<View>
                <Query>
                  <Where>
                    <Eq>
                      <FieldRef Name='FileLeafRef'/>
                      <Value Type='Text'>";
                    
                   camlQuery.ViewXml += filename + "</Value></Eq></Where><RowLimit>1</RowLimit> </Query> </View>";

                    ListItemCollection listItems = sharedDocumentList.GetItems(camlQuery);
                    context.Load(sharedDocumentList);
                    context.Load(listItems);
                    context.ExecuteQuery();
                    if (listItems.Count >= 1)
                    {
                        ListItem item = listItems[0];
                        FileInformation fileInformation = Microsoft.SharePoint.Client.File.OpenBinaryDirect(context, (string)item["FileRef"]);
                        using (System.IO.MemoryStream memoryStream = new MemoryStream())
                        {
                            //CopyStream(fileInformation.Stream, memoryStream);
                            return memoryStream.ToArray();
                        }
                    }
                }
            }
            return null;
        }



        public void SaveFileInSharePoint(string parentWebUrl, string serverRelativeUrl, string listName)
        {
  



            using (ClientContext ctx = GetContext(parentWebUrl))
            {
                //Microsoft.SharePoint.Client.File.SaveBinaryDirect(ctx, serverRelativeUrl, fileStream, true);

                //fileStream.Close();
                //fileStream.Dispose();

                //Get Document List
                List documentsList = ctx.Web.Lists.GetByTitle(listName);

                var fileCreationInformation = new FileCreationInformation();
                //Assign to content byte[] i.e. documentStream

                fileCreationInformation.Content = null;
                //Allow owerwrite of document

                fileCreationInformation.Overwrite = true;

                int pos = serverRelativeUrl.IndexOf("/", 2);
                serverRelativeUrl = serverRelativeUrl.Substring(pos);
                fileCreationInformation.Url = parentWebUrl + serverRelativeUrl; // "http://stadsportalen.trollhattan.se/asf/dokument/222.docx"; // serverRelativeUrl;
                
                Microsoft.SharePoint.Client.File uploadFile = documentsList.RootFolder.Files.Add(
                    fileCreationInformation);

                ctx.ExecuteQuery();
            }


        }

        /*
        public EmpirConnector.WebReference1.FileInfo[] LoadFiles(string serverRelativeUrl, string parentWebUrl, string listId)
        {

            using (ClientContext ctx = GetContext(parentWebUrl))
            {

                ctx.Load(ctx.Site);
                ctx.Load(ctx.Web);
                ctx.ExecuteQuery();
                //var query = new CamlQuery();

                //query.FolderServerRelativeUrl = serverRelativeUrl;

                //query.ViewXml = "<View Scope=\"RecursiveAll\"> " +
                //    "<Query>" +
                //    "<Where>" +
                //                "<Eq>" +
                //                    "<FieldRef Name=\"FileDirRef\" />" +
                //                    "<Value Type=\"Text\">" + serverRelativeUrl + "</Value>" +
                //                 "</Eq>" +
                //    "</Where>" +
                //    "<OrderBy><FieldRef Name='FileLeafRef'/></OrderBy>" +
                //    "</Query>" +
                //    "</View>";


                using (EmpirConnector.WebReference1.EmpirConnectorServiceWS client = new EmpirConnector.WebReference1.EmpirConnectorServiceWS())
                {
                    client.Url = Sharepoint_Doc.Entities.AddInGlobals.ConnectorSettings.ServiceUrl;

                    Uri uri = new Uri(ctx.Site.Url);
                    string host = "http://" + uri.Host;

                    client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                    return client.GetFilesInFolder(ctx.Site.Url, host + serverRelativeUrl);
                }

             
           
                List list = ctx.Web.Lists.GetById(new Guid(listId));

                Folder folder = ctx.Web.GetFolderByServerRelativeUrl(serverRelativeUrl);

                ctx.Load(folder, f=>f.Files.Include(ss=>ss.TimeLastModified, ss=>ss.ModifiedBy, ss=>ss.Name, ss=>ss.ListItemAllFields.DisplayName));
                ctx.ExecuteQuery();

                //foreach(Microsoft.SharePoint.Client.File f in folder.Files)
                //{
                //    ctx.Load(f.ListItemAllFields);
                //    ctx.Load(f.ModifiedBy);
                //}

                foreach (var ff in folder.Files)
                {
                    DateTime startDate = (DateTime.SpecifyKind(DateTime.Parse(ff.TimeLastModified.ToString()), DateTimeKind.Utc)).ToLocalTime();

                    System.IO.File.AppendAllText("c:\\temp\\meta.txt", ff.Name + "; " + startDate.ToString() + "; " +
                        ff.ModifiedBy.Title + Environment.NewLine);
                }

                var tt = folder.Files[0].Name;
                var xx = folder.Files[0].ModifiedBy.Title;
                var dd = folder.Files[0].TimeLastModified;
                

         
                //string intell = folder.Files[0].ListItemAllFields.n; // folder.Files[0].ListItemAllFields["Name"].ToString();
                //intell += "";
                return null;
                ////var folderItems = list.GetItems(query);
                ////ctx.Load(folderItems);
                //ctx.ExecuteQuery();


                //return folder.Files;

                //FileLeafRef
            }
           
        }
        */

        public List<Microsoft.SharePoint.Client.File> LoadFiles2(string serverRelativeUrl, string parentWebUrl, string listId)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {

                ctx.Load(ctx.Site);
                ctx.Load(ctx.Web);
                ctx.ExecuteQuery();

                Console.WriteLine(ctx.Web.Title);

                List list = ctx.Web.Lists.GetById(new Guid(listId));

                Folder folder = ctx.Web.GetFolderByServerRelativeUrl(serverRelativeUrl);
                
                if(AddinModule.CurrentFunction == AddinModule.CurrentFunctionEnum.Templates)
                    ctx.Load(folder, f => f.Files.Include(ss => ss.TimeLastModified, ss => ss.ModifiedBy, ss => ss.Name, ss => ss.ListItemAllFields.DisplayName, ss=>ss.ListItemAllFields["Intelligent"]));
 
                else
                    ctx.Load(folder,  f => f.Files.Include(ss => ss.TimeLastModified, ss => ss.ModifiedBy, ss => ss.Name, ss => ss.ListItemAllFields.DisplayName));
                
                ctx.ExecuteQuery();


                return folder.Files.ToList();
  
            }
        }
         public Microsoft.SharePoint.Client.File DeleteFile(string serverRelativeUrl, string parentWebUrl, string listId, string filename)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {

                try
                {
                    Microsoft.SharePoint.Client.File fi = ctx.Web.GetFileByServerRelativeUrl(serverRelativeUrl + "/" + filename);

                    ctx.Load(fi);
                    fi.DeleteObject();
                    ctx.ExecuteQuery();

                    return fi;
                }
                catch
                {
                    return null;
                }
            }
        }

         public bool CheckFileIn(string webApplication, string serverRelativeUrl, string parentWebUrl, string listId, string filename)
         {


             using (ClientContext ctx = GetContext(parentWebUrl))
             {
                 bool retVal = true;

                 try
                 {
                     Microsoft.SharePoint.Client.File fi = ctx.Web.GetFileByServerRelativeUrl(serverRelativeUrl + "/" + filename);

                   
                     ctx.Load(fi);
                     
                     ctx.ExecuteQuery();

                     if (fi.CheckOutType != CheckOutType.None)
                         fi.CheckIn("", CheckinType.MajorCheckIn);

                     ctx.ExecuteQuery();

                     //Must check in before visible in Service
                     bool missingRequiredField = false;
                     using (EmpirConnector.WebReference1.EmpirConnectorServiceWS client = new EmpirConnector.WebReference1.EmpirConnectorServiceWS())
                     {
                         client.Url = Sharepoint_Doc.Entities.AddInGlobals.ConnectorSettings.ServiceUrl;
                         client.Credentials = System.Net.CredentialCache.DefaultNetworkCredentials;
                         missingRequiredField = client.MissingRequiredField(parentWebUrl, webApplication + serverRelativeUrl + "/" + filename);
                         if (missingRequiredField)
                         {
                             fi.CheckOut();
                             ctx.ExecuteQuery();
                             retVal = false;
                         }
                     }

                     
                 }
                 catch(Exception ex)
                 {
                     string message = ex.Message;

                     retVal = false;
                 }

                 return retVal;
             }
         }

         public void SetDefaultContentType(string serverRelativeUrl, string parentWebUrl, string listId, string filename)
         {
             using (ClientContext ctx = GetContext(parentWebUrl))
             {

                 try
                 {
                     List list = ctx.Web.Lists.GetById(new Guid(listId));
                     ctx.Load(list);
                     ctx.Load(list.ContentTypes);
                     ctx.ExecuteQuery();

                     ContentType ct = null;

                     if (list.ContentTypes.Count > 0)
                         ct = list.ContentTypes[0];


                     Microsoft.SharePoint.Client.File fi = ctx.Web.GetFileByServerRelativeUrl(serverRelativeUrl + "/" + filename);
                     ctx.Load(fi);
                     ctx.ExecuteQuery();

                     fi.ListItemAllFields["ContentTypeId"] = null; // ct.Id;
                     ctx.ExecuteQuery();

                     //int itemId = int.Parse(list.GetItemById(fi.ListItemAllFields["ID"].ToString());
                     //ListItem listItem = list.GetItemById(itemId);
                     //listItem.ContentType.

                 }
                 catch
                 {
                    
                 }
             }

         }

         public string GetFilenameByID(string parentWebUrl, int id, string listId)
         {
             string fileName = "";

             using (ClientContext ctx = GetContext(parentWebUrl))
             {

                 try
                 {

                     List list = ctx.Web.Lists.GetById(new Guid(listId));
                     ListItem listItem = list.GetItemById(id);

                     ctx.Load(list);
                     ctx.Load(listItem);
                     ctx.ExecuteQuery();

                     fileName = listItem["FileLeafRef"].ToString();

                     
                 }
                 catch(Exception ex)
                 {
                     return null;
                 }

                 return fileName;
             }
         }

        public Microsoft.SharePoint.Client.File LoadFile(string serverRelativeUrl, string parentWebUrl, string listId, string filename)
        {
            using (ClientContext ctx = GetContext(parentWebUrl))
            {

                try
                {
                    Microsoft.SharePoint.Client.File fi = ctx.Web.GetFileByServerRelativeUrl(serverRelativeUrl + "/" + filename);
                    
                    ctx.Load(fi);
                    ctx.Load(fi.ListItemAllFields);
                    ctx.ExecuteQuery();
                   
                    return fi;
                }
                catch(Exception ex)
                {
                    return null;
                }


            }
        }
    }
}
