﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.SharePoint.Client;
using Sharepoint_Doc.Entities;
using Sharepoint_Doc.DAL;
using System.Net;

namespace EmpirConnector.DAL
{
    public class ExcludeSiteListNameDAL: DALBase
    {
        public ListItemCollection GetList()
        {
            string parentWebUrl = AddInGlobals.ConnectorSettings.ExcludeSiteListWebUrl;

            using (ClientContext ctx = GetContext(parentWebUrl)) //
            {
                ctx.AuthenticationMode = ClientAuthenticationMode.Default;
                ctx.Credentials = CredentialCache.DefaultNetworkCredentials;

                var query = new CamlQuery();

                query.ViewXml = "<View Scope=\"RecursiveAll\"> " +
                    "<Query>" +
                    "<Where>" +
                    "</Where>" +
                    "</Query>" +
                    "</View>";

                List list = ctx.Web.Lists.GetByTitle(AddInGlobals.ConnectorSettings.ExcludeSiteListName);

                var folderItems = list.GetItems(query);
                ctx.Load(folderItems);
                ctx.ExecuteQuery();

                return folderItems;
            }
        }

 
    }
}
